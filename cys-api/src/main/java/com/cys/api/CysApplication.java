package com.cys.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author cys
 * @version v1.0
 * @className CysApplication
 * @date 2024/12/30
 * @description TODO
 */
@Slf4j
@SpringBootApplication
@ComponentScan(basePackages = {"com.cys"})
public class CysApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(CysApplication.class, args);
            System.out.println("cys启动成功~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            System.out.println("cys启动失败~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
    }

}
