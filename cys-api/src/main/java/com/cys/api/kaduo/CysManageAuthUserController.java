package com.cys.api.kaduo;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.manageuser.domain.CysManageAuthUser;
import com.cys.kaduo.manageuser.service.ICysManageAuthUserService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 小程序授权管理员Controller
 * 
 * @author cys
 * @date 2023-09-15
 */
@RestController
@RequestMapping("/minisystem/manageuser")
public class CysManageAuthUserController extends BaseController
{
    @Autowired
    private ICysManageAuthUserService cysManageAuthUserService;

    /**
     * 查询小程序授权管理员列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:manageuser:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysManageAuthUser cysManageAuthUser)
    {
        startPage();
        List<CysManageAuthUser> list = cysManageAuthUserService.selectCysManageAuthUserList(cysManageAuthUser);
        return getDataTable(list);
    }

    /**
     * 导出小程序授权管理员列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:manageuser:export')")
    @Log(title = "小程序授权管理员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysManageAuthUser cysManageAuthUser)
    {
        List<CysManageAuthUser> list = cysManageAuthUserService.selectCysManageAuthUserList(cysManageAuthUser);
        ExcelUtil<CysManageAuthUser> util = new ExcelUtil<CysManageAuthUser>(CysManageAuthUser.class);
        util.exportExcel(response, list, "小程序授权管理员数据");
    }

    /**
     * 获取小程序授权管理员详细信息
     */
    @PreAuthorize("@ss.hasPermi('minisystem:manageuser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysManageAuthUserService.selectCysManageAuthUserById(id));
    }

    /**
     * 新增小程序授权管理员
     */
    @PreAuthorize("@ss.hasPermi('minisystem:manageuser:add')")
    @Log(title = "小程序授权管理员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysManageAuthUser cysManageAuthUser)
    {
        return toAjax(cysManageAuthUserService.insertCysManageAuthUser(cysManageAuthUser));
    }

    /**
     * 修改小程序授权管理员
     */
    @PreAuthorize("@ss.hasPermi('minisystem:manageuser:edit')")
    @Log(title = "小程序授权管理员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysManageAuthUser cysManageAuthUser)
    {
        return toAjax(cysManageAuthUserService.updateCysManageAuthUser(cysManageAuthUser));
    }

    /**
     * 删除小程序授权管理员
     */
    @PreAuthorize("@ss.hasPermi('minisystem:manageuser:remove')")
    @Log(title = "小程序授权管理员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysManageAuthUserService.deleteCysManageAuthUserByIds(ids));
    }
}
