package com.cys.api.kaduo;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.utils.SecurityUtils;
import com.cys.league.member.domain.CysLeagueMember;
import com.cys.league.member.service.ICysLeagueMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 团管理相关业务
 */
@RestController
@RequestMapping("/v1/leagueMember")
public class LeagueMemberController extends BaseController {

    @Autowired
    private ICysLeagueMemberService cysLeagueMemberService;

    /**
     * 我的团员列表
     * @return
     */
    @GetMapping("/listMember")
    public TableDataInfo listMember(CysLeagueMember cysLeagueMember){

        startPage();//开启分页

        cysLeagueMember.setDelFlag("0");
        cysLeagueMember.setUserId(SecurityUtils.getUserId());
        LambdaQueryWrapper<CysLeagueMember> wrapper = Wrappers.lambdaQuery(cysLeagueMember);

        wrapper.orderByDesc(CysLeagueMember::getGradYear);

        List<CysLeagueMember> cysLeagueMembers = cysLeagueMemberService.list(wrapper);

        return getDataTable(cysLeagueMembers);
    }

}
