package com.cys.api.kaduo;

import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.utils.SecurityUtils;
import com.cys.kaduo.apply.domain.CysAuthApply;
import com.cys.kaduo.apply.service.ICysAuthApplyService;
import com.cys.kaduo.core.service.AdminService;
import com.cys.kaduo.manageuser.domain.CysManageAuthUser;
import com.cys.kaduo.manageuser.service.ICysManageAuthUserService;
import com.cys.kaduo.notice.domain.CysNotice;
import com.cys.mynika.entity.ApplyAuthBody;
import com.cys.system.service.ISysRoleService;
import com.cys.system.service.ISysUserService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 第一版小程序超级管理员服务
 */
@RestController
@RequestMapping("/v1/admin")
public class AdminController extends BaseController {

    @Autowired
    private ICysManageAuthUserService cysManageAuthUserService;

    @Autowired
    private ICysAuthApplyService cysAuthApplyService;

    @Autowired
    private AdminService adminService;

    /**
     * 小程序菜单,自定义菜单
     * 1、按照角色拉取菜单内容
     * redis 策略
     * 缓存30天过期，缓存key值为："authMiniMenus:" + userId + roleId
     * 角色变了重新做缓存
     * @return
     */
    @GetMapping("/listMiniMenu")
    public AjaxResult listMiniMenu(){
       return adminService.listMiniMenu();
    }

    /**
     * 可申请的权限接口
     * 查找全部角色，过滤掉不可申请的权限列表，查找当前用户拥有的角色，循环做标记
     * @return
     */
    @GetMapping("/listApplyAuth")
    public AjaxResult listApplyAuth(){
        return adminService.listApplyAuth();
    }

    /**
     * 当前申请数量，超过三个时，将不再可以申请
     * 直接查表，看当前未处理的数量，返回未处理的申请单，前端通过length来判断有是否可以申请
     * @return
     */
    @GetMapping("/currentApply")
    public AjaxResult currentApply(){
        return AjaxResult.success();
    }

    /**
     * 管理员的授权单
     * 1、拿到当前管理员的userId
     * 2、用户Id去查表，查表主要是要roleId，按照逗号分隔成字符串
     * 3、按照分页及类型(待通过，已通过)查询当前数据，去申请表中查询可处理的数据，严格与权限挂钩。
     * @return
     */
    @GetMapping("/myAuthApply")
    public TableDataInfo myAuthApply() {
         Long userId = SecurityUtils.getUserId();

        //获取可以授权的角色ID组
         List<Long> roleIds = cysManageAuthUserService.lambdaQuery()
                .eq(CysManageAuthUser::getUserId, userId)
                .list()
                .stream()
                .map(CysManageAuthUser::getRoleId)
                .collect(Collectors.toList());

         startPage();//开启分页
         //此处逻辑分为已通过和未处理，自动拒绝的就不显示了
         List<CysAuthApply> authApplyList = cysAuthApplyService.lambdaQuery()
                .in(CysAuthApply::getRoleId, roleIds)
                .and(i->i.eq(CysAuthApply::getHandleId, userId).or().isNull(CysAuthApply::getHandleId))
                .and(j->j.eq(CysAuthApply::getHandleResult, "已通过").or().isNull(CysAuthApply::getHandleResult))
                .orderByAsc(CysAuthApply::getHandleId)
                .orderByDesc(CysAuthApply::getApplyTime)
                .list();

         return getDataTable(authApplyList);
    }

    /**
     * 管理员确认授权
     * @return
     */
    @PostMapping("/confirmAuth")
    public AjaxResult confirmAuth(@RequestBody String applyId){
        return adminService.confirmAuth(applyId);
    }

    /**
     * 用户申请权限
     * @return
     */
    @PostMapping("/applyAuth")
    public AjaxResult applyAuth(@RequestBody ApplyAuthBody authBody){
        return adminService.applyAuth(authBody.getRoleId(), authBody.getContent());
    }


    /**
     * 通知详情
     * @param id
     * @return
     */
    @GetMapping("/notice/get")
    public AjaxResult getNoticeDetail(String id){
        return adminService.getNoticeDetail(id);
    }

    /**
     * 通知列表
     * @param notice
     * @return
     */
    @GetMapping("/notice/list")
    public TableDataInfo listNotice(CysNotice notice){

        startPage();//开启分页

        List<CysNotice> cysNotices = adminService.listNotice(notice);

        return getDataTable(cysNotices);
    }

    /**
     * 未处理的通知个数
     * @return
     */
    @GetMapping("/notice/count")
    public AjaxResult noticeCount(){
        return adminService.noticeCount();
    }

}
