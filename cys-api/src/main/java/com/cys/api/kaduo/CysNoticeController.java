package com.cys.api.kaduo;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.notice.domain.CysNotice;
import com.cys.kaduo.notice.service.ICysNoticeService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 通知公告Controller
 *
 * @author cys
 * @date 2023-10-13
 */
@RestController
@RequestMapping("/minisystem/notice")
public class CysNoticeController extends BaseController {
    @Autowired
    private ICysNoticeService cysNoticeService;

    /**
     * 查询通知公告列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:notice:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysNotice cysNotice) {
        startPage();
        List<CysNotice> list = cysNoticeService.selectCysNoticeList(cysNotice);
        return getDataTable(list);
    }

    /**
     * 导出通知公告列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:notice:export')")
    @Log(title = "通知公告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysNotice cysNotice) {
        List<CysNotice> list = cysNoticeService.selectCysNoticeList(cysNotice);
        ExcelUtil<CysNotice> util = new ExcelUtil<CysNotice>(CysNotice.class);
        util.exportExcel(response, list, "通知公告数据");
    }

    /**
     * 获取通知公告详细信息
     */
    @PreAuthorize("@ss.hasPermi('minisystem:notice:query')")
    @GetMapping(value = "/{noticeId}")
    public AjaxResult getInfo(@PathVariable("noticeId") String noticeId) {
        return success(cysNoticeService.selectCysNoticeByNoticeId(noticeId));
    }

    /**
     * 新增通知公告
     */
    @PreAuthorize("@ss.hasPermi('minisystem:notice:add')")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysNotice cysNotice) {
        return toAjax(cysNoticeService.insertCysNotice(cysNotice));
    }

    /**
     * 修改通知公告
     */
    @PreAuthorize("@ss.hasPermi('minisystem:notice:edit')")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysNotice cysNotice) {
        return toAjax(cysNoticeService.updateCysNotice(cysNotice));
    }

    /**
     * 删除通知公告
     */
    @PreAuthorize("@ss.hasPermi('minisystem:notice:remove')")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{noticeIds}")
    public AjaxResult remove(@PathVariable String[] noticeIds) {
        return toAjax(cysNoticeService.deleteCysNoticeByNoticeIds(noticeIds));
    }
}
