package com.cys.api.kaduo;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;
import com.cys.kaduo.rolemenu.service.ICysRoleMinimenuService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 小程序菜单关联表Controller
 * 
 * @author cys
 * @date 2023-09-04
 */
@RestController
@RequestMapping("/minisystem/rolemenu")
public class CysRoleMinimenuController extends BaseController
{
    @Autowired
    private ICysRoleMinimenuService cysRoleMinimenuService;

    /**
     * 查询小程序菜单关联表列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:rolemenu:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysRoleMinimenu cysRoleMinimenu)
    {
        startPage();
        List<CysRoleMinimenu> list = cysRoleMinimenuService.selectCysRoleMinimenuList(cysRoleMinimenu);
        return getDataTable(list);
    }

    /**
     * 导出小程序菜单关联表列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:rolemenu:export')")
    @Log(title = "小程序菜单关联表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysRoleMinimenu cysRoleMinimenu)
    {
        List<CysRoleMinimenu> list = cysRoleMinimenuService.selectCysRoleMinimenuList(cysRoleMinimenu);
        ExcelUtil<CysRoleMinimenu> util = new ExcelUtil<CysRoleMinimenu>(CysRoleMinimenu.class);
        util.exportExcel(response, list, "小程序菜单关联表数据");
    }

    /**
     * 获取小程序菜单关联表详细信息
     */
    @PreAuthorize("@ss.hasPermi('minisystem:rolemenu:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cysRoleMinimenuService.selectCysRoleMinimenuById(id));
    }

    /**
     * 新增小程序菜单关联表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:rolemenu:add')")
    @Log(title = "小程序菜单关联表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysRoleMinimenu cysRoleMinimenu)
    {
        return toAjax(cysRoleMinimenuService.insertCysRoleMinimenu(cysRoleMinimenu));
    }

    /**
     * 修改小程序菜单关联表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:rolemenu:edit')")
    @Log(title = "小程序菜单关联表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysRoleMinimenu cysRoleMinimenu)
    {
        return toAjax(cysRoleMinimenuService.updateCysRoleMinimenu(cysRoleMinimenu));
    }

    /**
     * 删除小程序菜单关联表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:rolemenu:remove')")
    @Log(title = "小程序菜单关联表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cysRoleMinimenuService.deleteCysRoleMinimenuByIds(ids));
    }
}
