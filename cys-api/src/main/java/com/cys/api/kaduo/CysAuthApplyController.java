package com.cys.api.kaduo;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.apply.domain.CysAuthApply;
import com.cys.kaduo.apply.service.ICysAuthApplyService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 用户申请权限表Controller
 * 
 * @author cys
 * @date 2023-09-15
 */
@RestController
@RequestMapping("/minisystem/apply")
public class CysAuthApplyController extends BaseController
{
    @Autowired
    private ICysAuthApplyService cysAuthApplyService;

    /**
     * 查询用户申请权限表列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:apply:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysAuthApply cysAuthApply)
    {
        startPage();
        List<CysAuthApply> list = cysAuthApplyService.selectCysAuthApplyList(cysAuthApply);
        return getDataTable(list);
    }

    /**
     * 导出用户申请权限表列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:apply:export')")
    @Log(title = "用户申请权限表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysAuthApply cysAuthApply)
    {
        List<CysAuthApply> list = cysAuthApplyService.selectCysAuthApplyList(cysAuthApply);
        ExcelUtil<CysAuthApply> util = new ExcelUtil<CysAuthApply>(CysAuthApply.class);
        util.exportExcel(response, list, "用户申请权限表数据");
    }

    /**
     * 获取用户申请权限表详细信息
     */
    @PreAuthorize("@ss.hasPermi('minisystem:apply:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysAuthApplyService.selectCysAuthApplyById(id));
    }

    /**
     * 新增用户申请权限表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:apply:add')")
    @Log(title = "用户申请权限表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysAuthApply cysAuthApply)
    {
        return toAjax(cysAuthApplyService.insertCysAuthApply(cysAuthApply));
    }

    /**
     * 修改用户申请权限表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:apply:edit')")
    @Log(title = "用户申请权限表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysAuthApply cysAuthApply)
    {
        return toAjax(cysAuthApplyService.updateCysAuthApply(cysAuthApply));
    }

    /**
     * 删除用户申请权限表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:apply:remove')")
    @Log(title = "用户申请权限表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysAuthApplyService.deleteCysAuthApplyByIds(ids));
    }
}
