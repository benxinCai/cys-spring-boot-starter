package com.cys.api.kaduo;


import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.domain.entity.SysRole;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.minimenu.domain.CysMiniMenu;
import com.cys.kaduo.minimenu.service.ICysMiniMenuService;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;
import com.cys.kaduo.rolemenu.service.ICysRoleMinimenuService;
import com.cys.system.service.ISysRoleService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 小程序菜单Controller
 * 
 * @author cys
 * @date 2023-09-04
 */
@RestController
@RequestMapping("/minisystem/menu")
public class CysMiniMenuController extends BaseController
{
    @Autowired
    private ICysMiniMenuService cysMiniMenuService;

    @Autowired
    private ICysRoleMinimenuService cysRoleMinimenuService;

    @Autowired
    private ISysRoleService roleService;

    /**
     * 查询小程序菜单列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:menu:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysMiniMenu cysMiniMenu)
    {
        startPage();
        List<CysMiniMenu> list = cysMiniMenuService.selectCysMiniMenuList(cysMiniMenu);

        list = list.stream().map(e->{
            String roleNames = cysRoleMinimenuService.lambdaQuery()
                    .eq(CysRoleMinimenu::getMiniMenuId, e.getId())
                    .list()
                    .stream()
                    .map(CysRoleMinimenu::getRoleName)
                    .collect(Collectors.joining(","));
            e.setRoles(roleNames);
            return e;
        }).collect(Collectors.toList());

        return getDataTable(list);
    }

    /**
     * 获取某个小程序页面所拥有的角色
     * @return
     */
    @GetMapping("/listPageOwenPages")
    public AjaxResult listPageOwenPages(long menuId){

        List<SysRole> roleList = roleService.selectRoleAll();

        List<CysRoleMinimenu> cysRoleMinimenuList = cysRoleMinimenuService.lambdaQuery()
                .eq(CysRoleMinimenu::getMiniMenuId, menuId)
                .list();

//        roleList = roleList.stream()
//                .map(item -> {
//                    item.setMiniFlag(cysRoleMinimenuList.stream().anyMatch(e -> e.getRoleId() == item.getRoleId()));
//                    return item;
//                }).collect(Collectors.toList());

        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("roleArray", roleList);
        //resultMap.put("checkedCities", cysRoleMinimenuList.stream().map(CysRoleMinimenu::getRoleId).collect(Collectors.toList()));
        resultMap.put("checkedCities", cysRoleMinimenuList.stream().map(CysRoleMinimenu::getRoleId).collect(Collectors.toList()));

        return AjaxResult.success(resultMap);
    }

    @PostMapping("/saveRoleMenu")
    public AjaxResult saveRoleMenu(@RequestBody List<CysRoleMinimenu> minimenus){
        return cysMiniMenuService.saveRoleMenu(minimenus);
    }

    /**
     * 导出小程序菜单列表
     */
    @PreAuthorize("@ss.hasPermi('minisystem:menu:export')")
    @Log(title = "小程序菜单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysMiniMenu cysMiniMenu)
    {
        List<CysMiniMenu> list = cysMiniMenuService.selectCysMiniMenuList(cysMiniMenu);
        ExcelUtil<CysMiniMenu> util = new ExcelUtil<CysMiniMenu>(CysMiniMenu.class);
        util.exportExcel(response, list, "小程序菜单数据");
    }

    /**
     * 获取小程序菜单详细信息
     */
    @PreAuthorize("@ss.hasPermi('minisystem:menu:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cysMiniMenuService.selectCysMiniMenuById(id));
    }

    /**
     * 新增小程序菜单
     */
    @PreAuthorize("@ss.hasPermi('minisystem:menu:add')")
    @Log(title = "小程序菜单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysMiniMenu cysMiniMenu)
    {
        return toAjax(cysMiniMenuService.insertCysMiniMenu(cysMiniMenu));
    }

    /**
     * 修改小程序菜单
     */
    @PreAuthorize("@ss.hasPermi('minisystem:menu:edit')")
    @Log(title = "小程序菜单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysMiniMenu cysMiniMenu)
    {
        return toAjax(cysMiniMenuService.updateCysMiniMenu(cysMiniMenu));
    }

    /**
     * 删除小程序菜单
     */
    @PreAuthorize("@ss.hasPermi('minisystem:menu:remove')")
    @Log(title = "小程序菜单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cysMiniMenuService.deleteCysMiniMenuByIds(ids));
    }
}
