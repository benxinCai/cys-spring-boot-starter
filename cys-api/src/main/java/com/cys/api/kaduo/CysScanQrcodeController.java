package com.cys.api.kaduo;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.qrcode.domain.CysScanQrcode;
import com.cys.kaduo.qrcode.service.ICysScanQrcodeService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 扫码登录Controller
 * 
 * @author cys
 * @date 2023-10-18
 */
@RestController
@RequestMapping("/system/qrcode")
public class CysScanQrcodeController extends BaseController
{
    @Autowired
    private ICysScanQrcodeService cysScanQrcodeService;

    /**
     * 查询扫码登录列表
     */
    @PreAuthorize("@ss.hasPermi('system:qrcode:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysScanQrcode cysScanQrcode)
    {
        startPage();
        List<CysScanQrcode> list = cysScanQrcodeService.selectCysScanQrcodeList(cysScanQrcode);
        return getDataTable(list);
    }

    /**
     * 导出扫码登录列表
     */
    @PreAuthorize("@ss.hasPermi('system:qrcode:export')")
    @Log(title = "扫码登录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysScanQrcode cysScanQrcode)
    {
        List<CysScanQrcode> list = cysScanQrcodeService.selectCysScanQrcodeList(cysScanQrcode);
        ExcelUtil<CysScanQrcode> util = new ExcelUtil<CysScanQrcode>(CysScanQrcode.class);
        util.exportExcel(response, list, "扫码登录数据");
    }

    /**
     * 获取扫码登录详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:qrcode:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysScanQrcodeService.selectCysScanQrcodeById(id));
    }

    /**
     * 新增扫码登录
     */
    @PreAuthorize("@ss.hasPermi('system:qrcode:add')")
    @Log(title = "扫码登录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysScanQrcode cysScanQrcode)
    {
        return toAjax(cysScanQrcodeService.insertCysScanQrcode(cysScanQrcode));
    }

    /**
     * 修改扫码登录
     */
    @PreAuthorize("@ss.hasPermi('system:qrcode:edit')")
    @Log(title = "扫码登录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysScanQrcode cysScanQrcode)
    {
        return toAjax(cysScanQrcodeService.updateCysScanQrcode(cysScanQrcode));
    }

    /**
     * 删除扫码登录
     */
    @PreAuthorize("@ss.hasPermi('system:qrcode:remove')")
    @Log(title = "扫码登录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysScanQrcodeService.deleteCysScanQrcodeByIds(ids));
    }
}
