package com.cys.api.league;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.league.member.domain.CysLeagueMember;
import com.cys.league.member.service.ICysLeagueMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 我的团员信息Controller
 * 
 * @author cys
 * @date 2023-09-27
 */
@RestController
@RequestMapping("/league/member")
public class CysLeagueMemberController extends BaseController
{
    @Autowired
    private ICysLeagueMemberService cysLeagueMemberService;

    /**
     * 查询我的团员信息列表
     */
    //@PreAuthorize("@ss.hasPermi('league:member:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysLeagueMember cysLeagueMember)
    {
        startPage();
        List<CysLeagueMember> list = cysLeagueMemberService.selectCysLeagueMemberList(cysLeagueMember);
        return getDataTable(list);
    }

    /**
     * 导出我的团员信息列表
     */
    //@PreAuthorize("@ss.hasPermi('league:member:export')")
    @Log(title = "我的团员信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysLeagueMember cysLeagueMember)
    {
        List<CysLeagueMember> list = cysLeagueMemberService.selectCysLeagueMemberList(cysLeagueMember);
        ExcelUtil<CysLeagueMember> util = new ExcelUtil<CysLeagueMember>(CysLeagueMember.class);
        util.exportExcel(response, list, "我的团员信息数据");
    }

    /**
     * 获取我的团员信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('league:member:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysLeagueMemberService.selectCysLeagueMemberById(id));
    }

    /**
     * 新增我的团员信息
     */
    //@PreAuthorize("@ss.hasPermi('league:member:add')")
    @Log(title = "我的团员信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysLeagueMember cysLeagueMember)
    {
        return toAjax(cysLeagueMemberService.insertCysLeagueMember(cysLeagueMember));
    }

    /**
     * 修改我的团员信息
     */
    //@PreAuthorize("@ss.hasPermi('league:member:edit')")
    @Log(title = "我的团员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysLeagueMember cysLeagueMember)
    {
        return toAjax(cysLeagueMemberService.updateCysLeagueMember(cysLeagueMember));
    }

    /**
     * 删除我的团员信息
     */
    //@PreAuthorize("@ss.hasPermi('league:member:remove')")
    @Log(title = "我的团员信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysLeagueMemberService.deleteCysLeagueMemberByIds(ids));
    }
}
