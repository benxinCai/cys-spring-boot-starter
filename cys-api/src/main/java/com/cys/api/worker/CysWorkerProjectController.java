package com.cys.api.worker;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.worker.log.domain.CysWorkerProject;
import com.cys.worker.log.service.ICysWorkerProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工时所需项目表Controller
 * 
 * @author cys
 * @date 2024-03-28
 */
@RestController
@RequestMapping("/worker/project")
public class CysWorkerProjectController extends BaseController
{
    @Autowired
    private ICysWorkerProjectService cysWorkerProjectService;

    /**
     * 查询工时所需项目表列表
     */
    @PreAuthorize("@ss.hasPermi('worker:project:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysWorkerProject cysWorkerProject)
    {
        startPage();
        List<CysWorkerProject> list = cysWorkerProjectService.selectCysWorkerProjectList(cysWorkerProject);
        return getDataTable(list);
    }

    /**
     * 导出工时所需项目表列表
     */
    @PreAuthorize("@ss.hasPermi('worker:project:export')")
    @Log(title = "工时所需项目表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysWorkerProject cysWorkerProject)
    {
        List<CysWorkerProject> list = cysWorkerProjectService.selectCysWorkerProjectList(cysWorkerProject);
        ExcelUtil<CysWorkerProject> util = new ExcelUtil<CysWorkerProject>(CysWorkerProject.class);
        util.exportExcel(response, list, "工时所需项目表数据");
    }

    /**
     * 获取工时所需项目表详细信息
     */
    @PreAuthorize("@ss.hasPermi('worker:project:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysWorkerProjectService.selectCysWorkerProjectById(id));
    }

    /**
     * 新增工时所需项目表
     */
    @PreAuthorize("@ss.hasPermi('worker:project:add')")
    @Log(title = "工时所需项目表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysWorkerProject cysWorkerProject)
    {
        return toAjax(cysWorkerProjectService.insertCysWorkerProject(cysWorkerProject));
    }

    /**
     * 修改工时所需项目表
     */
    @PreAuthorize("@ss.hasPermi('worker:project:edit')")
    @Log(title = "工时所需项目表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysWorkerProject cysWorkerProject)
    {
        return toAjax(cysWorkerProjectService.updateCysWorkerProject(cysWorkerProject));
    }

    /**
     * 删除工时所需项目表
     */
    @PreAuthorize("@ss.hasPermi('worker:project:remove')")
    @Log(title = "工时所需项目表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysWorkerProjectService.deleteCysWorkerProjectByIds(ids));
    }

    /**
     * 我的工时项目
     * @return
     */
    @GetMapping("/myWorkProject")
    public AjaxResult myWorkProject(){
        return success(cysWorkerProjectService.myWorkProject());
    }
}
