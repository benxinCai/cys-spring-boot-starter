package com.cys.api.worker;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.worker.log.domain.CysWorkerLog;
import com.cys.worker.log.service.ICysWorkerLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 工作日志Controller
 * 
 * @author cys
 * @date 2023-08-22
 */
@RestController
@RequestMapping("/worker/log")
public class CysWorkerLogController extends BaseController
{
    @Autowired
    private ICysWorkerLogService cysWorkerLogService;

    /**
     * 查询工作日志列表
     */
    @PreAuthorize("@ss.hasPermi('worker:log:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysWorkerLog cysWorkerLog)
    {
        startPage();
        List<CysWorkerLog> list = cysWorkerLogService.selectCysWorkerLogList(cysWorkerLog);
        return getDataTable(list);
    }

    /**
     * 导出工作日志列表
     */
    @PreAuthorize("@ss.hasPermi('worker:log:export')")
    @Log(title = "工作日志", businessType = BusinessType.EXPORT)
    //@PostMapping("/export")
    public void export(HttpServletResponse response, CysWorkerLog cysWorkerLog)
    {
        List<CysWorkerLog> list = cysWorkerLogService.selectCysWorkerLogList(cysWorkerLog);
        ExcelUtil<CysWorkerLog> util = new ExcelUtil<CysWorkerLog>(CysWorkerLog.class);
        util.exportExcel(response, list, "工作日志数据");
    }

    /**
     * 获取工作日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('worker:log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(cysWorkerLogService.selectCysWorkerLogById(id));
    }

    /**
     * 新增工作日志
     */
    @PreAuthorize("@ss.hasPermi('worker:log:add')")
    @Log(title = "工作日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody @Validated CysWorkerLog cysWorkerLog)
    {
        return toAjax(cysWorkerLogService.insertCysWorkerLog(cysWorkerLog));
    }

    /**
     * 修改工作日志
     */
    @PreAuthorize("@ss.hasPermi('worker:log:edit')")
    @Log(title = "工作日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysWorkerLog cysWorkerLog)
    {
        return toAjax(cysWorkerLogService.updateCysWorkerLog(cysWorkerLog));
    }

    /**
     * 删除工作日志
     */
    @PreAuthorize("@ss.hasPermi('worker:log:remove')")
    @Log(title = "工作日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cysWorkerLogService.deleteCysWorkerLogByIds(ids));
    }

    @PostMapping("/exportWorkLog")
    public void exportWorkLog(String time) throws IOException {
        cysWorkerLogService.exportWorkLog(time);
    }

    @PostMapping("/exportSign")
    public void exportSign(String time) throws IOException {
        cysWorkerLogService.exportSign(time);
    }



}
