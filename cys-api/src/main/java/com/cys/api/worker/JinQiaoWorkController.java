package com.cys.api.worker;

import com.cys.common.core.domain.AjaxResult;
import com.cys.worker.log.service.ICysWorkerLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/v1/jinqiao")
public class JinQiaoWorkController {

    @Autowired
    private ICysWorkerLogService cysWorkerLogService;

    /**
     * 查找user信息,主要是通过昵称查找;分页
     * @return
     */
    @GetMapping("/listWork")
    public AjaxResult listWork(String time){
        return cysWorkerLogService.selectMiNiWorker(time);
    }


    /**
     * 年度统计
     * @param time
     * @return
     */
    @GetMapping("/getYearStatisticalData")
    public AjaxResult getYearStatisticalData(String time){
        return cysWorkerLogService.getYearStatisticalData(time);
    }

    /**
     * 各项目组所花费的工时
     * @param time
     * @return
     */
    @GetMapping("/getEachProjectData")
    public AjaxResult getEachProjectData(String time){
        return cysWorkerLogService.getEachProjectData(time);
    }

    /**
     * 月度统计
     * @param time
     * @return
     */
    @GetMapping("/getMonthStatisticalData")
    public AjaxResult getMonthStatisticalData(String time){
        return cysWorkerLogService.getMonthStatisticalData(time);
    }

    /**
     * 获取今日横幅信息
     * @return
     */
    @GetMapping("/getNoticeText")
    public AjaxResult getNoticeText(){
        return cysWorkerLogService.getNoticeText();
    }

}
