package com.cys.api.system.common;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.log.StaticLog;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.cys.common.constant.Constants;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.core.domain.model.LoginUser;
import com.cys.common.core.redis.RedisCache;
import com.cys.common.utils.MessageUtils;
import com.cys.common.utils.SecurityUtils;
import com.cys.common.utils.ServletUtils;
import com.cys.common.utils.StringUtils;
import com.cys.framework.manager.AsyncManager;
import com.cys.framework.manager.factory.AsyncFactory;
import com.cys.framework.web.service.SysPermissionService;
import com.cys.framework.web.service.TokenService;
import com.cys.kaduo.qrcode.domain.CysScanQrcode;
import com.cys.kaduo.qrcode.handle.RedisHandle;
import com.cys.kaduo.qrcode.service.ICysScanQrcodeService;
import com.cys.other.facade.MyWechatService;
import com.cys.system.domain.SysUserDetail;
import com.cys.system.service.ISysRoleService;
import com.cys.system.service.ISysUserDetailService;
import com.cys.system.service.ISysUserService;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * web端扫码登录
 */
@RestController
@RequestMapping("/scan")
public class ScanLoginController {

    @Autowired
    private RedisCache redis;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private MyWechatService myWechatService;

    @Autowired
    private ISysUserDetailService userDetailService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ICysScanQrcodeService scanQrcodeService;

    private String login_param = "scan:login:";

    /**
     * 微信登录，当用户绑定openid后将自动使用小程序获取到的jsCode自动登录
     * 如果不存在用户，那么直接注册
     * 注册时随机生成用户名及账号，用户名可由小程序端自由更改
     * @param jsCode
     * @return
     */
    @Transactional
    @PostMapping("openIdLogin")
    public AjaxResult openIdLogin(@RequestBody String jsCode) {
        String openId = "";
        try{
            openId = myWechatService.getOpenIdByCode(jsCode);
        }catch(Exception e){
            return AjaxResult.success("无法获取OpenId", "1");
        }

        if(StringUtils.isEmpty(openId)){
            return AjaxResult.success("无法获取OpenId", "1");
        }

        Optional<SysUserDetail> detail = userDetailService.lambdaQuery().eq(SysUserDetail::getOpenId, openId).oneOpt();

        String token = "";

        if(detail.isPresent()){
            token = this.handleLogin(detail.get().getUserId());
        }else{
            SysUser sysUser = new SysUser();
            sysUser.setUserName("NK" + IdWorker.getIdStr());
            sysUser.setPassword(RandomUtil.randomString(10));
            sysUser.setStatus("0");
            sysUser.setNickName(sysUser.getUserName());
            sysUser.setPassword(SecurityUtils.encryptPassword(sysUser.getPassword()));
            userService.insertUser(sysUser);

            SysUserDetail temp = new SysUserDetail();
            temp.setUserId(sysUser.getUserId());
            temp.setOpenId(openId);
            userDetailService.save(temp);

            roleService.authRole("common", sysUser.getUserId());//授权所有角色基础权限

            token = this.handleLogin(sysUser.getUserId());

            AsyncManager.me().execute(AsyncFactory.recordLogininfor(sysUser.getUserName(), Constants.REGISTER, MessageUtils.message("user.register.success")));
        }
        return AjaxResult.success("", token);
    }

    private String handleLogin(Long userId){
        SysUser user = userService.selectUserById(userId);
        return tokenService.createToken(new LoginUser(user.getUserId(), user.getDeptId(), user, permissionService.getMenuPermission(user)));
    }

    @GetMapping("getOpenId")
    public AjaxResult getOpenId(String jsCode) throws WxErrorException {
        return AjaxResult.success(myWechatService.getOpenIdByCode(jsCode));
    }


    //pc端或其他端， 获取登录二维码，默认二维码60秒有效，过了60秒后过期，uuid标识存于消息头abcd
    @RequestMapping(value = "/qrcode" ,method = RequestMethod.GET)
    public void createCodeImg(HttpServletResponse response){
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");

        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");

        try {
            String key = this.getKey();
            String uuid = key.substring(12);

            CysScanQrcode qrcode = scanQrcodeService.lambdaQuery().eq(CysScanQrcode::getUuid, uuid).one();

            RedisHandle.getRedis().setCacheObject(login_param + uuid, "false", 120, TimeUnit.SECONDS);//60秒扫描时间
            response.setHeader("abcd", uuid);
            byte[] images = qrcode.getImages();
            InputStream in = new ByteArrayInputStream(images);
            RedisHandle.getRedis().deleteObject(key);//删掉key,防止再次使用
            ImgUtil.write(ImageIO.read(in), "jpg", response.getOutputStream());
        } catch (Exception e) {
            StaticLog.error("create login qrcode is error，" + e.getMessage());
        }
    }

    private String getKey(){
        Collection<String> keys = RedisHandle.getRedis().keys("scan:qrcode:*");//找一个可以用的key,value值不重要
        Optional<String> optional = keys.stream().findFirst();
        if(optional.isPresent()){
            return keys.stream().findFirst().get();
        }else{
            RedisHandle.builder().handleScanQrcode();
        }
        return this.getKey();//递归调用,不再重复写逻辑了
    }

    /**
     * 用户成功扫描二维码,此接口需要确认端登录
     * 1、判断过没过期
     * 2、判断有没有token，能不能解析出用户数据(暂时不需要,此接口需要游登录操作)
     * 3、设置确认时间60秒
     * @return
     */
    @RequestMapping(value = "/scanQrCode" ,method = RequestMethod.GET)
    public AjaxResult scanQrCode(String u){
        if(StringUtils.isEmpty(u)){
            return AjaxResult.error("不存在u");
        }

        String redisValue = redis.getCacheObject(login_param + u);
        if(StringUtils.isEmpty(redisValue)){
            return AjaxResult.error("二维码已过期,请刷新后重新扫描。");
        }

//        if(redisValue.equals("confirm")){//确认阶段
//            return AjaxResult.error("此二维码已被扫描,请勿重复扫描。");
//        }

        redis.setCacheObject(login_param + u, "confirm", 120, TimeUnit.SECONDS);//120秒确认时间

        return AjaxResult.success();
    }

    //u是二维码标识
    @RequestMapping(value = "/confirmLogin" ,method = RequestMethod.GET)
    public AjaxResult confirmLogin(String u){
        if(StringUtils.isEmpty(u)){
            return AjaxResult.error("不存在u");
        }

        String redisValue = redis.getCacheObject(login_param + u);
        if(StringUtils.isEmpty(redisValue)){
            return AjaxResult.error("二维码已过期,请刷新后重新扫描。");
        }

        if(redisValue.equals("confirm")){//当前状态只有是确认，才能给与登录操作
            //给与前端60秒的扫描时间
            redis.setCacheObject(login_param + u, tokenService.getToken(ServletUtils.getRequest()), 120, TimeUnit.SECONDS);
            return AjaxResult.success("扫描成功");
        }

        return AjaxResult.warn("当前状态为非确认状态，无法登陆，请刷新后重新扫码登录。");
    }

    @RequestMapping(value = "/checkLogin" ,method = RequestMethod.GET)
    public AjaxResult checkLogin(String u){

        String redisValue = redis.getCacheObject(login_param + u);

        if(StringUtils.isEmpty(redisValue)){
            return AjaxResult.success("二维码已过期，请刷新后重新扫描", "1");
        }

        if(redisValue.equals("false")){
            return AjaxResult.success("未扫描", "2");
        }

        if(redisValue.equals("confirm")){
            return AjaxResult.success("已扫描，待确认", "3");
        }

        return AjaxResult.success("登陆成功", redisValue);
    }

}
