package com.cys.api.system.common;


import com.cys.common.core.domain.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 切换windows的Ip获取方式
 * 1、DHCP
 * 2、static
 */
@RestController
@RequestMapping("/network")
public class NetWorkController {

    private static String[] test = {"cmd /c start", "d:", "cd a", "./a.bat"};
    private static String dhcpNetWork = "cmd /c netsh interface ip set address \"以太网\" dhcp";
    private static String staticNetWork = "netsh interface ip set address \"以太网\" static 10.114.229.70 255.255.255.0 10.114.229.1 1";

    @GetMapping("/getLocalIp")
    public AjaxResult getLocalIp() throws UnknownHostException {
        return AjaxResult.success(InetAddress.getLocalHost().getHostAddress());
    }

    /**
     * 以下内容只针对金桥网络，其他地方，切完了就没网了。
     * 本地网络名称叫 "以太网"
     * 切换成dhcp模式：netsh interface ip set address "以太网" dhcp
     * 切换成固定IP模式：netsh interface ip set address "以太网" static 10.114.229.70 255.255.255.0 10.114.229.1 1
     * @return
     */
    @PostMapping("/convertNetWork")
    public AjaxResult convertNetWork(){
        return AjaxResult.success();
    }

    public static void main(String[] args) throws IOException, InterruptedException {

//        String[] cmd = new String[] {"runas", "/profile", "/user:DELL", "/savecred", dhcpNetWork };
//
//        Process process = Runtime.getRuntime().exec(cmd);
//        // 读取命令输出结果
//        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "gbk"));
//        String line;
//        while ((line = reader.readLine()) != null) {
//            System.out.println(line);
//        }
//        // 销毁process对象
//        process.destroy();


        test();

    }

    public static void test() throws IOException {
        String[] cmd1 = new String[] {"cmd.exe", "/c", "start", "runas", "/user:DELL", dhcpNetWork };
        Process process = Runtime.getRuntime().exec(test);
        // 读取命令输出结果
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "gbk"));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        // 销毁process对象
        process.destroy();
    }
}
