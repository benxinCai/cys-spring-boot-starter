package com.cys.api.tools;

import cn.hutool.core.util.ObjectUtil;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.utils.SecurityUtils;
import com.cys.tools.password.domain.CysManagePassword;
import com.cys.tools.password.service.ICysManagePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 我的工具-----密码管理控制类
 */
@RestController
@RequestMapping("/v1/myTools/password")
public class MyToolsPasswordController extends BaseController {

    @Autowired
    private ICysManagePasswordService managePasswordService;

    //按照分页获取密码列表
    @GetMapping("/listPassword")
    public TableDataInfo listPassword(){
        startPage();
        List<CysManagePassword> managePasswordList = managePasswordService.selectCysManagePasswordList(new CysManagePassword());
        return getDataTable(managePasswordList);
    }

    //根据密码ID获取密码
    @GetMapping("/getPassword")
    public AjaxResult getPassword(String id){

        Long userId = SecurityUtils.getUserId();
        CysManagePassword password = managePasswordService.lambdaQuery()
                .eq(CysManagePassword::getId, id)
                .eq(CysManagePassword::getUserId, userId)
                .one();
        if(ObjectUtil.isNull(password)){
            return AjaxResult.error("不存在密码，请刷新后重试.");
        }
        return AjaxResult.success(password.getPassword());
    }


}
