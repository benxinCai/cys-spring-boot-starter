package com.cys.api.tools;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.tools.password.domain.CysManagePassword;
import com.cys.tools.password.service.ICysManagePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 密码管理Controller
 * 
 * @author cys
 * @date 2024-05-15
 */
@RestController
@RequestMapping("/tool/password")
public class CysManagePasswordController extends BaseController
{
    @Autowired
    private ICysManagePasswordService cysManagePasswordService;

    /**
     * 查询密码管理列表
     */
    //@PreAuthorize("@ss.hasPermi('tool:password:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysManagePassword cysManagePassword)
    {
        startPage();
        List<CysManagePassword> list = cysManagePasswordService.selectCysManagePasswordList(cysManagePassword);
        return getDataTable(list);
    }

    /**
     * 修改授权密码
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @GetMapping("/modifyAuthPassword")
    public AjaxResult modifyAuthPassword(String oldPassword, String newPassword){
        return cysManagePasswordService.modifyAuthPassword(oldPassword, newPassword);
    }

    /**
     * 查看密码
     * @param id
     * @param authPassword
     * @return
     */
    @GetMapping("/checkAuth")
    public AjaxResult checkAuth(String id, String authPassword)
    {
        return cysManagePasswordService.checkAuth(id, authPassword);
    }

    /**
     * 导出密码管理列表
     */
    //@PreAuthorize("@ss.hasPermi('tool:password:export')")
    @Log(title = "密码管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysManagePassword cysManagePassword)
    {
        List<CysManagePassword> list = cysManagePasswordService.selectCysManagePasswordList(cysManagePassword);
        ExcelUtil<CysManagePassword> util = new ExcelUtil<CysManagePassword>(CysManagePassword.class);
        util.exportExcel(response, list, "密码管理数据");
    }

    /**
     * 获取密码管理详细信息
     */
    //@PreAuthorize("@ss.hasPermi('tool:password:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysManagePasswordService.selectCysManagePasswordById(id));
    }

    /**
     * 新增密码管理
     */
    //@PreAuthorize("@ss.hasPermi('tool:password:add')")
    @Log(title = "密码管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysManagePassword cysManagePassword)
    {
        return toAjax(cysManagePasswordService.insertCysManagePassword(cysManagePassword));
    }

    /**
     * 修改密码管理
     */
    //@PreAuthorize("@ss.hasPermi('tool:password:edit')")
    @Log(title = "密码管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysManagePassword cysManagePassword)
    {
        return toAjax(cysManagePasswordService.updateCysManagePassword(cysManagePassword));
    }

    /**
     * 删除密码管理
     */
    //@PreAuthorize("@ss.hasPermi('tool:password:remove')")
    @Log(title = "密码管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysManagePasswordService.deleteCysManagePasswordByIds(ids));
    }
}
