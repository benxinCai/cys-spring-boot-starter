package com.cys.api.tools;

import com.cys.common.annotation.Log;
import com.cys.common.core.controller.BaseController;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.page.TableDataInfo;
import com.cys.common.enums.BusinessType;
import com.cys.common.utils.poi.ExcelUtil;
import com.cys.kaduo.sign.domain.CysSign;
import com.cys.kaduo.sign.service.ICysSignService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 签到管理Controller
 * 
 * @author cys
 * @date 2023-10-19
 */
@RestController
@RequestMapping("/system/sign")
public class CysSignController extends BaseController
{
    @Autowired
    private ICysSignService cysSignService;

    /**
     * 查询签到管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:sign:list')")
    @GetMapping("/list")
    public TableDataInfo list(CysSign cysSign)
    {
        startPage();
        List<CysSign> list = cysSignService.selectCysSignList(cysSign);
        return getDataTable(list);
    }

    /**
     * 导出签到管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:sign:export')")
    @Log(title = "签到管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CysSign cysSign)
    {
        List<CysSign> list = cysSignService.selectCysSignList(cysSign);
        ExcelUtil<CysSign> util = new ExcelUtil<CysSign>(CysSign.class);
        util.exportExcel(response, list, "签到管理数据");
    }

    /**
     * 获取签到管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:sign:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(cysSignService.selectCysSignById(id));
    }

    /**
     * 新增签到管理
     */
    @PreAuthorize("@ss.hasPermi('system:sign:add')")
    @Log(title = "签到管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CysSign cysSign)
    {
        return toAjax(cysSignService.insertCysSign(cysSign));
    }

    /**
     * 修改签到管理
     */
    @PreAuthorize("@ss.hasPermi('system:sign:edit')")
    @Log(title = "签到管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CysSign cysSign)
    {
        return toAjax(cysSignService.updateCysSign(cysSign));
    }

    /**
     * 删除签到管理
     */
    @PreAuthorize("@ss.hasPermi('system:sign:remove')")
    @Log(title = "签到管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(cysSignService.deleteCysSignByIds(ids));
    }
}
