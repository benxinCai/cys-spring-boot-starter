package com.cys.api.tools;

import com.cys.base.holiday.service.ICysHolidayService;
import com.cys.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author cys
 * @since 2023-07-21
 */
@RestController
@RequestMapping("/holiday")
public class CysHolidayController {

    @Autowired
    private ICysHolidayService cysHolidayService;

    @GetMapping("/jinQiaoWorkHoliday")
    public AjaxResult listJinQiaoWorkHoliday(String time){

        return cysHolidayService.jinQiaoWorkHoliday(time);
    }

}
