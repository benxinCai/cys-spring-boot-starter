package com.cys.api.tools;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.utils.SecurityUtils;
import com.cys.kaduo.sign.domain.CysSign;
import com.cys.kaduo.sign.service.ICysSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 签到模块
 */
@RestController
@RequestMapping("/v1/sign")
public class SignController {

    @Autowired
    private ICysSignService cysSignService;

    /**
     * 签到
     * @return
     */
    @PostMapping("/sign")
    public AjaxResult sign(){
        return cysSignService.sign();
    }

    /**
     * 今日是否签到
     * @return
     */
    @GetMapping("/isSignDay")
    public AjaxResult isSignDay(){
        Long userId = SecurityUtils.getUserId();

        QueryWrapper<CysSign> queryWrapper = new QueryWrapper<CysSign>();
        queryWrapper.apply("to_days(sign_date) = to_days(now())").lambda().eq(CysSign::getUserId, userId);
        List<CysSign> signList = cysSignService.list(queryWrapper);

        if(CollUtil.isNotEmpty(signList)) {
            return AjaxResult.success(1);
        }
        return AjaxResult.success(0);//0是可以签到
    }

    /**
     * 统计数据
     * @return
     */
    @GetMapping("/getSignData")
    public AjaxResult getSignData(){
        Integer continuousSignCount = cysSignService.continuousSignCount();//连续签到
        Long signCount = cysSignService.signCount();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("continuousSignCount", continuousSignCount);
        data.put("signCount", signCount);
        return AjaxResult.success(data);
    }

}
