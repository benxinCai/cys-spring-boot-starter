package com.cys.common.core.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("httpProxyUtil")
public class HttpProxyUtil {

    @Value("${proxy.enabled}")
    private boolean enabled;
    @Value("${proxy.url}")
    private String url;
    @Value("${proxy.port}")
    private Integer port;

    public boolean isEnabled(){
        return enabled;
    }

    public String getUrl(){
        return url;
    }

    public Integer getPort(){
        return port;
    }

    public static HttpProxyUtil getThis(){
        return SpringBeanUtil.getBean(HttpProxyUtil.class);
    }

}
