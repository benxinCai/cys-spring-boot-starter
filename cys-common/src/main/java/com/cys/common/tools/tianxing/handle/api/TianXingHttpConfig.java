package com.cys.common.tools.tianxing.handle.api;


import cn.hutool.http.HttpConfig;
import com.cys.common.core.util.HttpProxyUtil;

/**
 * 航海王起航-小程序抓包自动化工具
 */
public class TianXingHttpConfig {

    public static HttpConfig getConfig() {
        HttpConfig httpConfig = new HttpConfig();

        HttpProxyUtil httpProxyUtil = HttpProxyUtil.getThis();
        if (httpProxyUtil.isEnabled()) {
            httpConfig.setHttpProxy(httpProxyUtil.getUrl(), httpProxyUtil.getPort());
        }

        //httpConfig.setHttpProxy("proxy.dq.petrochina", 8080);
        httpConfig.setReadTimeout(5000);
        return httpConfig;
    }

}
