package com.cys.common.tools.tianxing.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 *
 * @author cys
 * @version v1.0
 * @className TianXingHoliday
 * @date 2024/12/30
 * @description TODO
 */
@Data
public class TianXingHoliday {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "当前阳历日期")
    private String date;

    @ApiModelProperty(value = "日期类型，为0表示工作日、为1节假日、为2双休日、3为调休日（上班）")
    private String daycode;

    @ApiModelProperty(value = "星期（数字）")
    private String weekday;

    @ApiModelProperty(value = "星期（中文）")
    private String cnweekday;

    @ApiModelProperty(value = "文字提示，工作日、节假日、节日、双休日、调休日")
    private String info;

    @ApiModelProperty(value = "节假日名称（中文）")
    private String name;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

}
