package com.cys.common.tools.tianxing.handle.api;

import cn.hutool.http.HttpRequest;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson2.JSONObject;
import com.cys.common.tools.tianxing.constant.TianXingUrl;
import com.cys.common.tools.tianxing.entity.TianXingHoliday;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 天行api调用接口
 */
public class TianXingApi {

    private final static String API_KEY = "5da9071e45ae62aec67efc7d4839e6bc";

    public static List<TianXingHoliday> listHolidayByMouth(String date){
        Map<String, Object> paramMap = new HashMap<>();

        paramMap.put("key", API_KEY);
        paramMap.put("type", "2");
        paramMap.put("date", date);

        String result = HttpRequest.get(TianXingUrl.LIST_HOLIDAY)
                .setConfig(TianXingHttpConfig.getConfig())
                .form(paramMap)
                .execute().body();


        try{
            return JSONObject.parseObject(result).getJSONObject("result").getJSONArray("list").toList(TianXingHoliday.class);
        }catch(Exception e){
            StaticLog.error(e);
        }
        return new ArrayList<>();
    }

}
