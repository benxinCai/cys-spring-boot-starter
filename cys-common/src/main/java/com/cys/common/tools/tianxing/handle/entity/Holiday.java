package com.cys.common.tools.tianxing.handle.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author cys
 * @since 2023-07-21
 */
@Data
public class Holiday implements Serializable {

    private static final long serialVersionUID = 1L;

    private String date;

    private String daycode;

    private String weekday;

    private String cnweekday;

    private String info;

    private String name;

    private Date createDate;


}
