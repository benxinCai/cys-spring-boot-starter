package com.cys.scheduled.system;

import cn.hutool.core.date.DateUtil;
import com.cys.common.core.domain.entity.SysRole;
import com.cys.kaduo.apply.domain.CysAuthApply;
import com.cys.kaduo.apply.service.ICysAuthApplyService;
import com.cys.kaduo.notice.adapter.NoticeAdapter;
import com.cys.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service("systemScheduled")
public class SystemScheduledService {

    @Autowired
    private ICysAuthApplyService cysAuthApplyService;

    @Autowired
    private ISysRoleService roleService;

    /**
     * 处理申请单数据，删掉无用的
     */
    public void handleApplyData(){
        cysAuthApplyService.deleteRecentApplyData();
    }

    /**
     * 分析数据发送给镜花水月
     * 1、当前有多少待处理的权限申请单，各个申请单角色的详情
     * 2、当前系统每类角色下有多少个用户
     * 3、
     */
    public void handleAnalyzeData(){

        String yesterday = DateUtil.format(new Date(), "yyyy-MM-dd");
        String title = yesterday + "系统运行数据";


        //1、待处理权限数据内容
        List<CysAuthApply> applyList =  cysAuthApplyService.lambdaQuery().isNull(CysAuthApply::getHandleResult).list();
        StringBuffer sb = new StringBuffer();
        sb.append("1、当前待处理的权限申请单共有:").append(applyList.size()).append("个。");
        if(applyList.size() != 0){
            sb.append("其中:\n");
            applyList.stream().collect(Collectors.groupingBy(CysAuthApply::getRoleName)).forEach((key, value) -> {
                sb.append("\t[").append(key).append("]").append("权限有").append(value.size()).append("个待处理\n");
            });
        }else{
            sb.append("\n");
        }


        //2、每类角色下面有多少人
        List<SysRole> roleList = roleService.selectRoleListByAnalyze();
        sb.append("2、当前共计已分配角色:" + roleList.size()).append("个。其中:\n");
        roleList.stream().collect(Collectors.groupingBy(SysRole::getRoleName)).forEach((key, value) -> {
            sb.append("\t拥有[").append(key).append("]").append("角色的用户共计").append(value.size()).append("个\n");
        });
        sb.append("\n");

        List<Long> userIds = new ArrayList<>();
        userIds.add(5L);//我的数据

        userIds.forEach(userId -> {
            NoticeAdapter.builder()
                    .setTitle(title)
                    .setType("系统运行数据")
                    .setContent(sb.toString())
                    .setSendUserId(userId)
                    .sendNotice();
        });
    }


}
