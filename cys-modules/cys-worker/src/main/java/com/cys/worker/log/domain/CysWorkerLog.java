package com.cys.worker.log.domain;

import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 工作日志对象 cys_worker_log
 * 
 * @author cys
 * @date 2023-08-22
 */
@Data
@ApiModel(value="cys_worker_log对象", description="")
@Accessors(chain = true)
@TableName("cys_worker_log")
public class CysWorkerLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId
    private String id;

    /** 用户id */
    @ApiModelProperty(value = "用户ID")
    @TableField("user_id")
    private Long userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    @ApiModelProperty(value = "用户名称")
    @TableField("user_name")
    private String userName;

    /** 日志内容 */
    @Excel(name = "日志内容")
    @ApiModelProperty(value = "日志内容")
    @NotEmpty(message = "请填写日志内容")
    @TableField("content")
    private String content;

    /** 是否补签日志 */
    @ApiModelProperty(value = "日志内容")
    @TableField("amends")
    private String amends;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    @TableField("create_date")
    private Date createDate;

    /** 签到时间,非补签状态下就是当前日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "签到时间,非补签状态下就是当前日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "签到时间,非补签状态下就是当前日期")
    @TableField("sign_date")
    @NotNull(message = "请输入工作时间")
    private Date signDate;

    /** 工作地点 */
    @Excel(name = "工作地点")
    @ApiModelProperty(value = "工作地点")
    @TableField("work_location")
    @NotEmpty(message = "请输入工作地点")
    private String workLocation;

    /** 工作方式 */
    @Excel(name = "工作方式")
    @ApiModelProperty(value = "工作方式")
    @TableField("work_type")
    @NotEmpty(message = "请输入工作方式")
    private String workType;

    /** 工作小时数 */
    @Excel(name = "工作小时数")
    @ApiModelProperty(value = "工作小时数")
    @NotEmpty(message = "请输入工作小时数")
    @TableField("work_hour")
    private String workHour;

    /** 日工时汇总 */
    @Excel(name = "日工时汇总")
    @ApiModelProperty(value = "日工时汇总")
    @TableField("daywork_hours")
    @NotEmpty(message = "请输入日工时汇总")
    private String dayworkHours;

    /** 项目人天 */
    @Excel(name = "项目人天")
    @ApiModelProperty(value = "项目人天")
    @TableField("project_person_day")
    @NotEmpty(message = "请输入项目人天")
    private String projectPersonDay;

    @TableField("tags")
    private String tags;

    @TableField("current_project_name")
    @ApiModelProperty(value = "项目名称")
    private String currentProjectName;

    @TableField("current_project_code")
    @ApiModelProperty(value = "项目编码")
    private String currentProjectCode;

    @TableField(exist = false)
    private JSONArray tagsArray;

    @TableField(exist = false)
    private String handleSignDate;

}
