package com.cys.worker.log.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.worker.log.domain.CysWorkerProject;

/**
 * 工时所需项目表Mapper接口
 * 
 * @author cys
 * @date 2024-03-28
 */
public interface CysWorkerProjectMapper extends BaseMapper<CysWorkerProject> {

}
