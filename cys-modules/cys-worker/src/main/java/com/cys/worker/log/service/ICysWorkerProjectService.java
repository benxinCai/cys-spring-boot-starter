package com.cys.worker.log.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.worker.log.domain.CysWorkerProject;

/**
 * 工时所需项目表Service接口
 * 
 * @author cys
 * @date 2024-03-28
 */
public interface ICysWorkerProjectService  extends IService<CysWorkerProject>
{
    /**
     * 查询工时所需项目表
     * 
     * @param id 工时所需项目表主键
     * @return 工时所需项目表
     */
    public CysWorkerProject selectCysWorkerProjectById(String id);

    /**
     * 查询工时所需项目表列表
     * 
     * @param cysWorkerProject 工时所需项目表
     * @return 工时所需项目表集合
     */
    public List<CysWorkerProject> selectCysWorkerProjectList(CysWorkerProject cysWorkerProject);

    /**
     * 新增工时所需项目表
     * 
     * @param cysWorkerProject 工时所需项目表
     * @return 结果
     */
    public int insertCysWorkerProject(CysWorkerProject cysWorkerProject);

    /**
     * 修改工时所需项目表
     * 
     * @param cysWorkerProject 工时所需项目表
     * @return 结果
     */
    public int updateCysWorkerProject(CysWorkerProject cysWorkerProject);

    /**
     * 批量删除工时所需项目表
     * 
     * @param ids 需要删除的工时所需项目表主键集合
     * @return 结果
     */
    public int deleteCysWorkerProjectByIds(String[] ids);

    /**
     * 删除工时所需项目表信息
     * 
     * @param id 工时所需项目表主键
     * @return 结果
     */
    public int deleteCysWorkerProjectById(String id);

    //我的工时项目
    public List<CysWorkerProject> myWorkProject();
}
