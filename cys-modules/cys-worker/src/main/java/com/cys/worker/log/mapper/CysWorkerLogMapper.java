package com.cys.worker.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.worker.log.domain.CysWorkerLog;

/**
 * 工作日志Mapper接口
 * 
 * @author cys
 * @date 2023-08-22
 */
public interface CysWorkerLogMapper extends BaseMapper<CysWorkerLog> {

}
