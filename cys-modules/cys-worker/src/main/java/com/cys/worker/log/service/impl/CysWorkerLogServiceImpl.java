package com.cys.worker.log.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.base.holiday.domain.CysHoliday;
import com.cys.base.holiday.service.ICysHolidayService;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.exception.base.BaseException;
import com.cys.common.utils.DateUtils;
import com.cys.common.utils.SecurityUtils;
import com.cys.common.utils.StringUtils;
import com.cys.worker.log.domain.CysWorkerLog;
import com.cys.worker.log.domain.CysWorkerProject;
import com.cys.worker.log.mapper.CysWorkerLogMapper;
import com.cys.worker.log.service.ICysWorkerLogService;
import com.cys.worker.log.service.ICysWorkerProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 工作日志Service业务层处理
 * 
 * @author cys
 * @date 2023-08-22
 */
@Service
public class CysWorkerLogServiceImpl extends ServiceImpl<CysWorkerLogMapper, CysWorkerLog> implements ICysWorkerLogService
{

    @Autowired
    private CysWorkLogExcel workLogExcel;

    @Autowired
    private CysSignExcel signExcel;

    @Autowired
    private ICysHolidayService holidayService;

    @Autowired
    private ICysWorkerProjectService workerProjectService;

    @Autowired
    private ICysWorkerProjectService projectService;

    /**
     * 查询工作日志
     * 
     * @param id 工作日志主键
     * @return 工作日志
     */
    @Override
    public CysWorkerLog selectCysWorkerLogById(Long id)
    {
        return this.getById(id);
    }

    /**
     * 考勤内容
     *
     * 判断当前日期范围，
     * 1、这个日期是一个过去的月份，那就用假期数据全部展示即可
     * 2、这个日期是当前月，那就用假期数据全部展示即可
     * 3、如果这个日期是超前月，那就展示已写日志的月份吧
     *
     * @param time
     * @return
     */
    public AjaxResult selectMiNiWorker(String time){

        //当月所有日志
        Map<String, List<CysWorkerLog>> logs = this.lambdaQuery()
                .likeRight(CysWorkerLog::getSignDate, time)
                .eq(CysWorkerLog::getUserId, SecurityUtils.getUserId())
                .list()
                .stream()
                .map(e->{
                    e.setHandleSignDate(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD, e.getSignDate()));
                    return e;
                }).collect(Collectors.groupingBy(CysWorkerLog::getHandleSignDate));

        //当月所有假期数据
        List<CysHoliday> cysHolidayList = holidayService.lambdaQuery()
                .likeRight(CysHoliday::getDate, time)
                .orderByDesc(CysHoliday::getDate)
                .list();

        //处理数据,按照当月日期一条一条处理。
        List<JSONObject> resultData = cysHolidayList.stream()
                .map(day -> {
                    JSONObject jsonObject = this.initJson();
                    jsonObject.put("signDate", day.getDate());
                    if(Arrays.asList("0", "3").contains(day.getDaycode())){//正常上班,0是工作日,3是调休上班;
                        jsonObject.put("isWork", true);
                    }else{//放假
                        jsonObject.put("isWork", false);
                    }
                    List<CysWorkerLog> cysWorkerLogList = logs.get(day.getDate());
                    if(CollUtil.isNotEmpty(cysWorkerLogList)){
                        StringBuffer sb = new StringBuffer();
                        for(int i = 1; i <= cysWorkerLogList.size(); i++){
                            sb.append(i).append("、").append(cysWorkerLogList.get(i-1).getContent());
                            if(i != cysWorkerLogList.size()){
                                sb.append("<br/>");
                            }
                        }
                        jsonObject.put("content", sb.toString());
                        CysWorkerLog log = cysWorkerLogList.get(0);
                        jsonObject.put("workLocation", log.getWorkLocation());
                        jsonObject.put("workType", log.getWorkType());
                        jsonObject.put("tags", JSON.parseArray(log.getTags()));
                        jsonObject.put("remove", true);
                    }else{
                        if(this.passCurrentDate(day.getDate())){//超过今天，不需要展示了
                            jsonObject.put("remove", false);
                        }else{

                            JSONArray jsonArray = holidayService.jinQiaoWorkHolidayBase(day.getDate());//把加班标签干掉，只要没有工作日志，就不需要有加班标签
                            for(JSONObject json : jsonArray.toList(JSONObject.class)){
                                if("加班".equals(json.getString("label"))){
                                    jsonArray.remove(json);
                                    break;
                                }
                            }

                            //上班没有工作日志的话提示补充，放假没有工作日志的话提示好好休息
                            if(jsonObject.getBoolean("isWork")){//正常上班
                                jsonObject.put("content", "没有填写日志,请尽快补充.");
                            }else{//放假
                                jsonObject.put("content", "今日放假,请好好休息吧.");
                            }

                            jsonObject.put("tags", jsonArray);
                            jsonObject.put("remove", true);
                        }
                    }
                    return jsonObject;
                }).filter(e -> e.getBoolean("remove")).collect(Collectors.toList());

        //对结果数据的处理，当日期超过今天时，没有日志的就给过滤掉
        //对于结果没有超过今天的，假期可以没有日志，上班没有日志需要给提示
        //最终都是对今天作为临界点进行处理。

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("logs", resultData);
        resultMap.put("count", logs.size());

        return AjaxResult.success(resultMap);
    }

    /**
     * 获取选择年的全部信息
     *
     * 1、优先取出今年所有日志
     * 2、取出今年的所有日期
     * 3、按组分类
     *
     * @param time
     * @return
     */
    public AjaxResult getYearStatisticalData(String time){
        Long userId = SecurityUtils.getUserId();
        String yearly = StrUtil.sub(time, 0, 4);

        Map<String, Object> yearlyData = new HashMap<>();
        //当月所有日志,并且按照签到时间分组。
        Map<String, List<CysWorkerLog>> logs = this.lambdaQuery()
                .likeRight(CysWorkerLog::getSignDate, yearly)
                .eq(CysWorkerLog::getUserId, userId)
                .list()
                .stream()
                .map(e->{
                    e.setHandleSignDate(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD, e.getSignDate()));
                    return e;
                }).collect(Collectors.groupingBy(CysWorkerLog::getHandleSignDate));

        //当年所有假期数据
        List<CysHoliday> holidayList = holidayService.lambdaQuery()
                .likeRight(CysHoliday::getDate, yearly)
                .orderByDesc(CysHoliday::getDate)
                .list();

        //非工作日
        List<String> notWeekDay = holidayList.stream()
                .filter(item->!item.getDaycode().equals("0"))
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());

        //工作日
        List<String> weekDay = holidayList.stream()
                .filter(item->item.getDaycode().equals("0"))
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());

        yearlyData.put("allDay", holidayList.size());//今年一共多少天
        yearlyData.put("weekDay", weekDay.size());//共有多少个工作日
        yearlyData.put("workDay", logs.size());//年度已工作天数
        yearlyData.put("addWorkDay", logs.keySet().stream().filter(item-> notWeekDay.contains(item)).count());//加班天数:非工作日上班个数

        Date nowDate = new Date();

        //今天之前的日期
        List<String> beforeAllDay = holidayList.stream()
                .filter(item-> {
                    Date holdayDate = DateUtil.parse(item.getDate(), "yyyy-MM-dd");
                    return DateUtil.compare(nowDate, holdayDate) > 0;
                })
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());

        yearlyData.put("restDay", beforeAllDay.stream().filter(item->!logs.containsKey(item)).count());//休息天数:不包括今天，之前所有日期内没有记录日志的都算没上班，就是休息

        //今天之前的工作日日期没有写日志的都是请假
        List<String> beforeWorkDay = holidayList.stream()
                .filter(item->item.getDaycode().equals("0"))
                .filter(item-> {
                    Date holdayDate = DateUtil.parse(item.getDate(), "yyyy-MM-dd");
                    return DateUtil.compare(nowDate, holdayDate) > 0;
                })
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());//工作日数据
        yearlyData.put("leaveDay", beforeWorkDay.stream().filter(item->!logs.containsKey(item)).count());//请假天数:不包括今天，之前所有工作日内没有记录日志的都算没上班,就是请假

        return AjaxResult.success(yearlyData);
    }

    public AjaxResult getMonthStatisticalData(String time) {
        Long userId = SecurityUtils.getUserId();
        Map<String, Object> monthData = new HashMap<>();

        //当月所有日志,并且按照签到时间分组。
        Map<String, List<CysWorkerLog>> logs = this.lambdaQuery()
                .likeRight(CysWorkerLog::getSignDate, time)
                .eq(CysWorkerLog::getUserId, userId)
                .list()
                .stream()
                .map(e->{
                    e.setHandleSignDate(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD, e.getSignDate()));
                    return e;
                }).collect(Collectors.groupingBy(CysWorkerLog::getHandleSignDate));

        //当月所有假期数据
        List<CysHoliday> holidayList = holidayService.lambdaQuery()
                .likeRight(CysHoliday::getDate, time)
                .orderByDesc(CysHoliday::getDate)
                .list();

        //非工作日
        List<String> notWeekDay = holidayList.stream()
                .filter(item->!item.getDaycode().equals("0"))
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());

        //工作日
        List<String> weekDay = holidayList.stream()
                .filter(item->item.getDaycode().equals("0"))
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());

        monthData.put("allDay", holidayList.size());//本月一共多少天
        monthData.put("weekDay", weekDay.size());//共有多少个工作日
        monthData.put("workDay", logs.size());//月度
        monthData.put("addWorkDay", logs.keySet().stream().filter(item-> notWeekDay.contains(item)).count());//加班天数:非工作日上班个数

        Date nowDate = new Date();

        String date = DateUtil.format(new Date(), "yyyy-MM-dd");

        //今天之前的日期
        List<String> beforeAllDay = holidayList.stream()
                .filter(item-> !StrUtil.contains(item.getDate(), date))
                .filter(item-> {
                    Date holdayDate = DateUtil.parse(item.getDate(), "yyyy-MM-dd");
                    return DateUtil.compare(nowDate, holdayDate) > 0;
                })
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());

        monthData.put("restDay", beforeAllDay.stream().filter(item->!logs.containsKey(item)).count());//休息天数:不包括今天，之前所有日期内没有记录日志的都算没上班，就是休息

        //今天之前的工作日日期没有写日志的都是请假
        List<String> beforeWorkDay = holidayList.stream()
                .filter(item-> !StrUtil.contains(item.getDate(), date))
                .filter(item->item.getDaycode().equals("0"))
                .filter(item-> {
                    Date holdayDate = DateUtil.parse(item.getDate(), "yyyy-MM-dd");
                    return DateUtil.compare(nowDate, holdayDate) > 0;
                })
                .map(CysHoliday::getDate)
                .collect(Collectors.toList());//工作日数据
        monthData.put("leaveDay", beforeWorkDay.stream().filter(item->!logs.containsKey(item)).count());//请假天数:不包括今天，之前所有工作日内没有记录日志的都算没上班,就是请假


        return AjaxResult.success(monthData);
    }


    /**
     * 获取今日工作台滚动是否需要记录日志
     * @return
     */
    public AjaxResult getNoticeText() {
        String time = DateUtil.format(new Date(), "yyyy-MM-dd");//今日时间
        CysHoliday  holiday = this.holidayService.lambdaQuery().eq(CysHoliday::getDate, time).one();
        if(ObjectUtil.isNull(holiday)){
            return AjaxResult.success("请联系管理员同步日期数据.");
        }
        long count = this.lambdaQuery().likeRight(CysWorkerLog::getSignDate, time).count();
        if(holiday.getDaycode().equals("0")) {//0是工作日
            if(count > 0){//工作日且记录了日志
                return AjaxResult.success("今天是工作日,您已记录日志.");
            }else{
                return AjaxResult.success("今天是工作日,您还未记录日志,请记录工作日志.");
            }
        }else{
            if(count > 0){
                return AjaxResult.success("今日加班,已经记录工作日志.");
            }else{
                return AjaxResult.success("今日休息,不需要记录工作日志,祝您生活愉快.");
            }
        }
    }

    /**
     * 获取当前年度每个项目组的所用工时数据
     * @return
     */
    public AjaxResult getEachProjectData(String time) {

        Long userId = SecurityUtils.getUserId();
        String yearly = StrUtil.sub(time, 0, 4);

        Map<String, String> projectMapper = projectService
                .lambdaQuery()
                .list()
                .stream()
                .collect(Collectors.toMap(CysWorkerProject::getProjectCode, CysWorkerProject::getProjectName));

        //今年的所有日志,并且按照签到时间分组。
        Map<String, List<CysWorkerLog>> logs = this.lambdaQuery()
                .likeRight(CysWorkerLog::getSignDate, yearly)
                .eq(CysWorkerLog::getUserId, userId)
                .list()
                .stream()
                .map(e->{
                    e.setHandleSignDate(DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD, e.getSignDate()));
                    return e;
                }).collect(Collectors.groupingBy(CysWorkerLog::getHandleSignDate));

        Map<String, Integer> resultMap = new HashMap<>();
        //按照项目来分，把每一天的数据，都进行拆分，最小单位就是天
        for(String key : logs.keySet()){
            String currentProjectCode = logs.get(key).get(0).getCurrentProjectCode();//取项目编号，拿真实的数据进行统计
            String name = projectMapper.get(currentProjectCode);
            if(StrUtil.isEmpty(name)){
                continue;
            }
            if(resultMap.containsKey(name)) {
                resultMap.put(name, resultMap.get(name) + 1);
            }else{
                resultMap.put(name, 1);
            }
        }

        return AjaxResult.success(resultMap);

    }

    private JSONObject initJson(){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", "");
        jsonObject.put("signDate", "");
        jsonObject.put("workLocation", "");
        jsonObject.put("workType", "");
        jsonObject.put("tags", "");
        jsonObject.put("remove", true);
        return jsonObject;
    }

    /**
     * 查询工作日志列表
     * 
     * @param cysWorkerLog 工作日志
     * @return 工作日志
     */
    @Override
    public List<CysWorkerLog> selectCysWorkerLogList(CysWorkerLog cysWorkerLog)
    {
        Date signDate = cysWorkerLog.getSignDate();
        if(ObjectUtil.isNull(signDate)){
            throw new BaseException("请输入录入日期后查询");
        }

        cysWorkerLog.setSignDate(null);
        cysWorkerLog.setUserId(SecurityUtils.getUserId());
        LambdaQueryWrapper<CysWorkerLog> wrapper = Wrappers.lambdaQuery(cysWorkerLog);

        wrapper.likeRight(CysWorkerLog::getSignDate, DateUtils.parseDateToStr(DateUtils.YYYY_MM, signDate));

        wrapper.orderByDesc(CysWorkerLog::getSignDate);

        List<CysWorkerLog> logs = this.list(wrapper);
        logs.forEach(e->{
            e.setTagsArray(JSON.parseArray(e.getTags()));
        });
        return logs;
    }

    /**
     * 新增工作日志
     * 有子表的话请自行添加
     * @param cysWorkerLog 工作日志
     * @return 结果
     */
    @Override
    public int insertCysWorkerLog(CysWorkerLog cysWorkerLog)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        cysWorkerLog.setUserId(user.getUserId());
        cysWorkerLog.setUserName(user.getNickName());
        cysWorkerLog.setCreateDate(new Date());
        cysWorkerLog.setTags(cysWorkerLog.getTagsArray().toString());
        this.setProjectCode(cysWorkerLog);
        this.save(cysWorkerLog);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    private void setProjectCode(CysWorkerLog cysWorkerLog){
        String code = cysWorkerLog.getCurrentProjectCode();
        if(StrUtil.isNotEmpty(code)){
            Optional<CysWorkerProject> projectOptional = workerProjectService.lambdaQuery()
                    .eq(CysWorkerProject::getProjectCode, code)
                    .list()
                    .stream()
                    .findFirst();

            if(projectOptional.isPresent()){
                cysWorkerLog.setCurrentProjectName(projectOptional.get().getProjectName());
            }

        }
    }

    /**
     * 修改工作日志
     * 有子表的话请自行修改
     * @param cysWorkerLog 工作日志
     * @return 结果
     */
    @Override
    public int updateCysWorkerLog(CysWorkerLog cysWorkerLog)
    {
        cysWorkerLog.setTags(cysWorkerLog.getTagsArray().toString());
        this.setProjectCode(cysWorkerLog);
        this.updateById(cysWorkerLog);
        return 1;
    }

    /**
     * 批量删除工作日志
     * 有子表的话请自行修改
     * @param ids 需要删除的工作日志主键
     * @return 结果
     */
    @Override
    public int deleteCysWorkerLogByIds(Long[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除工作日志信息
     * 
     * @param id 工作日志主键
     * @return 结果
     */
    @Override
    public int deleteCysWorkerLogById(Long id)
    {
        this.removeById(id);
        return 1;
    }

    /**
     * 考勤内容导出
     * 共三份文件：
     * 1、日志内容考勤的excel，需要包含考勤内容
     * 2、上班考勤表，是那一天上班的，那一天加班的
     * 3、本月工作汇报表，word，也可以考虑做一下
     * 最后将以上三类做成压缩文件导出即可
     */
    public void exportWorkLog(String time) throws IOException {

        if(StringUtils.isEmpty(time)){
            throw new BaseException("不存在时间，无法下载");
        }
        this.workLogExcel.exportWorkLog(time);

    }

    public void exportSign(String time) throws IOException {

        if(StringUtils.isEmpty(time)){
            throw new BaseException("不存在时间，无法下载");
        }
        this.signExcel.exportSign(time);//日志记录excel

    }

    /**
     * 超过当前时间返回true
     * @return
     */
    private boolean passCurrentDate(String time){
        Date date = DateUtil.parse(time, DateUtils.YYYY_MM_DD);//参数时间
        Date currentDate = DateUtil.date();//当前时间
        int flag = DateUtil.compare(date, currentDate);
        if(flag == 1){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        String date1 = "2023-05-21";
        String date2 = "2023-05-22";

        System.out.println(DateUtil.compare(DateUtil.parse(date1, DateUtils.YYYY_MM_DD), DateUtil.parse(date2, DateUtils.YYYY_MM_DD)));
    }


}
