package com.cys.worker.log.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.utils.SecurityUtils;
import com.cys.worker.log.domain.CysWorkerLog;
import com.cys.worker.log.domain.CysWorkerProject;
import com.cys.worker.log.mapper.CysWorkerProjectMapper;
import com.cys.worker.log.service.ICysWorkerLogService;
import com.cys.worker.log.service.ICysWorkerProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 工时所需项目表Service业务层处理
 * 
 * @author cys
 * @date 2024-03-28
 */
@Service
public class CysWorkerProjectServiceImpl extends ServiceImpl<CysWorkerProjectMapper, CysWorkerProject> implements ICysWorkerProjectService
{
    @Autowired
    private CysWorkerProjectMapper cysWorkerProjectMapper;

    @Autowired
    private ICysWorkerLogService workerLogService;

    /**
     * 获取我要填报的工作日志项目，取最近一天的标注，当默认值
     * @return
     */
    public List<CysWorkerProject> myWorkProject() {

        Long userId = SecurityUtils.getUserId();
        List<CysWorkerProject> workerProjectList = this.list();


        Optional<CysWorkerLog> workerLogOptional = workerLogService.lambdaQuery()
                .eq(CysWorkerLog::getUserId, userId)
                .isNotNull(CysWorkerLog::getCurrentProjectCode)
                .orderByDesc(CysWorkerLog::getCreateDate)
                .last("limit 1")
                .oneOpt();

        if(workerLogOptional.isPresent()) {//存在
            CysWorkerLog log = workerLogOptional.get();
            workerProjectList = workerProjectList.stream().map(res->{
                if(res.getProjectCode().equals(log.getCurrentProjectCode())){
                    res.setIsView("是");
                }
                return res;
            }).collect(Collectors.toList());

        }

        return workerProjectList;
    }

    /**
     * 查询工时所需项目表
     * 
     * @param id 工时所需项目表主键
     * @return 工时所需项目表
     */
    @Override
    public CysWorkerProject selectCysWorkerProjectById(String id)
    {
        return this.getById(id);
    }

    /**
     * 查询工时所需项目表列表
     * 
     * @param cysWorkerProject 工时所需项目表
     * @return 工时所需项目表
     */
    @Override
    public List<CysWorkerProject> selectCysWorkerProjectList(CysWorkerProject cysWorkerProject)
    {
        LambdaQueryWrapper<CysWorkerProject> wrapper = Wrappers.lambdaQuery(cysWorkerProject);
        return this.list(wrapper);
    }

    /**
     * 新增工时所需项目表
     * 有子表的话请自行添加
     * @param cysWorkerProject 工时所需项目表
     * @return 结果
     */
    @Override
    public int insertCysWorkerProject(CysWorkerProject cysWorkerProject)
    {
        this.save(cysWorkerProject);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改工时所需项目表
     * 有子表的话请自行修改
     * @param cysWorkerProject 工时所需项目表
     * @return 结果
     */
    @Override
    public int updateCysWorkerProject(CysWorkerProject cysWorkerProject)
    {
        this.updateById(cysWorkerProject);
        return 1;
    }

    /**
     * 批量删除工时所需项目表
     * 有子表的话请自行修改
     * @param ids 需要删除的工时所需项目表主键
     * @return 结果
     */
    @Override
    public int deleteCysWorkerProjectByIds(String[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除工时所需项目表信息
     * 
     * @param id 工时所需项目表主键
     * @return 结果
     */
    @Override
    public int deleteCysWorkerProjectById(String id)
    {
        this.removeById(id);
        return 1;
    }
}
