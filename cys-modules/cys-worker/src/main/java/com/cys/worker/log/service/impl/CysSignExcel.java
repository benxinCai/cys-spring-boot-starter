package com.cys.worker.log.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.cys.base.holiday.domain.CysHoliday;
import com.cys.base.holiday.service.ICysHolidayService;
import com.cys.common.utils.DateUtils;
import com.cys.common.utils.SecurityUtils;
import com.cys.common.utils.ServletUtils;
import com.cys.worker.log.domain.CysWorkerLog;
import com.cys.worker.log.service.ICysWorkerLogService;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 考勤内容excel
 */
@Service
public class CysSignExcel {

    private short fontSize = 10;

    @Autowired
    private ICysHolidayService holidayService;

    @Autowired
    private ICysWorkerLogService logService;

    public void exportSign(String time) throws IOException {

        String paramTime = time.substring(0,7);//yyyy-MM

        //当月所有假期数据
        List<CysHoliday> cysHolidayList = holidayService.lambdaQuery()
                .likeRight(CysHoliday::getDate, paramTime)
                .orderByAsc(CysHoliday::getDate)
                .list();

        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter(true);

        //第一页，基础信息
        writer.renameSheet("考勤表");
        //合并单元格后的标题行，使用默认标题样式
        mergerTitle(writer, time, cysHolidayList.size());
        mergerCompany(writer, time, cysHolidayList.size());

        writer.setCurrentRow(2);

        //当月所有日志
        Map<String, List<CysWorkerLog>> logs = logService.lambdaQuery()
                .likeRight(CysWorkerLog::getSignDate, paramTime)
                .eq(CysWorkerLog::getUserId, SecurityUtils.getUserId())
                .list()
                .stream()
                .map(e-> e.setHandleSignDate(this.getDayOfTime(DateUtils.parseDateToStr("yyyy-MM-dd", e.getSignDate()))))
                .collect(Collectors.groupingBy(CysWorkerLog::getHandleSignDate));

        //是否上班数据
        Map<String, Integer> goWorkMap = cysHolidayList.stream()
                .collect(Collectors.toMap(day -> this.getDayOfTime(day.getDate()), day -> {
                    if(Arrays.asList("0", "3").contains(day.getDaycode())){//正常上班,0是工作日，3是调休上班
                        return 0;
                    }
                    return 1;//放假
                }));

        Map<Object, Object> resultMap = new LinkedHashMap<Object, Object>();
        resultMap.put("序号", 1);
        writer.setColumnWidth(0, 5);
        writer.setColumnWidth(2, 20);
        resultMap.put("姓名", SecurityUtils.getLoginUser().getUser().getNickName());
        resultMap.put("部门", "经营管理业务部");
        for(int i = 1; i <= cysHolidayList.size(); i++){
            writer.setColumnWidth(i + 2, 3);
            if(ObjectUtil.isNull(logs.get(i + ""))){//没有写日志
                if(goWorkMap.get(String.valueOf(i)) == 0){//上班,但没有日志，当做事假处理
                    resultMap.put(i, "○");//事假
                }else{//不上班,那就公休
                    resultMap.put(i, "□");//公休
                }
            }else{//写了日志
                if(goWorkMap.get(String.valueOf(i)) == 0){//上班,有日志
                    resultMap.put(i, "/");//出勤
                }else{//不上班有日志，那就是加班
                    resultMap.put(i, "+");//加班
                }

            }
        }
        resultMap.put("总计天数", logs.keySet().size());
        writer.write(CollUtil.newArrayList(resultMap), true);

        this.rowTwoAndThreeStyle(writer);

        // 获取表格有多少列，设置头的样式及是否假期的颜色
        int rowSize = writer.getColumnCount();
        //设置头
        for(int i = 0; i< rowSize;i++){
            String key = String.valueOf(i-2);
            boolean isFill = false;
            if(goWorkMap.containsKey(key)){//存在这个key
                if(goWorkMap.get(key) == 1){//上班
                    isFill = true;
                }
            }

            setCellStyle(writer, i, 2, isFill);

        }

        writer.setCurrentRow(writer.getRowCount());
        Map<Object, Object> tips = new LinkedHashMap<Object, Object>();
        tips.put("序号", 2);
        tips.put("姓名", "");
        writer.write(CollUtil.newArrayList(tips));

        this.mergerIndex(writer, cysHolidayList.size());
        this.mergerSignPeople(writer);

        Date date = DateUtils.parseDate(time);

        String year =  DateUtils.parseDateToStr("yyyy", date);
        String month = DateUtils.parseDateToStr("M", date);
        Integer maxDays = DateUtil.lengthOfMonth(Integer.parseInt(month), DateUtil.isLeapYear(Integer.parseInt(year)));
        StringBuffer sb = new StringBuffer();

        sb.append(SecurityUtils.getLoginUser().getUser().getNickName()).append("考勤（")
                .append(year).append(".").append(month + "月").append("1日-").append(month + "月").append(maxDays).append("日）")
                .append(".xlsx");

        HttpServletResponse response = ServletUtils.getResponse();
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        response.setHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(sb.toString(),"UTF-8"));
        ServletOutputStream out = response.getOutputStream();
        writer.flush(out, true);
        writer.close();
        IoUtil.close(out);

    }

    /**
     * 设置签名人
     */
    private void mergerSignPeople(ExcelWriter writer){
        //合并 选择范围至少要两个cell要不会报错
        //合并开始行 结束行
        int startRow = 10, endRow = 10;

        //合并后设置样式
        CellStyle row0Style = writer.createRowStyle(10);
        // 设置水平居左
        row0Style.setAlignment(HorizontalAlignment.LEFT);
        // 设置垂直居中
        row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = writer.createFont();
        font.setFontHeightInPoints(fontSize);
        font.setFontName("宋体");
        row0Style.setFont(font);
        writer.merge(startRow, endRow, 6, 12 , "考勤员:", row0Style);
        writer.merge(startRow, endRow, 16, 21 , "审核员:", row0Style);
    }


    private String getDayOfTime(String time){
        StringBuffer sb = new StringBuffer();
        sb.append(Integer.parseInt(time.substring(8, 10)));
        return sb.toString();
    }

    private void mergerCompany(ExcelWriter writer, String time, int maxDay){
        //合并 选择范围至少要两个cell要不会报错
        //合并的开始列 ，最后一列
        int startCol = 0,lastCol = maxDay + 3;
        //合并开始行 结束行
        int startRow = 1,endRow = 1;

        //合并后设置样式
        CellStyle row0Style = writer.createRowStyle(0);
        // 设置水平居中
        row0Style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直居中
        row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = writer.createFont();
        font.setFontHeightInPoints(fontSize);
        font.setFontName("宋体");
        row0Style.setFont(font);
        writer.merge(startRow, endRow, startCol, lastCol , "单位：大庆金桥信息技术工程有限公司", row0Style);
    }

    /**
     * 合并标题
     * @param writer
     * @param time
     */
    private void mergerTitle(ExcelWriter writer, String time, int maxDay){
        //合并 选择范围至少要两个cell要不会报错
        //合并的开始列 ，最后一列
        int startCol = 0,lastCol = maxDay + 3;
        //合并开始行 结束行
        int startRow = 0,endRow = 0;

        //大庆金桥信息技术工程有限公司_7 月_1_日至_7 月_31日考勤表
        StringBuffer sb = new StringBuffer();
        Date date = DateUtils.parseDate(time);
        String month = DateUtils.parseDateToStr("M", date);
        sb.append("大庆金桥信息技术工程有限公司_").append(month).append(" 月_1_日至_").append(month).append(" 月_").append(maxDay).append("日考勤表");

        //合并后设置样式
        CellStyle row0Style = writer.createRowStyle(0);

        // 解决填充背景色没有边框问题
        row0Style.setBorderBottom(BorderStyle.THIN);
        row0Style.setBorderRight(BorderStyle.THIN);

        // 设置水平居中
        row0Style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直居中
        row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = writer.createFont();
        font.setBold(true);
        font.setFontHeightInPoints(Short.parseShort("14"));
        font.setFontName("宋体");
        row0Style.setFont(font);
        writer.merge(startRow, endRow, startCol, lastCol , sb.toString(), row0Style);
    }

    private void rowTwoAndThreeStyle(ExcelWriter writer){

        this.setBaseRowTwoAndThreeStyle(writer, 3);

    }

    private void setBaseRowTwoAndThreeStyle(ExcelWriter writer, int row){
        int maxColumn = writer.getColumnCount();
        for(int i = 0; i < maxColumn; i++){
            //合并后设置样式
            CellStyle row0Style = writer.createCellStyle(i, row);

            row0Style.setBorderBottom(BorderStyle.THIN);
            row0Style.setBorderRight(BorderStyle.THIN);
            // 设置水平居中
            row0Style.setAlignment(HorizontalAlignment.CENTER);
            // 设置垂直居中
            row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
            Font font = writer.createFont();
            //font.setBold(true);
            font.setFontHeightInPoints(fontSize);
            font.setFontName("宋体");
            row0Style.setFont(font);
        }

    }


    /**
     * 合并考勤确认人
     * @param writer
     */
    private void mergerIndex(ExcelWriter writer, int maxDay){
        //合并 选择范围至少要两个cell要不会报错
        //合并的开始列 ，最后一列
        int startCol = 2, lastCol = maxDay + 3;
        //合并开始行 结束行
        int startRow = 4, endRow = 4;

        writer.merge(startRow, endRow, startCol, lastCol , "出勤∕公休□  迟到＃  旷工×  病假△  事假○  代休▽  婚假 W  丧假 D  产假 B  工伤 H   离职 ※  年假 P   补助V 加班+ 公出＞", null);

        for(int i = 0; i<= maxDay+3; i++){
            //合并后设置样式
            CellStyle row0Style = writer.createCellStyle(i,4);

            row0Style.setBorderBottom(BorderStyle.THIN);
            row0Style.setBorderRight(BorderStyle.THIN);
            // 设置水平居中
            row0Style.setAlignment(HorizontalAlignment.CENTER);
            // 设置垂直居中
            row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
            Font font = writer.createFont();
            //font.setBold(true);
            font.setFontHeightInPoints(fontSize);
            font.setFontName("宋体");
            row0Style.setFont(font);
        }

    }

    /**
     * 自定义设置单元格样式
     *
     * @param writer hutool-Excel写入器
     * @param x      x_坐标
     * @param y      y_坐标
     */
    private void setCellStyle(ExcelWriter writer, int x, int y, boolean isFill) {
        CellStyle cellStyle = writer.createCellStyle(x, y);
        // 填充背景色
        if(isFill){
            cellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        }else{
            cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        }
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        // 解决填充背景色没有边框问题
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        Font font = writer.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints(fontSize);
        // 设置水平居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setFont(font);
        // 设置垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setWrapText(true);
    }


}
