package com.cys.worker.log.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.cys.base.holiday.domain.CysHoliday;
import com.cys.base.holiday.service.ICysHolidayService;
import com.cys.common.utils.DateUtils;
import com.cys.common.utils.SecurityUtils;
import com.cys.common.utils.ServletUtils;
import com.cys.worker.log.domain.CysWorkerLog;
import com.cys.worker.log.service.ICysWorkerLogService;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 工作日志导出
 */
@Service
public class CysWorkLogExcel {

    @Autowired
    private ICysHolidayService holidayService;

    @Autowired
    private ICysWorkerLogService logService;

    private short fontSize = 11;


    /**
     * 日志内容，具体那天上班，加班，未上班，考勤人等
     */
    public void exportWorkLog(String time) throws IOException {

        // 通过工具类创建writer
        ExcelWriter writer = ExcelUtil.getWriter(true);

        //第一页，基础信息
        writer.renameSheet("日志表");
        //跳过当前行，既第一行，非必须，在此演示用
        //writer.passCurrentRow();
        //合并单元格后的标题行，使用默认标题样式
        mergerTitle(writer, time);
        mergerCompany(writer);
        mergerLead(writer);
        int width = 11;
        writer.setColumnWidth(0, width);//日期
        writer.setColumnWidth(1, 45);//日志
        writer.setColumnWidth(2, width);//工作日志数
        writer.setColumnWidth(3, width);//日工时汇总
        writer.setColumnWidth(4, width);//项目人天
        writer.setColumnWidth(5, width);//工作地点
        writer.setColumnWidth(6, width);//工作方式


        String paramTime = time.substring(0,7);//yyyy-MM

        //当月所有假期数据
        List<CysHoliday> cysHolidayList = holidayService.lambdaQuery()
                .likeRight(CysHoliday::getDate, paramTime)
                .orderByAsc(CysHoliday::getDate)
                .list();

        //当月所有日志
        Map<String, List<CysWorkerLog>> logs = logService.lambdaQuery()
                .likeRight(CysWorkerLog::getSignDate, paramTime)
                .eq(CysWorkerLog::getUserId, SecurityUtils.getUserId())
                .list()
                .stream()
                .map(e-> e.setHandleSignDate(this.getDayOfTime(DateUtils.parseDateToStr("yyyy-MM-dd", e.getSignDate()))))
                .collect(Collectors.groupingBy(CysWorkerLog::getHandleSignDate));

        //处理数据,按照当月日期一条一条处理。
        List<Map<String, Object>> excelList = cysHolidayList.stream()
                .map(day -> {
                    Map<String, Object> resultMap = this.initRow();
                    String timeParam = this.getDayOfTime(day.getDate());
                    List<CysWorkerLog> cysWorkerLogList = logs.get(timeParam);
                    resultMap.put("日期", timeParam);
                    if(ObjectUtil.isNotNull(cysWorkerLogList)){
                        StringBuffer sb = new StringBuffer();
                        for(int i = 1; i <= cysWorkerLogList.size(); i++){
                            sb.append(i).append("、").append(cysWorkerLogList.get(i-1).getContent());
                            if(i != cysWorkerLogList.size()){
                                sb.append("\n");
                            }
                        }
                        resultMap.put("", sb.toString());
                        CysWorkerLog log = cysWorkerLogList.get(0);
                        resultMap.put("工作小时数", Integer.parseInt(log.getWorkHour()));
                        resultMap.put("日工时汇总", Integer.parseInt(log.getDayworkHours()));
                        resultMap.put("项目人天", Integer.parseInt(log.getProjectPersonDay()));
                        resultMap.put("工作地点", log.getWorkLocation());
                        resultMap.put("工作方式", log.getWorkType());
                    }
                    return resultMap;
                }).collect(Collectors.toList());

        writer.setCurrentRow(2);
        writer.write(excelList, true);

        Map<String, Integer> goWorkMap = cysHolidayList.stream()
                .collect(Collectors.toMap(day -> this.getDayOfTime(day.getDate()), day -> {
                    if(Arrays.asList("0", "3").contains(day.getDaycode())){//正常上班,0是工作日，1是调休上班
                        return 0;
                    }
                    return 1;//放假
                }));

        // 获取表格有多少列
        int rowSize = writer.getColumnCount();
        //设置头
        for(int i=0; i< rowSize;i++){
            setCellStyle(writer, i, 2, false, true);
        }
        // 遍历数据行，设置单元格样式
        for (int y = 3; y <= excelList.size() + 2; y++) {
            Map<String, Object> map = excelList.get(y - 3);
            // 设置行背景颜色
            for (int x = 0; x < rowSize; x++) {
                boolean isFill;
                boolean isCenter;

                if (goWorkMap.get(map.get("日期")) == 1) {//需要上班，直接
                    isFill = true;
                }else{
                    isFill = false;
                }
                if(x == 1){
                    isCenter = false;
                }else{
                    isCenter = true;
                }
                setCellStyle(writer, x, y, isFill, isCenter);
            }
        }

        //合计
        writer.setCurrentRow(writer.getRowCount());
        Map<String, Object> totalMap = this.initRow();
        totalMap.put("日期", "合计");//日期列显示合计
        totalMap.put("", logs.size() + "天");

        writer.write(CollUtil.newArrayList(totalMap));
        int y = writer.getRowCount() - 1;
        for (int x = 0; x < rowSize; x++) {
            setCellStyleTotal(writer, x, y);
        }

        //审核的结尾
        writer.setCurrentRow(writer.getRowCount());
        Map<String, Object> appMap = this.initRow();
        appMap.put("日期", "");//日期列不显示
        appMap.put("", "部门审核：");
        appMap.put("工作地点", "公司审核：");
        writer.write(CollUtil.newArrayList(appMap));

        y = writer.getRowCount() - 1;
        for (int x = 0; x < rowSize; x++) {
            setCellStyleApp(writer, x, y);
        }
        HttpServletResponse response = ServletUtils.getResponse();
        String fileName = "工作报告-" + DateUtils.parseDateToStr("MM", DateUtils.parseDate(time)) + "月-" + SecurityUtils.getLoginUser().getUser().getNickName() + ".xlsx";
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName,"UTF-8"));
        ServletOutputStream out = response.getOutputStream();
        writer.flush(out, true);
        writer.close();
        IoUtil.close(out);
    }


    private Map<String, Object> initRow(){
        Map<String, Object> resultMap = new LinkedHashMap<String, Object>();
        resultMap.put("日期", "");
        resultMap.put("", "");
        resultMap.put("工作小时数", "");
        resultMap.put("日工时汇总", "");
        resultMap.put("项目人天", "");
        resultMap.put("工作地点", "");
        resultMap.put("工作方式", "");
        return resultMap;
    }

    private void setCellStyleApp(ExcelWriter writer, int x, int y){


        CellStyle cellStyle = writer.createCellStyle(x, y);

        // 解决填充背景色没有边框问题
        if(x != 0){//0不需要边框
            cellStyle.setBorderLeft(BorderStyle.THIN);
            cellStyle.setBorderBottom(BorderStyle.THIN);
        }
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        Font font = writer.createFont();
        font.setFontName("微软雅黑");
        font.setFontHeightInPoints(fontSize);
        cellStyle.setFont(font);
        // 设置水平居左
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        // 设置垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
    }

    private void setCellStyleTotal(ExcelWriter writer, int x, int y){


        CellStyle cellStyle = writer.createCellStyle(x, y);
        // 填充背景色
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // 解决填充背景色没有边框问题
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        Font font = writer.createFont();
        font.setFontName("微软雅黑");
        font.setFontHeightInPoints(fontSize);
        cellStyle.setFont(font);
        // 设置水平居中
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直居中
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setWrapText(true);
    }

    /**
     * 自定义设置单元格样式
     *
     * @param writer hutool-Excel写入器
     * @param x      x_坐标
     * @param y      y_坐标
     */
    private void setCellStyle(ExcelWriter writer, int x, int y, boolean isFill, boolean isCenter) {
        CellStyle cellStyle = writer.createCellStyle(x, y);
        // 填充背景色
        if(isFill){
            cellStyle.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        }

        // 解决填充背景色没有边框问题
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
        Font font = writer.createFont();
        font.setFontName("微软雅黑");
        font.setFontHeightInPoints(fontSize);
        cellStyle.setFont(font);
        if(isCenter){
            // 设置水平居中
            cellStyle.setAlignment(HorizontalAlignment.CENTER);
            // 设置垂直居中
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        }else {
            // 设置水平居中
            cellStyle.setAlignment(HorizontalAlignment.LEFT);
            // 设置垂直居中
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        }
        cellStyle.setWrapText(true);
    }

    private String getDayOfTime(String time){
        StringBuffer sb = new StringBuffer();
        sb.append(Integer.parseInt(time.substring(8, 10))).append("日");
        return sb.toString();
    }


    /**
     * 合并考勤确认人
     * @param writer
     */
    private void mergerLead(ExcelWriter writer){
        //合并 选择范围至少要两个cell要不会报错
        //合并的开始列 ，最后一列
        int startCol = 5, lastCol = 6;
        //合并开始行 结束行
        int startRow = 1, endRow = 1;

        StringBuffer sb = new StringBuffer();
        sb.append("考勤确认人：").append("周晶");

        String content = sb.toString();

        //合并后设置样式
        CellStyle row0Style = writer.createRowStyle(0);

        // 设置水平居中
        row0Style.setAlignment(HorizontalAlignment.LEFT);
        // 设置垂直居中
        row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = writer.createFont();
        //font.setBold(true);
        font.setFontHeightInPoints(fontSize);
        font.setFontName("微软雅黑");
        row0Style.setFont(font);
        writer.merge(startRow, endRow, startCol, lastCol , content, row0Style);

    }

    /**
     * 合并所属公司
     * @param writer
     */
    private void mergerCompany(ExcelWriter writer){
        //合并 选择范围至少要两个cell要不会报错
        //合并的开始列 ，最后一列
        int startCol = 2, lastCol = 4;
        //合并开始行 结束行
        int startRow = 1, endRow = 1;

        String content = "所属公司：贝世康公司";

        //合并后设置样式
        CellStyle row0Style = writer.createRowStyle(0);

        // 设置水平居中
        row0Style.setAlignment(HorizontalAlignment.LEFT);
        // 设置垂直居中
        row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = writer.createFont();
        //font.setBold(true);
        font.setFontHeightInPoints(fontSize);
        font.setFontName("微软雅黑");
        row0Style.setFont(font);
        writer.merge(startRow, endRow, startCol, lastCol , content, row0Style);

    }

    /**
     * 合并标题
     * @param writer
     * @param time
     */
    private void mergerTitle(ExcelWriter writer, String time){
        //合并 选择范围至少要两个cell要不会报错
        //合并的开始列 ，最后一列
        int startCol = 0,lastCol = 6;
        //合并开始行 结束行
        int startRow = 0,endRow = 0;

        StringBuffer sb = new StringBuffer();
        Date date = DateUtils.parseDate(time);
        String year = DateUtils.parseDateToStr("yyyy", date);
        String month = DateUtils.parseDateToStr("MM", date);
        sb.append(year).append("年").append(month).append("月份外包人员工作考勤");

        String content = sb.toString();

        //合并后设置样式
        CellStyle row0Style = writer.createRowStyle(0);
        // 设置水平居中
        row0Style.setAlignment(HorizontalAlignment.CENTER);
        // 设置垂直居中
        row0Style.setVerticalAlignment(VerticalAlignment.CENTER);
        Font font = writer.createFont();
        //font.setBold(true);
        font.setFontHeightInPoints(Short.parseShort("18"));
        font.setFontName("微软雅黑");
        row0Style.setFont(font);
        writer.merge(startRow, endRow, startCol, lastCol , content, row0Style);
    }


}
