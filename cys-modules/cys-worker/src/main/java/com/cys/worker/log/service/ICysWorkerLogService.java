package com.cys.worker.log.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.common.core.domain.AjaxResult;
import com.cys.worker.log.domain.CysWorkerLog;

import java.io.IOException;
import java.util.List;

/**
 * 工作日志Service接口
 * 
 * @author cys
 * @date 2023-08-22
 */
public interface ICysWorkerLogService extends IService<CysWorkerLog>
{
    /**
     * 查询工作日志
     * 
     * @param id 工作日志主键
     * @return 工作日志
     */
    public CysWorkerLog selectCysWorkerLogById(Long id);

    /**
     * 小程序查询工作日志的方式
     *
     * @param time 时间
     * @return 工作日志集合
     */
    public AjaxResult selectMiNiWorker(String time);

    /**
     * 统计年度及月度相关数据
     * @param time
     * @return
     */
    public AjaxResult getYearStatisticalData(String time);

    /**
     * 统计月度工作数据
     * @param time
     * @return
     */
    public AjaxResult getMonthStatisticalData(String time);

    public AjaxResult getNoticeText();

    /**
     * 各项目组所花费的工时
     * @param time
     * @return
     */
    public AjaxResult getEachProjectData(String time);

    /**
     * 查询工作日志列表
     * 
     * @param cysWorkerLog 工作日志
     * @return 工作日志集合
     */
    public List<CysWorkerLog> selectCysWorkerLogList(CysWorkerLog cysWorkerLog);

    /**
     * 新增工作日志
     * 
     * @param cysWorkerLog 工作日志
     * @return 结果
     */
    public int insertCysWorkerLog(CysWorkerLog cysWorkerLog);

    /**
     * 修改工作日志
     * 
     * @param cysWorkerLog 工作日志
     * @return 结果
     */
    public int updateCysWorkerLog(CysWorkerLog cysWorkerLog);

    /**
     * 批量删除工作日志
     * 
     * @param ids 需要删除的工作日志主键集合
     * @return 结果
     */
    public int deleteCysWorkerLogByIds(Long[] ids);

    /**
     * 删除工作日志信息
     * 
     * @param id 工作日志主键
     * @return 结果
     */
    public int deleteCysWorkerLogById(Long id);

    public void exportWorkLog(String time) throws IOException;

    public void exportSign(String time) throws IOException;
}
