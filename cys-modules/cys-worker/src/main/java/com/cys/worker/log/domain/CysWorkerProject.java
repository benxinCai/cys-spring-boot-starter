package com.cys.worker.log.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 工时所需项目表对象 cys_worker_project
 * 
 * @author cys
 * @date 2024-03-28
 */
@Data
@ApiModel(value="cys_worker_project对象", description="")
@TableName("cys_worker_project")
public class CysWorkerProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableField("id")
    private String id;

    /** 项目名称 */
    @Excel(name = "项目名称")
    @ApiModelProperty(value = "项目名称")
    @TableField("project_name")
    private String projectName;

    /** 项目编号 */
    @Excel(name = "项目编号")
    @ApiModelProperty(value = "项目编号")
    @TableField("project_code")
    private String projectCode;

    /** 是否展示 */
    @Excel(name = "是否展示")
    @ApiModelProperty(value = "是否展示")
    @TableField("is_view")
    private String isView;

}
