package com.cys.league.member.domain;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 我的团员信息对象 cys_league_member
 * 
 * @author cys
 * @date 2023-09-27
 */
@Data
@ApiModel(value="cys_league_member对象", description="")
@TableName("cys_league_member")
public class CysLeagueMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableField("id")
    private String id;

    /** 毕业年份 */
    @Excel(name = "毕业年份")
    @ApiModelProperty(value = "毕业年份")
    @TableField(value = "grad_year", condition = SqlCondition.LIKE)
    private String gradYear;

    /** 毕业时班级 */
    @Excel(name = "毕业时班级")
    @ApiModelProperty(value = "毕业时班级")
    @TableField(value = "grad_class", condition = SqlCondition.LIKE)
    private String gradClass;

    /** 毕业时班主任 */
    @Excel(name = "毕业时班主任")
    @ApiModelProperty(value = "毕业时班主任")
    @TableField(value = "grad_teacher", condition = SqlCondition.LIKE)
    private String gradTeacher;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    @ApiModelProperty(value = "学生姓名")
    @TableField(value = "student_name", condition = SqlCondition.LIKE)
    private String studentName;

    /** 身份证号 */
    @Excel(name = "身份证号")
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "id_card", condition = SqlCondition.LIKE)
    private String idCard;

    /** 入团时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入团时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "入团时间")
    @TableField("in_time")
    private Date inTime;

    /** 团员编号 */
    @ApiModelProperty(value = "团员编号")
    @TableField("numbers")
    private String numbers;

    /** 删除标识 */
    @ApiModelProperty(value = "入团时间")
    @TableField("del_flag")
    private String delFlag;

    /** 老师ID */
    @ApiModelProperty(value = "入团时间")
    @TableField("user_id")
    private Long userId;

    /** 老师昵称 */
    @ApiModelProperty(value = "入团时间")
    @TableField("nick_name")
    private String nickName;

    /** 老师昵称 */
    @ApiModelProperty(value = "备注")
    @TableField("remark")
    private String remark;

}
