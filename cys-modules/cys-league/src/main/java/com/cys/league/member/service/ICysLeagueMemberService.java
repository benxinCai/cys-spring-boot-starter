package com.cys.league.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.league.member.domain.CysLeagueMember;

import java.util.List;

/**
 * 我的团员信息Service接口
 * 
 * @author cys
 * @date 2023-09-27
 */
public interface ICysLeagueMemberService  extends IService<CysLeagueMember>
{
    /**
     * 查询我的团员信息
     * 
     * @param id 我的团员信息主键
     * @return 我的团员信息
     */
    public CysLeagueMember selectCysLeagueMemberById(String id);

    /**
     * 查询我的团员信息列表
     * 
     * @param cysLeagueMember 我的团员信息
     * @return 我的团员信息集合
     */
    public List<CysLeagueMember> selectCysLeagueMemberList(CysLeagueMember cysLeagueMember);

    /**
     * 新增我的团员信息
     * 
     * @param cysLeagueMember 我的团员信息
     * @return 结果
     */
    public int insertCysLeagueMember(CysLeagueMember cysLeagueMember);

    /**
     * 修改我的团员信息
     * 
     * @param cysLeagueMember 我的团员信息
     * @return 结果
     */
    public int updateCysLeagueMember(CysLeagueMember cysLeagueMember);

    /**
     * 批量删除我的团员信息
     * 
     * @param ids 需要删除的我的团员信息主键集合
     * @return 结果
     */
    public int deleteCysLeagueMemberByIds(String[] ids);

    /**
     * 删除我的团员信息信息
     * 
     * @param id 我的团员信息主键
     * @return 结果
     */
    public int deleteCysLeagueMemberById(String id);
}
