package com.cys.league.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.league.member.domain.CysLeagueMember;

/**
 * 我的团员信息Mapper接口
 * 
 * @author cys
 * @date 2023-09-27
 */
public interface CysLeagueMemberMapper extends BaseMapper<CysLeagueMember> {

}
