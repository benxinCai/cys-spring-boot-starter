package com.cys.league.member.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.utils.DateUtils;
import com.cys.common.utils.SecurityUtils;
import com.cys.league.member.domain.CysLeagueMember;
import com.cys.league.member.mapper.CysLeagueMemberMapper;
import com.cys.league.member.service.ICysLeagueMemberService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 我的团员信息Service业务层处理
 * 
 * @author cys
 * @date 2023-09-27
 */
@Service
public class CysLeagueMemberServiceImpl extends ServiceImpl<CysLeagueMemberMapper, CysLeagueMember> implements ICysLeagueMemberService
{

    /**
     * 查询我的团员信息
     * 
     * @param id 我的团员信息主键
     * @return 我的团员信息
     */
    @Override
    public CysLeagueMember selectCysLeagueMemberById(String id)
    {
        return this.getById(id);
    }

    /**
     * 查询我的团员信息列表
     * 
     * @param cysLeagueMember 我的团员信息
     * @return 我的团员信息
     */
    @Override
    public List<CysLeagueMember> selectCysLeagueMemberList(CysLeagueMember cysLeagueMember)
    {

        cysLeagueMember.setDelFlag("0");
        cysLeagueMember.setUserId(SecurityUtils.getUserId());
        LambdaQueryWrapper<CysLeagueMember> wrapper = Wrappers.lambdaQuery(cysLeagueMember);

        return this.list(wrapper);
    }

    /**
     * 新增我的团员信息
     * 有子表的话请自行添加
     * @param cysLeagueMember 我的团员信息
     * @return 结果
     */
    @Override
    public int insertCysLeagueMember(CysLeagueMember cysLeagueMember)
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        cysLeagueMember.setUserId(user.getUserId());
        cysLeagueMember.setNickName(user.getNickName());
        cysLeagueMember.setDelFlag("0");
        cysLeagueMember.setCreateTime(DateUtils.getNowDate());
        this.save(cysLeagueMember);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改我的团员信息
     * 有子表的话请自行修改
     * @param cysLeagueMember 我的团员信息
     * @return 结果
     */
    @Override
    public int updateCysLeagueMember(CysLeagueMember cysLeagueMember)
    {
        this.updateById(cysLeagueMember);
        return 1;
    }

    /**
     * 批量删除我的团员信息
     * 有子表的话请自行修改
     * @param ids 需要删除的我的团员信息主键
     * @return 结果
     */
    @Override
    public int deleteCysLeagueMemberByIds(String[] ids)
    {
        this.lambdaUpdate()
                .set(CysLeagueMember::getDelFlag, "1")
                .in(CysLeagueMember::getId, ids)
                .update();
        return 1;
    }

    /**
     * 删除我的团员信息信息
     * 
     * @param id 我的团员信息主键
     * @return 结果
     */
    @Override
    public int deleteCysLeagueMemberById(String id)
    {
        this.lambdaUpdate()
                .set(CysLeagueMember::getDelFlag, "1")
                .eq(CysLeagueMember::getId, id)
                .update();
        return 1;
    }
}
