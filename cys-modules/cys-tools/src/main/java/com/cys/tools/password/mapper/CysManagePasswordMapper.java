package com.cys.tools.password.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.tools.password.domain.CysManagePassword;

/**
 * 密码管理Mapper接口
 * 
 * @author cys
 * @date 2024-05-15
 */
public interface CysManagePasswordMapper extends BaseMapper<CysManagePassword> {

}
