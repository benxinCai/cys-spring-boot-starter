package com.cys.tools.password.service.impl;

import java.util.Arrays;
import java.util.List;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.utils.DateUtils;
import com.cys.common.utils.SecurityUtils;
import com.cys.common.utils.StringUtils;
import com.cys.tools.password.domain.CysManagePassword;
import com.cys.tools.password.mapper.CysManagePasswordMapper;
import com.cys.tools.password.service.ICysManagePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 密码管理Service业务层处理
 * 
 * @author cys
 * @date 2024-05-15
 */
@Service
public class CysManagePasswordServiceImpl extends ServiceImpl<CysManagePasswordMapper, CysManagePassword> implements ICysManagePasswordService
{

    /**
     * 查询密码管理
     * 
     * @param id 密码管理主键
     * @return 密码管理
     */
    @Override
    public CysManagePassword selectCysManagePasswordById(String id)
    {
        CysManagePassword cysManagePassword = this.getById(id);
        cysManagePassword.setAuthPassword(null);
        return cysManagePassword;
    }

    /**
     * 查询密码管理列表
     * 
     * @param cysManagePassword 密码管理
     * @return 密码管理
     */
    @Override
    public List<CysManagePassword> selectCysManagePasswordList(CysManagePassword cysManagePassword)
    {
        cysManagePassword.setUserId(SecurityUtils.getUserId());
        LambdaQueryWrapper<CysManagePassword> wrapper = Wrappers.lambdaQuery(cysManagePassword);
        List<CysManagePassword> cysManagePasswordList = this.list(wrapper);
        cysManagePasswordList.forEach(item ->{
            item.setPassword(null);
            item.setAuthPassword(null);
        });
        return cysManagePasswordList;
    }

    /**
     * 变更授权密码，旧的密码和新的密码一起
     * @param oldPassword
     * @param newPassword
     * @return
     */
    public AjaxResult modifyAuthPassword(String oldPassword, String newPassword) {

        if(StringUtils.isEmpty(newPassword)){
            return AjaxResult.error("请输入新密码");
        }

        Long userId = SecurityUtils.getUserId();
        List<CysManagePassword> managePasswordList = this.lambdaQuery()
                .eq(CysManagePassword::getUserId, userId)
                .eq(CysManagePassword::getAuthPassword, oldPassword)
                .list();

        if(CollUtil.isEmpty(managePasswordList)){
            return AjaxResult.error("您输入的授权密码不对,请重新输入.");
        }

        this.lambdaUpdate()
                .eq(CysManagePassword::getUserId, userId)
                .set(CysManagePassword::getAuthPassword, newPassword)
                .update();
        return AjaxResult.success();
    }

    /**
     * 通过授权密码查看密码
     * @return
     */
    public AjaxResult checkAuth(String id, String authPassword) {
        CysManagePassword managePassword = this.getById(id);

        if(managePassword.getAuthPassword().equals(authPassword)){
            return AjaxResult.success(true);
        }

        return AjaxResult.success(false);
    }

    /**
     * 新增密码管理
     * 有子表的话请自行添加
     * @param cysManagePassword 密码管理
     * @return 结果
     */
    @Override
    public int insertCysManagePassword(CysManagePassword cysManagePassword)
    {
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        cysManagePassword.setUserId(sysUser.getUserId());
        cysManagePassword.setUserName(sysUser.getUserName());
        cysManagePassword.setCreateDate(DateUtils.getNowDate());
        cysManagePassword.setAuthPassword("123456");
        this.save(cysManagePassword);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改密码管理
     * 有子表的话请自行修改
     * @param cysManagePassword 密码管理
     * @return 结果
     */
    @Override
    public int updateCysManagePassword(CysManagePassword cysManagePassword)
    {
        this.updateById(cysManagePassword);
        return 1;
    }

    /**
     * 批量删除密码管理
     * 有子表的话请自行修改
     * @param ids 需要删除的密码管理主键
     * @return 结果
     */
    @Override
    public int deleteCysManagePasswordByIds(String[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除密码管理信息
     * 
     * @param id 密码管理主键
     * @return 结果
     */
    @Override
    public int deleteCysManagePasswordById(String id)
    {
        this.removeById(id);
        return 1;
    }
}
