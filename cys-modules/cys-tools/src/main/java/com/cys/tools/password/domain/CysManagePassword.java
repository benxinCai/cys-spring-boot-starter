package com.cys.tools.password.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * 密码管理对象 cys_manage_password
 * 
 * @author cys
 * @date 2024-05-15
 */
@Data
@ApiModel(value="cys_manage_password对象", description="")
@TableName("cys_manage_password")
public class CysManagePassword extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 账号标题 */
    @Excel(name = "账号标题")
    @ApiModelProperty(value = "账号标题")
    @TableField(value = "title", condition = SqlCondition.LIKE)
    private String title;

    /** 账号 */
    @Excel(name = "账号")
    @ApiModelProperty(value = "账号")
    @TableField(value = "account", condition = SqlCondition.LIKE)
    private String account;

    /** 密码 */
    @ApiModelProperty(value = "账号")
    @TableField("password")
    private String password;

    /** 用户ID */
    @ApiModelProperty(value = "账号")
    @TableField("user_id")
    private Long userId;

    /** 用户名称 */
    @ApiModelProperty(value = "账号")
    @TableField("user_name")
    private String userName;

    /** 授权密码 */
    @ApiModelProperty(value = "账号")
    @TableField("auth_password")
    private String authPassword;

    /** 创建时间 */
    @TableField("create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    /** 备注 */
    @TableField("remarks")
    private String remarks;

}
