package com.cys.tools.password.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.common.core.domain.AjaxResult;
import com.cys.tools.password.domain.CysManagePassword;

/**
 * 密码管理Service接口
 * 
 * @author cys
 * @date 2024-05-15
 */
public interface ICysManagePasswordService  extends IService<CysManagePassword>
{
    /**
     * 查询密码管理
     * 
     * @param id 密码管理主键
     * @return 密码管理
     */
    public CysManagePassword selectCysManagePasswordById(String id);

    /**
     * 查询密码管理列表
     * 
     * @param cysManagePassword 密码管理
     * @return 密码管理集合
     */
    public List<CysManagePassword> selectCysManagePasswordList(CysManagePassword cysManagePassword);

    public AjaxResult modifyAuthPassword(String oldPassword, String newPassword);//修改授权密码

    public AjaxResult checkAuth(String id, String authPassword);

    /**
     * 新增密码管理
     * 
     * @param cysManagePassword 密码管理
     * @return 结果
     */
    public int insertCysManagePassword(CysManagePassword cysManagePassword);

    /**
     * 修改密码管理
     * 
     * @param cysManagePassword 密码管理
     * @return 结果
     */
    public int updateCysManagePassword(CysManagePassword cysManagePassword);

    /**
     * 批量删除密码管理
     * 
     * @param ids 需要删除的密码管理主键集合
     * @return 结果
     */
    public int deleteCysManagePasswordByIds(String[] ids);

    /**
     * 删除密码管理信息
     * 
     * @param id 密码管理主键
     * @return 结果
     */
    public int deleteCysManagePasswordById(String id);
}
