package com.cys.kaduo.manageuser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.common.core.domain.entity.SysRole;
import com.cys.kaduo.manageuser.domain.CysManageAuthUser;

import java.util.List;

/**
 * 小程序授权管理员Service接口
 * 
 * @author cys
 * @date 2023-09-15
 */
public interface ICysManageAuthUserService  extends IService<CysManageAuthUser>
{

    List<List<SysRole>> listRoleByUserName(String userName);

    /**
     * 查询小程序授权管理员
     * 
     * @param id 小程序授权管理员主键
     * @return 小程序授权管理员
     */
    public CysManageAuthUser selectCysManageAuthUserById(String id);

    /**
     * 查询小程序授权管理员列表
     * 
     * @param cysManageAuthUser 小程序授权管理员
     * @return 小程序授权管理员集合
     */
    public List<CysManageAuthUser> selectCysManageAuthUserList(CysManageAuthUser cysManageAuthUser);

    /**
     * 新增小程序授权管理员
     * 
     * @param cysManageAuthUser 小程序授权管理员
     * @return 结果
     */
    public int insertCysManageAuthUser(CysManageAuthUser cysManageAuthUser);

    /**
     * 修改小程序授权管理员
     * 
     * @param cysManageAuthUser 小程序授权管理员
     * @return 结果
     */
    public int updateCysManageAuthUser(CysManageAuthUser cysManageAuthUser);

    /**
     * 批量删除小程序授权管理员
     * 
     * @param ids 需要删除的小程序授权管理员主键集合
     * @return 结果
     */
    public int deleteCysManageAuthUserByIds(String[] ids);

    /**
     * 删除小程序授权管理员信息
     * 
     * @param id 小程序授权管理员主键
     * @return 结果
     */
    public int deleteCysManageAuthUserById(String id);
}
