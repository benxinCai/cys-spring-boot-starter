package com.cys.kaduo.rolemenu.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 小程序菜单关联表对象 cys_role_minimenu
 * 
 * @author cys
 * @date 2023-09-04
 */
@Data
@ApiModel(value="cys_role_minimenu对象", description="")
@TableName("cys_role_minimenu")
public class CysRoleMinimenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId
    private String id;

    /** 小程序菜单ID */
    @Excel(name = "小程序菜单ID")
    @ApiModelProperty(value = "小程序菜单ID")
    @TableField("mini_menu_id")
    private String miniMenuId;

    /** 菜单名称 */
    @Excel(name = "菜单名称")
    @ApiModelProperty(value = "菜单名称")
    @TableField("text")
    private String text;

    /** 角色ID */
    @Excel(name = "角色ID")
    @ApiModelProperty(value = "角色ID")
    @TableField("role_id")
    private Long roleId;

    /** 角色菜单 */
    @Excel(name = "角色菜单")
    @ApiModelProperty(value = "角色菜单")
    @TableField("role_name")
    private String roleName;

}
