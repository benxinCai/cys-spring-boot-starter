package com.cys.mynika.service;

import cn.hutool.core.date.DateUtil;

import java.math.BigDecimal;
import java.util.Date;

public class LevelHandle {


    /**
     * 寻到大千等级计算器
     *
     * 当前升级后所增长的修为固定，例如当前升一级所需修为是1000，每升一级所增长的是50，那么下一级所需要的修为就是1050，再下一级所需修为就是1100
     * 以上相当于一个等差数列！
     *
     * 尾项的计算方式：尾项 = 首项 + (项数 - 1) * 升级所需修为
     *
     * 合计所需修为公式： （首项+尾项）*项数/2
     *
     * 聚灵阵每天固定给修为：1000
     * 瓶子固定每天给3000
     * 每周固定给桃子：32000个
     *
     */
    public static void main(String[] args) {
        //1、初始化计算数据
        BigDecimal currentUpdateXiuWei = new BigDecimal(27200);//当前提升以及所需要的修为
        BigDecimal currentLevel = new BigDecimal(957);//当前等级
        BigDecimal lastLevel = new BigDecimal(1200);//提升境界时所需要的等级 金仙935，玄仙后875,金仙后1200
        BigDecimal updateXiuweiCount = new BigDecimal(70);//每提升一级所需要的修为90， 金仙
        BigDecimal weekTao = new BigDecimal(42000);//每周大概获取桃子数量
        BigDecimal shengyuTaozi = new BigDecimal(0);
        BigDecimal xiangshu = lastLevel.subtract(currentLevel);
        BigDecimal weixiang = currentUpdateXiuWei.add(xiangshu.subtract(BigDecimal.ONE).multiply(updateXiuweiCount));//尾项的计算
        BigDecimal total = currentUpdateXiuWei.add(weixiang).multiply(xiangshu).divide(new BigDecimal(2), 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal dayTao = weekTao.divide(new BigDecimal(7), 2, BigDecimal.ROUND_HALF_UP);
        BigDecimal dayXiuWei = dayTao.multiply(new BigDecimal(10)).add(new BigDecimal(1000)).add(new BigDecimal(3000));
        StringBuffer sb = new StringBuffer();
        sb.append("从").append(currentLevel).append("升级到").append(lastLevel).append("共需要：");
        System.out.println(sb);
        int days = total.subtract(shengyuTaozi.multiply(BigDecimal.TEN)).divide(dayXiuWei, 2, BigDecimal.ROUND_HALF_UP).intValue();
        System.out.println(days + "天");
        Date date = new Date();

        String start = DateUtil.format(date, "yyyy-MM-dd");

        String end = DateUtil.format(DateUtil.offsetDay(date, days), "yyyy-MM-dd");

        System.out.println("从" + start + "到" + end);

        System.out.println("大概需要：" + total.subtract(shengyuTaozi.multiply(BigDecimal.TEN)).divide(BigDecimal.TEN).intValue() + "桃子");

    }

}
