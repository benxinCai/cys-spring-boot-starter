package com.cys.kaduo.manageuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.manageuser.domain.CysManageAuthUser;

/**
 * 小程序授权管理员Mapper接口
 * 
 * @author cys
 * @date 2023-09-15
 */
public interface CysManageAuthUserMapper extends BaseMapper<CysManageAuthUser> {

}
