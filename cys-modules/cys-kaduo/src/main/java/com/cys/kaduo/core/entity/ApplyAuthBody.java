package com.cys.mynika.entity;

import lombok.Data;

@Data
public class ApplyAuthBody {
    private Long roleId;
    private String content;
}
