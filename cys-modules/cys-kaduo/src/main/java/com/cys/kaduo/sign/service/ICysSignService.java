package com.cys.kaduo.sign.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.common.core.domain.AjaxResult;
import com.cys.kaduo.sign.domain.CysSign;

import java.util.List;

/**
 * 签到管理Service接口
 * 
 * @author cys
 * @date 2023-10-19
 */
public interface ICysSignService  extends IService<CysSign>
{
    /**
     * 查询签到管理
     * 
     * @param id 签到管理主键
     * @return 签到管理
     */
    public CysSign selectCysSignById(String id);

    /**
     * 查询签到管理列表
     * 
     * @param cysSign 签到管理
     * @return 签到管理集合
     */
    public List<CysSign> selectCysSignList(CysSign cysSign);

    /**
     * 新增签到管理
     * 
     * @param cysSign 签到管理
     * @return 结果
     */
    public int insertCysSign(CysSign cysSign);

    /**
     * 修改签到管理
     * 
     * @param cysSign 签到管理
     * @return 结果
     */
    public int updateCysSign(CysSign cysSign);

    /**
     * 批量删除签到管理
     * 
     * @param ids 需要删除的签到管理主键集合
     * @return 结果
     */
    public int deleteCysSignByIds(String[] ids);

    /**
     * 删除签到管理信息
     * 
     * @param id 签到管理主键
     * @return 结果
     */
    public int deleteCysSignById(String id);

    public AjaxResult sign();

    public Long signCount();//累计签到

    public int continuousSignCount();//连续签到
}
