package com.cys.kaduo.notice.adapter;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.log.StaticLog;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.core.util.SpringBeanUtil;
import com.cys.common.exception.base.BaseException;
import com.cys.kaduo.notice.domain.CysNotice;
import com.cys.kaduo.notice.service.ICysNoticeService;
import com.cys.other.util.BeanUtil;
import com.cys.system.service.ISysNoticeService;

import java.util.Date;

/**
 * 消息通知适配器
 */
public class NoticeAdapter {

    private CysNotice cysNotice;

    private static ICysNoticeService noticeService;//用户通知

    private static ISysNoticeService sysNoticeService;//系统通知,暂时设计成模版

    public static ISysNoticeService getTempNotice(){
        if(ObjectUtil.isNull(sysNoticeService)){
            sysNoticeService = SpringBeanUtil.getBean(ISysNoticeService.class);
        }
        return sysNoticeService;
    }

    private static ICysNoticeService getService(){
        if(ObjectUtil.isNull(noticeService)){
            noticeService = SpringBeanUtil.getBean(ICysNoticeService.class);
        }
        return noticeService;
    }

    private NoticeAdapter(){//每次初始化创建一个新的
        cysNotice = new CysNotice();
    }

    public static NoticeAdapter builder(){
        return new NoticeAdapter();
    }

    public NoticeAdapter setTitle(String title){
        cysNotice.setNoticeTitle(title);
        return this;
    }

    public NoticeAdapter setContent(String content){
        cysNotice.setNoticeContent(content);
        return this;
    }

    public NoticeAdapter setType(String type){
        cysNotice.setNoticeType(type);
        return this;
    }

    public NoticeAdapter setSendUserId(Long userId){
        cysNotice.setUserId(userId);
        return this;
    }

    public void sendNotice(){

        if(ObjectUtil.isNull(cysNotice.getUserId())){
            throw new BaseException("不存在待接收用户，请检查输入参数");
        }
        SysUser user = BeanUtil.getUserService().selectUserById(cysNotice.getUserId());
        cysNotice.setNickName(user.getNickName());
        cysNotice.setCreateDate(new Date());
        cysNotice.setCreateUser("镜花水月");
        StaticLog.info("发送系统消息,当前给:{},发送类型为:{}", user.getNickName(), cysNotice.getNoticeType());
        getService().save(cysNotice);

    }



}
