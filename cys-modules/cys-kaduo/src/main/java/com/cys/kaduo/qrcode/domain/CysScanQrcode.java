package com.cys.kaduo.qrcode.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 扫码登录对象 cys_scan_qrcode
 * 
 * @author cys
 * @date 2023-10-18
 */
@Data
@ApiModel(value="cys_scan_qrcode对象", description="")
@TableName("cys_scan_qrcode")
public class CysScanQrcode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 码表主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** uuid码 */
    @Excel(name = "uuid码")
    @ApiModelProperty(value = "uuid码")
    @TableField("uuid")
    private String uuid;

    /** 图片二进制 */
    @Excel(name = "图片二进制")
    @ApiModelProperty(value = "图片二进制")
    @TableField("images")
    private byte[] images;

    /** 使用次数 */
    @Excel(name = "使用次数")
    @ApiModelProperty(value = "使用次数")
    @TableField("use_count")
    private Integer useCount;

}
