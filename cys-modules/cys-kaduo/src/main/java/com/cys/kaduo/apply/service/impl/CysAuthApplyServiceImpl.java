package com.cys.kaduo.apply.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.log.StaticLog;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.core.text.StrFormatter;
import com.cys.kaduo.apply.domain.CysAuthApply;
import com.cys.kaduo.apply.mapper.CysAuthApplyMapper;
import com.cys.kaduo.apply.service.ICysAuthApplyService;
import com.cys.kaduo.notice.adapter.NoticeAdapter;
import com.cys.system.domain.SysNotice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 用户申请权限表Service业务层处理
 * 
 * @author cys
 * @date 2023-09-15
 */
@Service("authApplyService")
public class CysAuthApplyServiceImpl extends ServiceImpl<CysAuthApplyMapper, CysAuthApply> implements ICysAuthApplyService
{

    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    @Autowired
    private TransactionDefinition transactionDefinition;

    /**
     * 查询用户申请权限表
     * 
     * @param id 用户申请权限表主键
     * @return 用户申请权限表
     */
    @Override
    public CysAuthApply selectCysAuthApplyById(String id)
    {
        return this.getById(id);
    }

    /**
     * 查询用户申请权限表列表
     * 
     * @param cysAuthApply 用户申请权限表
     * @return 用户申请权限表
     */
    @Override
    public List<CysAuthApply> selectCysAuthApplyList(CysAuthApply cysAuthApply)
    {
        LambdaQueryWrapper<CysAuthApply> wrapper = Wrappers.lambdaQuery(cysAuthApply);
        return this.list(wrapper);
    }

    /**
     * 新增用户申请权限表
     * 有子表的话请自行添加
     * @param cysAuthApply 用户申请权限表
     * @return 结果
     */
    @Override
    public int insertCysAuthApply(CysAuthApply cysAuthApply)
    {
        this.save(cysAuthApply);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改用户申请权限表
     * 有子表的话请自行修改
     * @param cysAuthApply 用户申请权限表
     * @return 结果
     */
    @Override
    public int updateCysAuthApply(CysAuthApply cysAuthApply)
    {
        this.updateById(cysAuthApply);
        return 1;
    }

    /**
     * 批量删除用户申请权限表
     * 有子表的话请自行修改
     * @param ids 需要删除的用户申请权限表主键
     * @return 结果
     */
    @Override
    public int deleteCysAuthApplyByIds(String[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除用户申请权限表信息
     * 
     * @param id 用户申请权限表主键
     * @return 结果
     */
    @Override
    public int deleteCysAuthApplyById(String id)
    {
        this.removeById(id);
        return 1;
    }

    /**
     * 删除没有权限管理员申请的数据
     * 1、直接删除
     * 2、给申请人发消息
     */
    public void deleteRecentApplyData(){

        Date date = DateUtil.offsetDay(new Date(), -7);

        List<CysAuthApply> applyList = this.lambdaQuery()
                .isNull(CysAuthApply::getHandleResult)
                .le(CysAuthApply::getApplyTime, date)
                .list();
        for(CysAuthApply apply : applyList){//一个一个删除后刷新
            //手动开启事务
            TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
            try{
                this.removeById(apply.getId());//删除

                SysNotice sysNotice = NoticeAdapter.getTempNotice().selectNoticeById(1l);//固定为1的数据
                //发送通知
//                String contentTemp = "<p>您好：</p><p>\t您申请的权限[{}]因为在7天内无管理员处理，已经被系统自动拒绝，如果您还需要使用此功能，" +
//                        "请您再次申请，建议您在申请的时候可以写详细一点申请原因或者直接联系管理员给您审批。";
                String contentTemp = sysNotice.getNoticeContent();
                NoticeAdapter.builder()
                        .setTitle("系统自动拒绝权限【"+ apply.getRoleName() +"】申请通知")
                        .setContent(StrFormatter.format(contentTemp, apply.getRoleName()))
                        .setType("拒绝权限申请")
                        .setSendUserId(apply.getUserId())
                        .sendNotice();
                dataSourceTransactionManager.commit(transactionStatus);//手动提交事务
            }catch(Exception e){
                //手动回滚事务
                StaticLog.error("此删除申请单及发送消息操作出现异常，异常原因为:{}", e.getMessage());
                dataSourceTransactionManager.rollback(transactionStatus);//手动回滚事务
            }

        }
    }
}
