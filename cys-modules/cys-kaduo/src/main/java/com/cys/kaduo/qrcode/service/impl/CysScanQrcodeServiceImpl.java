package com.cys.kaduo.qrcode.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.kaduo.qrcode.domain.CysScanQrcode;
import com.cys.kaduo.qrcode.mapper.CysScanQrcodeMapper;
import com.cys.kaduo.qrcode.service.ICysScanQrcodeService;
import com.cys.other.facade.MyWechatService;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 扫码登录Service业务层处理
 * 
 * @author cys
 * @date 2023-10-18
 */
@Service
@DependsOn({"wxMaConfiguration"})
public class CysScanQrcodeServiceImpl extends ServiceImpl<CysScanQrcodeMapper, CysScanQrcode> implements ICysScanQrcodeService
{

    @Autowired
    private MyWechatService myWechatService;

    //@PostConstruct
    public void createQrCode() throws WxErrorException {
        CysScanQrcode qrcode = null;
        for(int a=0; a<3000; a++){
            qrcode = new CysScanQrcode();
            String uuid = IdUtil.fastSimpleUUID();
            byte[] images = myWechatService.getUnLimitQrCode(uuid,"pages/index");
            qrcode.setUuid(uuid);
            qrcode.setUseCount(0);
            qrcode.setImages(images);
            this.save(qrcode);
        }
    }

    /**
     * 查询扫码登录
     * 
     * @param id 扫码登录主键
     * @return 扫码登录
     */
    @Override
    public CysScanQrcode selectCysScanQrcodeById(String id)
    {
        return this.getById(id);
    }

    /**
     * 查询扫码登录列表
     * 
     * @param cysScanQrcode 扫码登录
     * @return 扫码登录
     */
    @Override
    public List<CysScanQrcode> selectCysScanQrcodeList(CysScanQrcode cysScanQrcode)
    {
        LambdaQueryWrapper<CysScanQrcode> wrapper = Wrappers.lambdaQuery(cysScanQrcode);
        return this.list(wrapper);
    }

    /**
     * 新增扫码登录
     * 有子表的话请自行添加
     * @param cysScanQrcode 扫码登录
     * @return 结果
     */
    @Override
    public int insertCysScanQrcode(CysScanQrcode cysScanQrcode)
    {
        this.save(cysScanQrcode);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改扫码登录
     * 有子表的话请自行修改
     * @param cysScanQrcode 扫码登录
     * @return 结果
     */
    @Override
    public int updateCysScanQrcode(CysScanQrcode cysScanQrcode)
    {
        this.updateById(cysScanQrcode);
        return 1;
    }

    /**
     * 批量删除扫码登录
     * 有子表的话请自行修改
     * @param ids 需要删除的扫码登录主键
     * @return 结果
     */
    @Override
    public int deleteCysScanQrcodeByIds(String[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除扫码登录信息
     * 
     * @param id 扫码登录主键
     * @return 结果
     */
    @Override
    public int deleteCysScanQrcodeById(String id)
    {
        this.removeById(id);
        return 1;
    }
}
