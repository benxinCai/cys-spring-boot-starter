package com.cys.kaduo.minimenu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.minimenu.domain.CysMiniMenu;

/**
 * 小程序菜单Mapper接口
 * 
 * @author cys
 * @date 2023-09-04
 */
public interface CysMiniMenuMapper extends BaseMapper<CysMiniMenu> {

}
