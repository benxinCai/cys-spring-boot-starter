package com.cys.kaduo.notice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.kaduo.notice.domain.CysNotice;

import java.util.List;

/**
 * 通知公告Service接口
 * 
 * @author cys
 * @date 2023-10-13
 */
public interface ICysNoticeService  extends IService<CysNotice>
{
    /**
     * 查询通知公告
     * 
     * @param noticeId 通知公告主键
     * @return 通知公告
     */
    public CysNotice selectCysNoticeByNoticeId(String noticeId);

    /**
     * 查询通知公告列表
     * 
     * @param cysNotice 通知公告
     * @return 通知公告集合
     */
    public List<CysNotice> selectCysNoticeList(CysNotice cysNotice);

    /**
     * 新增通知公告
     * 
     * @param cysNotice 通知公告
     * @return 结果
     */
    public int insertCysNotice(CysNotice cysNotice);

    /**
     * 修改通知公告
     * 
     * @param cysNotice 通知公告
     * @return 结果
     */
    public int updateCysNotice(CysNotice cysNotice);

    /**
     * 批量删除通知公告
     * 
     * @param noticeIds 需要删除的通知公告主键集合
     * @return 结果
     */
    public int deleteCysNoticeByNoticeIds(String[] noticeIds);

    /**
     * 删除通知公告信息
     * 
     * @param noticeId 通知公告主键
     * @return 结果
     */
    public int deleteCysNoticeByNoticeId(String noticeId);
}
