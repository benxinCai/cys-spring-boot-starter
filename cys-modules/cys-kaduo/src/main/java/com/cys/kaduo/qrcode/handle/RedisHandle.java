package com.cys.kaduo.qrcode.handle;

import cn.hutool.log.StaticLog;
import com.cys.common.core.redis.RedisCache;
import com.cys.common.core.util.SpringBeanUtil;
import com.cys.kaduo.qrcode.domain.CysScanQrcode;
import com.cys.kaduo.qrcode.service.ICysScanQrcodeService;
import com.cys.other.util.BeanUtil;

import java.util.Collection;
import java.util.List;

public class RedisHandle {

    private static RedisCache redis;

    private RedisHandle() {
        redis = BeanUtil.getRedis();
    }

    /**
     * 获取redis方法
     *
     * @return
     */
    public static RedisCache getRedis() {
        return BeanUtil.getRedis();
    }

    /**
     * 处理部分逻辑,不一定可以复用
     *
     * @return
     */
    public static RedisHandle builder() {
        return new RedisHandle();
    }

    /**
     * 处理扫描二维码多出来的数据
     */
    public void handleScanQrcode() {

        long startTime = System.currentTimeMillis();
        RedisCache redisCache = BeanUtil.getRedis();
        Collection<String> keys = redisCache.keys("scan:*");
        for (String item : keys) {
            redisCache.deleteObject(item);
        }
        String key = "scan:qrcode:";
        List<CysScanQrcode> qrcodeList = SpringBeanUtil.getBean(ICysScanQrcodeService.class).lambdaQuery().select(CysScanQrcode::getUuid).list();
        for (CysScanQrcode qrcode : qrcodeList) {
            redisCache.setCacheObject(key + qrcode.getUuid(), 1);
        }
        long endTime = System.currentTimeMillis();
        StaticLog.info("把2000条数据塞到redis中,共计花费：" + (endTime - startTime) / 1000 + "秒");
    }

}
