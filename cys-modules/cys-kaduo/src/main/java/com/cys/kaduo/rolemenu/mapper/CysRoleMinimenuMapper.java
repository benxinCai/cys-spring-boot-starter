package com.cys.kaduo.rolemenu.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;

/**
 * 小程序菜单关联表Mapper接口
 * 
 * @author cys
 * @date 2023-09-04
 */
public interface CysRoleMinimenuMapper extends BaseMapper<CysRoleMinimenu> {

}
