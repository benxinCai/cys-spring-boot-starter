package com.cys.kaduo.notice.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 通知公告对象 cys_notice
 * 
 * @author cys
 * @date 2023-10-13
 */
@Data
@ApiModel(value="cys_notice对象", description="")
@TableName("cys_notice")
public class CysNotice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公告ID */
    @ApiModelProperty(value = "${comment}")
    @TableId("notice_id")
    private String noticeId;

    /** 用户ID */
    @ApiModelProperty(value = "${comment}")
    @TableField("user_id")
    private Long userId;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    @ApiModelProperty(value = "用户昵称")
    @TableField("nick_name")
    private String nickName;

    /** 公告标题 */
    @Excel(name = "公告标题")
    @ApiModelProperty(value = "公告标题")
    @TableField("notice_title")
    private String noticeTitle;

    /** 公告类型 */
    @Excel(name = "公告类型")
    @ApiModelProperty(value = "公告类型")
    @TableField("notice_type")
    private String noticeType;

    /** 0是未读，1是已读 */
    @Excel(name = "0是未读，1是已读")
    @ApiModelProperty(value = "0是未读，1是已读")
    @TableField("read1")
    private String read1;

    /** 已读时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "已读时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "已读时间")
    @TableField("read_time")
    private Date readTime;

    /** 已读时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "已读时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "已读时间")
    @TableField("create_date")
    private Date createDate;

    /** 公告内容 */
    @Excel(name = "公告内容")
    @ApiModelProperty(value = "公告内容")
    @TableField("notice_content")
    private String noticeContent;

    /** 公告状态（0正常 1关闭） */
    @Excel(name = "公告状态", readConverterExp = "0=正常,1=关闭")
    @ApiModelProperty(value = "公告状态")
    @TableField("status1")
    private String status1;

    @ApiModelProperty(value = "创建者")
    @TableField("create_user")
    private String createUser;

}
