package com.cys.kaduo.apply.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.apply.domain.CysAuthApply;

/**
 * 用户申请权限表Mapper接口
 * 
 * @author cys
 * @date 2023-09-15
 */
public interface CysAuthApplyMapper extends BaseMapper<CysAuthApply> {

}
