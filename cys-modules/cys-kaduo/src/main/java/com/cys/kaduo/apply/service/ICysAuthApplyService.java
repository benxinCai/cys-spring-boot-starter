package com.cys.kaduo.apply.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.kaduo.apply.domain.CysAuthApply;

import java.util.List;

/**
 * 用户申请权限表Service接口
 * 
 * @author cys
 * @date 2023-09-15
 */
public interface ICysAuthApplyService  extends IService<CysAuthApply>
{
    /**
     * 查询用户申请权限表
     * 
     * @param id 用户申请权限表主键
     * @return 用户申请权限表
     */
    public CysAuthApply selectCysAuthApplyById(String id);

    /**
     * 查询用户申请权限表列表
     * 
     * @param cysAuthApply 用户申请权限表
     * @return 用户申请权限表集合
     */
    public List<CysAuthApply> selectCysAuthApplyList(CysAuthApply cysAuthApply);

    /**
     * 新增用户申请权限表
     * 
     * @param cysAuthApply 用户申请权限表
     * @return 结果
     */
    public int insertCysAuthApply(CysAuthApply cysAuthApply);

    /**
     * 修改用户申请权限表
     * 
     * @param cysAuthApply 用户申请权限表
     * @return 结果
     */
    public int updateCysAuthApply(CysAuthApply cysAuthApply);

    /**
     * 批量删除用户申请权限表
     * 
     * @param ids 需要删除的用户申请权限表主键集合
     * @return 结果
     */
    public int deleteCysAuthApplyByIds(String[] ids);

    /**
     * 删除用户申请权限表信息
     * 
     * @param id 用户申请权限表主键
     * @return 结果
     */
    public int deleteCysAuthApplyById(String id);

    public void deleteRecentApplyData();
}
