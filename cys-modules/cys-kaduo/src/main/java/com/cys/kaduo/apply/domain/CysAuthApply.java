package com.cys.kaduo.apply.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 用户申请权限表对象 cys_auth_apply
 * 
 * @author cys
 * @date 2023-09-15
 */
@Data
@ApiModel(value="cys_auth_apply对象", description="")
@TableName("cys_auth_apply")
public class CysAuthApply extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableField("id")
    private String id;

    /** 申请人ID */
    @ApiModelProperty(value = "${comment}")
    @TableField("user_id")
    private Long userId;

    /** 申请人名称 */
    @Excel(name = "申请人名称")
    @ApiModelProperty(value = "申请人名称")
    @TableField("user_name")
    private String userName;

    @Excel(name = "申请人昵称")
    @ApiModelProperty(value = "申请人昵称")
    @TableField("nick_name")
    private String nickName;

    /** 角色ID */
    @ApiModelProperty(value = "申请人名称")
    @TableField("role_id")
    private Long roleId;

    /** 角色名称 */
    @Excel(name = "角色名称")
    @ApiModelProperty(value = "角色名称")
    @TableField("role_name")
    private String roleName;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "申请时间")
    @TableField("apply_time")
    private Date applyTime;

    /** 处理人ID */
    @ApiModelProperty(value = "申请时间")
    @TableField("handle_id")
    private Long handleId;

    /** 处理人名称 */
    @Excel(name = "处理人名称")
    @ApiModelProperty(value = "处理人名称")
    @TableField("handle_name")
    private String handleName;

    /** 处理结果 */
    @Excel(name = "处理结果")
    @ApiModelProperty(value = "处理结果")
    @TableField("handle_result")
    private String handleResult;

    /** 申请内容 */
    @Excel(name = "申请内容")
    @ApiModelProperty(value = "申请内容")
    @TableField("apply_content")
    private String applyContent;

    /** 处理结果的解释 */
    @Excel(name = "处理结果的解释")
    @ApiModelProperty(value = "处理结果的解释")
    @TableField("handle_content")
    private String handleContent;

    /** 处理时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "处理时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "处理时间")
    @TableField("handle_time")
    private Date handleTime;

}
