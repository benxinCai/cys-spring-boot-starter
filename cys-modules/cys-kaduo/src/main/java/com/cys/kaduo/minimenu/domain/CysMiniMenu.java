package com.cys.kaduo.minimenu.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 小程序菜单对象 cys_mini_menu
 * 
 * @author cys
 * @date 2023-09-04
 */
@Data
@ApiModel(value="cys_mini_menu对象", description="")
@TableName("cys_mini_menu")
public class CysMiniMenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId
    private String id;

    /** 菜单名称 */
    @Excel(name = "菜单名称")
    @ApiModelProperty(value = "菜单名称")
    @TableField("text")
    private String text;

    /** 菜单跳转路径 */
    @Excel(name = "菜单跳转路径")
    @ApiModelProperty(value = "菜单跳转路径")
    @TableField("page")
    private String page;

    /** 菜单图标 */
    @Excel(name = "菜单图标")
    @ApiModelProperty(value = "菜单图标")
    @TableField("icon")
    private String icon;

    @Excel(name = "图标颜色")
    @ApiModelProperty(value = "图标颜色")
    @TableField("color")
    private String color;

    @TableField(exist = false)
    private String roles;

}
