package com.cys.kaduo.minimenu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.common.core.domain.AjaxResult;
import com.cys.kaduo.minimenu.domain.CysMiniMenu;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;

import java.util.List;

/**
 * 小程序菜单Service接口
 * 
 * @author cys
 * @date 2023-09-04
 */
public interface ICysMiniMenuService extends IService<CysMiniMenu>
{
    /**
     * 查询小程序菜单
     * 
     * @param id 小程序菜单主键
     * @return 小程序菜单
     */
    public CysMiniMenu selectCysMiniMenuById(Long id);

    /**
     * 查询小程序菜单列表
     * 
     * @param cysMiniMenu 小程序菜单
     * @return 小程序菜单集合
     */
    public List<CysMiniMenu> selectCysMiniMenuList(CysMiniMenu cysMiniMenu);

    public AjaxResult saveRoleMenu(List<CysRoleMinimenu> minimenus);

    /**
     * 新增小程序菜单
     * 
     * @param cysMiniMenu 小程序菜单
     * @return 结果
     */
    public int insertCysMiniMenu(CysMiniMenu cysMiniMenu);

    /**
     * 修改小程序菜单
     * 
     * @param cysMiniMenu 小程序菜单
     * @return 结果
     */
    public int updateCysMiniMenu(CysMiniMenu cysMiniMenu);

    /**
     * 批量删除小程序菜单
     * 
     * @param ids 需要删除的小程序菜单主键集合
     * @return 结果
     */
    public int deleteCysMiniMenuByIds(Long[] ids);

    /**
     * 删除小程序菜单信息
     * 
     * @param id 小程序菜单主键
     * @return 结果
     */
    public int deleteCysMiniMenuById(Long id);
}
