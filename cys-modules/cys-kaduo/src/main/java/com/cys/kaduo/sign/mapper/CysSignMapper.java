package com.cys.kaduo.sign.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.sign.domain.CysSign;

/**
 * 签到管理Mapper接口
 * 
 * @author cys
 * @date 2023-10-19
 */
public interface CysSignMapper extends BaseMapper<CysSign> {

}
