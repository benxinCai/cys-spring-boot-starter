package com.cys.kaduo.qrcode.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.kaduo.qrcode.domain.CysScanQrcode;


import java.util.List;

/**
 * 扫码登录Service接口
 * 
 * @author cys
 * @date 2023-10-18
 */
public interface ICysScanQrcodeService  extends IService<CysScanQrcode>
{
    /**
     * 查询扫码登录
     * 
     * @param id 扫码登录主键
     * @return 扫码登录
     */
    public CysScanQrcode selectCysScanQrcodeById(String id);

    /**
     * 查询扫码登录列表
     * 
     * @param cysScanQrcode 扫码登录
     * @return 扫码登录集合
     */
    public List<CysScanQrcode> selectCysScanQrcodeList(CysScanQrcode cysScanQrcode);

    /**
     * 新增扫码登录
     * 
     * @param cysScanQrcode 扫码登录
     * @return 结果
     */
    public int insertCysScanQrcode(CysScanQrcode cysScanQrcode);

    /**
     * 修改扫码登录
     * 
     * @param cysScanQrcode 扫码登录
     * @return 结果
     */
    public int updateCysScanQrcode(CysScanQrcode cysScanQrcode);

    /**
     * 批量删除扫码登录
     * 
     * @param ids 需要删除的扫码登录主键集合
     * @return 结果
     */
    public int deleteCysScanQrcodeByIds(String[] ids);

    /**
     * 删除扫码登录信息
     * 
     * @param id 扫码登录主键
     * @return 结果
     */
    public int deleteCysScanQrcodeById(String id);
}
