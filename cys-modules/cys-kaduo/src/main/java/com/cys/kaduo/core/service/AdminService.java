package com.cys.kaduo.core.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.StopWatch;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.log.StaticLog;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.domain.entity.SysRole;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.core.redis.RedisCache;
import com.cys.common.utils.SecurityUtils;
import com.cys.common.utils.StringUtils;
import com.cys.kaduo.apply.domain.CysAuthApply;
import com.cys.kaduo.apply.service.ICysAuthApplyService;
import com.cys.kaduo.manageuser.domain.CysManageAuthUser;
import com.cys.kaduo.manageuser.service.ICysManageAuthUserService;
import com.cys.kaduo.minimenu.domain.CysMiniMenu;
import com.cys.kaduo.minimenu.service.ICysMiniMenuService;
import com.cys.kaduo.notice.adapter.NoticeAdapter;
import com.cys.kaduo.notice.domain.CysNotice;
import com.cys.kaduo.notice.service.ICysNoticeService;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;
import com.cys.kaduo.rolemenu.service.ICysRoleMinimenuService;
import com.cys.other.util.BeanUtil;
import com.cys.system.domain.SysNotice;
import com.cys.system.service.ISysRoleService;
import com.cys.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 管理员业务方法
 */
@Service
public class AdminService {
    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ICysMiniMenuService cysMiniMenuService;

    @Autowired
    private ICysRoleMinimenuService cysRoleMinimenuService;

    @Autowired
    private ICysManageAuthUserService cysManageAuthUserService;

    @Autowired
    private ICysAuthApplyService cysAuthApplyService;

    @Autowired
    private ICysNoticeService noticeService;


    /**
     * 小程序菜单,自定义菜单
     * 1、按照角色拉取菜单内容
     * redis 策略
     * 缓存30天过期，缓存key值为："authMiniMenus:" + userId + roleId
     * 角色变了重新做缓存
     * @return
     */
    public AjaxResult listMiniMenu(){

        Long userId = SecurityUtils.getUserId();
        RedisCache redisCache = BeanUtil.getRedis();
        List<Long> roleIds = roleService.selectByUserId(userId)
                .stream()
                .map(SysRole::getRoleId)
                .collect(Collectors.toList());
        if(CollUtil.isEmpty(roleIds)){
            roleIds.add(-100l);
        }

        StringBuffer redisKey = new StringBuffer();
        redisKey.append("authMiniMenus:").append(userId).append(":").append(CollUtil.join(roleIds.stream().sorted().collect(Collectors.toList()), ","));

        List<CysMiniMenu> miniMenus = redisCache.getCacheObject(redisKey.toString());

        if(CollUtil.isEmpty(miniMenus)){//不存在，则从库里面取一次
            StopWatch sw = new StopWatch("小程序自定义菜单");
            sw.start("计时");
            List<String> miniMenuIds = cysRoleMinimenuService.lambdaQuery().in(CysRoleMinimenu::getRoleId, roleIds)
                    .list()
                    .stream()
                    .map(CysRoleMinimenu::getMiniMenuId)
                    .collect(Collectors.toList());

            if(CollUtil.isEmpty(miniMenuIds)){
                miniMenuIds.add("不存在角色的兼容");
            }
            miniMenus = cysMiniMenuService.lambdaQuery().in(CysMiniMenu::getId, miniMenuIds).list();
            redisCache.setCacheObject(redisKey.toString(), miniMenus, 30, TimeUnit.DAYS);//30天过期
            sw.stop();
            StaticLog.info("请求小程序端自定义菜单耗时：{}{}", sw.getLastTaskTimeMillis(), "ms");
        }

        return AjaxResult.success(miniMenus);
    }



    /**
     * 通用角色可以申请角色的列表
     * 查找全部角色，过滤掉不可申请的权限列表，查找当前用户拥有的角色，循环做标记
     * @return
     */
    public AjaxResult listApplyAuth(){
        Long userId = SecurityUtils.getUserId();

        SysRole role = new SysRole();
        role.setAppletShow("是");

        List<SysRole> sysRoleList = this.roleService.selectRoleListNoAuth(role);


        Set<String> roleKeyList = this.roleService.selectRolePermissionByUserId(userId);

        //申请单数据，申请的角色Id列表
        List<Long> applyRoleIds = cysAuthApplyService.lambdaQuery()
                .isNull(CysAuthApply::getHandleResult)
                .eq(CysAuthApply::getUserId, userId)
                .list()
                .stream()
                .map(CysAuthApply::getRoleId)
                .collect(Collectors.toList());

        sysRoleList = sysRoleList.stream()
                .map(item->{
                    String showType = "10";//没有的权限
                    if(roleKeyList.contains(item.getRoleKey())){
                        showType = "0";//0代表已经拥有
                    }
                    if(applyRoleIds.contains(item.getRoleId())){
                        showType = "1";//1代表申请中
                    }
                    item.setShowType(showType);
                    return item;
                }).collect(Collectors.toList());


        return AjaxResult.success(sysRoleList);
    }

    /**
     * 用户申请权限接口
     * @return
     */
    public AjaxResult applyAuth(Long roleId, String content){

        SysRole role = roleService.selectRoleById(roleId);//用户要申请的角色
        SysUser user = SecurityUtils.getLoginUser().getUser();

        if(ObjectUtil.isNull(role)){
            return AjaxResult.error("不存在该权限,请刷新后重试.");
        }

        //存在,则不授权了
        if(roleService.selectByUserId(user.getUserId()).stream().anyMatch(e->e.getRoleId() == role.getRoleId())){
            return AjaxResult.error("您已拥有该权限,请不要重复申请.");
        }

        //申请中
        List<CysAuthApply> applies = cysAuthApplyService.lambdaQuery()
                .eq(CysAuthApply::getRoleId, role.getRoleId())
                .eq(CysAuthApply::getUserId, user.getUserId())
                .isNull(CysAuthApply::getHandleResult)
                .list();
        if(CollUtil.isNotEmpty(applies)){//不为空，说明在申请中
            return AjaxResult.error("此权限已经在申请中,请耐心等待.");
        }

        //申请上限
        if(applies.size() >= 3){
            return AjaxResult.error("您已到达申请上限，请管理员审核后再次申请.");
        }

        CysAuthApply authApply = new CysAuthApply();//申请单
        authApply.setUserId(user.getUserId());
        authApply.setApplyTime(new Date());
        authApply.setApplyContent(content);
        authApply.setRoleId(role.getRoleId());
        authApply.setRoleName(role.getRoleName());
        authApply.setNickName(user.getNickName());
        authApply.setUserName(user.getUserName());
        cysAuthApplyService.save(authApply);

        //给所有可以申请处理权限的管理员发送消息，通知处理审核
        List<CysManageAuthUser> cysManageAuthUsers = cysManageAuthUserService.lambdaQuery()
                .eq(CysManageAuthUser::getRoleId, role.getRoleId())
                .list();
        StringBuffer title = new StringBuffer();
        SysNotice sysNotice = NoticeAdapter.getTempNotice().selectNoticeById(2l);//固定为2的数据
        title.append("【").append(role.getRoleName()).append("】").append("的权限申请");
        String contents = StrFormatter.format(sysNotice.getNoticeContent(),
                user.getUserName() + "[" + user.getNickName() + "]",
                        role.getRoleName()
        );
        for(CysManageAuthUser authUser : cysManageAuthUsers){
            NoticeAdapter.builder()
                    .setTitle(title.toString())
                    .setContent(contents)
                    .setType("权限申请")
                    .setSendUserId(authUser.getUserId())
                    .sendNotice();
        }

        return AjaxResult.success("申请成功,请等待审核.");
    }

    /**
     * 当前申请数量，超过三个时，将不再可以申请
     * 直接查表，看当前未处理的数量，返回未处理的申请单，前端通过length来判断有是否可以申请
     * @return
     */
    public AjaxResult currentApply(){
        return AjaxResult.success();
    }

    /**
     * 管理员赋予角色,确认授权
     * 1、回写审批单数据
     * 2、授权角色
     * @return
     */
    @Transactional
    public AjaxResult confirmAuth(String applyId){
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();//管理员
        CysAuthApply authApply = cysAuthApplyService.getById(applyId);//待授权的申请单

        if(ObjectUtil.isNull(authApply)){
            return AjaxResult.error("不存在该审批单");
        }

        List<CysManageAuthUser> manageAuthUsers = cysManageAuthUserService.lambdaQuery()
                .eq(CysManageAuthUser::getRoleId, authApply.getRoleId())
                .eq(CysManageAuthUser::getUserId, sysUser.getUserId())
                .list();

        if(CollUtil.isEmpty(manageAuthUsers)){
            return AjaxResult.error("您无权审批该申请单,请刷新后重试.");
        }

        if(StringUtils.isNotEmpty(authApply.getHandleResult())){
            return AjaxResult.error("该审批单已被处理,请刷新后再试");
        }

        authApply.setHandleContent("审批通过,祝您使用愉快.");
        authApply.setHandleId(sysUser.getUserId());
        authApply.setHandleName(sysUser.getNickName());
        authApply.setHandleResult("已通过");
        authApply.setHandleTime(new Date());
        cysAuthApplyService.updateById(authApply);//回写审批单数据

        roleService.authRole(authApply.getRoleId(), authApply.getUserId());

        return AjaxResult.success();
    }

    /**
     * 获取通知详情
     * @return
     */
    public AjaxResult getNoticeDetail(String id){

        noticeService.lambdaUpdate()
                .set(CysNotice::getRead1, "是")
                .set(CysNotice::getReadTime, new Date())
                .eq(CysNotice::getNoticeId, id)
                .update();
        CysNotice notice = noticeService.getById(id);
        return AjaxResult.success(notice);
    }

    /**
     * 获取通知列表
     * @param notice
     * @return
     */
    public List<CysNotice> listNotice(CysNotice notice){

        notice.setUserId(SecurityUtils.getUserId());

        LambdaQueryWrapper<CysNotice> wrapper = Wrappers.lambdaQuery(notice);

        wrapper.orderByAsc(CysNotice::getRead1);
        wrapper.orderByDesc(CysNotice::getCreateDate);

        return noticeService.list(wrapper);
    }

    public AjaxResult noticeCount(){
        int count = noticeService.lambdaQuery()
                .eq(CysNotice::getUserId, SecurityUtils.getUserId())
                .eq(CysNotice::getRead1, "否")
                .count();
        return AjaxResult.success(count);
    }

}
