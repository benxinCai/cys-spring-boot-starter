package com.cys.mynika.entity;

import lombok.Data;

@Data
public class MobageBody {

    private String userName;
    private String passWord;

}
