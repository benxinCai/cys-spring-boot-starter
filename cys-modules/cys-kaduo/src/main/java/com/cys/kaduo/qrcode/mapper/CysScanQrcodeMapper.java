package com.cys.kaduo.qrcode.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.qrcode.domain.CysScanQrcode;

/**
 * 扫码登录Mapper接口
 * 
 * @author cys
 * @date 2023-10-18
 */
public interface CysScanQrcodeMapper extends BaseMapper<CysScanQrcode> {

}
