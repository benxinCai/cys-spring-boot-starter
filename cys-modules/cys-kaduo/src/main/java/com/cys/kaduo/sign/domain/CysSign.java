package com.cys.kaduo.sign.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 签到管理对象 cys_sign
 * 
 * @author cys
 * @date 2023-10-19
 */
@Data
@ApiModel(value="cys_sign对象", description="")
@TableName("cys_sign")
public class CysSign extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 签到主键 */
    @ApiModelProperty(value = "${comment}")
    @TableId("id")
    private String id;

    /** 年 */
    @Excel(name = "年")
    @ApiModelProperty(value = "年")
    @TableField("year")
    private String year;

    /** 月 */
    @Excel(name = "月")
    @ApiModelProperty(value = "月")
    @TableField("month")
    private String month;

    /** 日 */
    @Excel(name = "日")
    @ApiModelProperty(value = "日")
    @TableField("day")
    private String day;

    /** 签到时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Excel(name = "签到时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "签到时间")
    @TableField("sign_date")
    private Date signDate;

    /** 签到用户ID */
    @Excel(name = "签到用户ID")
    @ApiModelProperty(value = "签到用户ID")
    @TableField("user_id")
    private Long userId;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    @ApiModelProperty(value = "用户昵称")
    @TableField("nick_name")
    private String nickName;

}
