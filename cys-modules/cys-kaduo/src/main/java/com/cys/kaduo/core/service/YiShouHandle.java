package com.cys.mynika.service;

import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.RandomUtil;
import net.bytebuddy.description.type.TypeList;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 尼卡异兽计算器
 * 前提：
 * 1、青龙，且不触发协同，不触发道法连击
 * 2、精怪固定，神通固定
 * 3、连爆反默认全满，反击协同概率上限80，连击次数最高4次，每次连击后重新判断是否为50%(也就是当前满连击属性的一半)
 */
public class YiShouHandle {

    private BigDecimal attribute_warning = new BigDecimal(100);

    /**
     * 异兽伤害处理过程
     * 1、青龙，且不触发协同，不触发道法连击
     * 2、精怪固定，神通固定
     * 3、连爆反默认全满，反击协同概率上限80，连击次数最高4次，每次连击后重新判断是否为50%(也就是当前满连击属性的一半)
     * 4、15轮打满
     *
     * 战斗过程：
     * 我方加载状态，我方灵兽出手，我方人物出手，对方人物出手
     *
     * @return
     */
    public Map<String, BigDecimal> handle(YiShouBody body) {

        //定义数据
        BigDecimal bs = body.getJcbs();//爆伤
        BigDecimal shTotal = BigDecimal.ZERO;//累计伤害量
        BigDecimal damoCount = BigDecimal.ZERO;//大魔叠的次数
        Integer ljCount = 0;//连击次数
        BigDecimal gjl = BigDecimal.ZERO;//攻击力
        boolean jy = false;
        boolean mieXin = true;
        BigDecimal nuqi = BigDecimal.ZERO;
        Map<String, BigDecimal> result = new HashMap<>();
        boolean fj_jy = false;

        //1、第一回合开始前初始化数据
        for(int round = 1; round <= 15; round++) {//共计15回合
            //回合开始初始化数据
            if(!fj_jy) {
                jy = false;
            }
            fj_jy = false;
            ljCount = 0;
            //第一回合，人物出手前，青龙需要先打一次
            if(round == 1){
                gjl = this.getGjl(round, body, damoCount,false, shTotal);
                BigDecimal qinglong = this.getQingLongShangHai(body, gjl);
                nuqi = nuqi.add(body.getXiaoYue());
                shTotal = shTotal.add(qinglong);
                while(this.ljCheck(ljCount, shTotal, body.getLianj())){//连击部分
                    if(ljCount == 0){//第一次打，攻击力比较低
                        BigDecimal renwu = this.getRenWuShangHai(bs, gjl, body, false, true);//人物第一次出手伤害
                        shTotal = shTotal.add(renwu);
                        ljCount++;
                        bs = bs.add(body.getQlbs());
                        jy = this.jyCheck(shTotal, body.getJiyun(), round);
                        nuqi = nuqi.add(body.getPg());
                        this.setMap(result, "gj", BigDecimal.ONE);
                        continue;
                    }
                    gjl = this.getGjl(round, body, damoCount,true, shTotal);
                    BigDecimal renwu = this.getRenWuShangHai(bs, gjl, body, mieXin, true);//人物连击后伤害
                    shTotal = shTotal.add(renwu);
                    bs = bs.add(body.getQlbs());
                    this.setMap(result, "gj", BigDecimal.ONE);
                    if(!jy) {
                        jy = this.jyCheck(shTotal, body.getJiyun(), round);
                    }
                    mieXin = !mieXin;
                    ljCount++;
                }
                if(this.fjCheck(jy, shTotal, body.getFanj())){
                    damoCount = damoCount.add(BigDecimal.ONE);
                    this.setMap(result, "fj", BigDecimal.ONE);
                    ljCount = 0;

                    while(this.ljCheck(ljCount, shTotal, body.getLianj())){//连击部分
                        gjl = this.getGjl(round, body, damoCount,true, shTotal);
                        BigDecimal renwu = this.getRenWuShangHai(bs, gjl, body, mieXin, true);//人物连击后伤害
                        shTotal = shTotal.add(renwu);
                        if(!jy) {
                            jy = this.jyCheck(shTotal, body.getJiyun(), round);
                            fj_jy = jy;
                        }
                        bs = bs.add(body.getQlbs());
                        mieXin = !mieXin;
                        ljCount++;
                        this.setMap(result, "gj", BigDecimal.ONE);
                    }
                }
            }else{
                //2-15回合
                gjl = this.getGjl(round, body, damoCount,true, shTotal);
                BigDecimal qinglong = this.getQingLongShangHai(body, gjl);
                nuqi = nuqi.add(body.getXiaoYue());
                shTotal = shTotal.add(qinglong);
                if(this.xieTong(round)){
                    nuqi = nuqi.add(body.getXiaoYue());
                    shTotal = shTotal.add(qinglong);
                }
                if(new BigDecimal(10000).compareTo(nuqi) == -1){//释放道法，道法不计算连击
                    gjl = this.getGjl(round, body, damoCount,true, shTotal);
                    boolean bjCheck = this.bjCheck(shTotal, body.getBaoj());
                    BigDecimal sh = this.daoFaShangHai(body, gjl, bs, mieXin, bjCheck);
                    shTotal = shTotal.add(sh);
                    System.out.println("在第"+round+"回合释放了道法，本次道法伤害为:" + sh.setScale(2, RoundingMode.FLOOR));
                    bs = bs.add(body.getQlbs());
                    if(bjCheck) {
                        mieXin = !mieXin;
                    }else{
                        mieXin = true;
                    }
                    ljCount++;
                    if(!jy) {
                        jy = this.jyCheck(shTotal, body.getJiyun(), round);
                    }
                    nuqi = new BigDecimal(3000);
                    this.setMap(result, "gj", BigDecimal.ONE);
                }else {
                    nuqi = nuqi.add(body.getPg());
                }

                while(this.ljCheck(ljCount, shTotal, body.getLianj())){//连击部分
                    gjl = this.getGjl(round, body, damoCount,true, shTotal);
                    boolean bjCheck = this.bjCheck(shTotal, body.getBaoj());
                    BigDecimal renwu = this.getRenWuShangHai(bs, gjl, body, mieXin, bjCheck);//人物连击后伤害
                    shTotal = shTotal.add(renwu);
                    bs = bs.add(body.getQlbs());
                    if(bjCheck) {
                        mieXin = !mieXin;
                    }else{
                        mieXin = true;
                    }
                    ljCount++;
                    if(!jy) {
                        jy = this.jyCheck(shTotal, body.getJiyun(), round);
                    }
                    this.setMap(result, "gj", BigDecimal.ONE);
                }

                if(this.fjCheck(jy, shTotal, body.getFanj())){
                    damoCount = damoCount.add(BigDecimal.ONE);
                    this.setMap(result, "fj", BigDecimal.ONE);
                    ljCount = 0;

                    while(this.ljCheck(ljCount, shTotal, body.getLianj())){//连击部分
                        gjl = this.getGjl(round, body, damoCount,true, shTotal);
                        boolean bjCheck = this.bjCheck(shTotal, body.getBaoj());
                        BigDecimal renwu = this.getRenWuShangHai(bs, gjl, body, mieXin, bjCheck);//人物连击后伤害
                        shTotal = shTotal.add(renwu);
                        if(!jy) {
                            jy = this.jyCheck(shTotal, body.getJiyun(), round);
                            fj_jy = jy;
                        }
                        bs = bs.add(body.getQlbs());
                        if(bjCheck) {
                            mieXin = !mieXin;
                        }else{
                            mieXin = true;
                        }
                        ljCount++;
                        this.setMap(result, "gj", BigDecimal.ONE);
                    }
                }
            }
            this.setMap(result, "bs", bs.divide(new BigDecimal(1), 2 ,BigDecimal.ROUND_HALF_UP));
        }

        this.setMap(result, "total", shTotal.divide(new BigDecimal(10000), 2 ,BigDecimal.ROUND_HALF_UP));
        System.out.println(result);

        return result;
    }

    private void setMap(Map<String, BigDecimal> map, String key, BigDecimal value){
        if(map.containsKey(key)){
            BigDecimal temp = map.get(key);
            map.put(key, temp.add(value));
        }else {
            map.put(key, value);
        }
    }

    //乌龟
    private boolean xieTong(Integer round){
        if(round%2 == 0){
            List<WeightRandom.WeightObj<String>> list = new ArrayList<WeightRandom.WeightObj<String>>();
            list.add(new WeightRandom.WeightObj<>("协同", 25));
            list.add(new WeightRandom.WeightObj<>("不协同", 75));
            WeightRandom wr = RandomUtil.weightRandom(list);
            String result = wr.next().toString();
            if("不协同".equals(result)){
                return false;
            }
            return true;
        }
        return false;
    }

    private BigDecimal computeBox(BigDecimal totalSh) {
        if(this.betweenShBox("0", "323", totalSh)) {//24
            return new BigDecimal(130);
        }
        if(this.betweenShBox("323", "1483.95", totalSh)) {//25
            return new BigDecimal(145);
        }
        if(this.betweenShBox("1483.95", "5348.64", totalSh)) {//26
            return new BigDecimal(160);
        }
        if(this.betweenShBox("5348.64", "16200", totalSh)) {//26
            return new BigDecimal(175);
        }
        if(this.betweenShBox("16200", "65400", totalSh)) {//27
            return new BigDecimal(190);
        }
        if(this.betweenShBox("65400", "229200", totalSh)) {//28
            return new BigDecimal(205);
        }

        BigDecimal line = new BigDecimal("229200");
        BigDecimal nextLine = new BigDecimal("229200");
        BigDecimal addValue = new BigDecimal("163800");
        BigDecimal kangxing = new BigDecimal("15");
        BigDecimal jcKangXing = new BigDecimal("205");
        while(true) {
            nextLine = nextLine.add(addValue);
            if(this.betweenShBox(line, nextLine, totalSh)) {
                return jcKangXing.add(kangxing);
            }
            kangxing = kangxing.add(new BigDecimal(15));
        }
    }

    private Boolean betweenShBox(String min, String max, BigDecimal totalSh) {
        BigDecimal tempMin = new BigDecimal(min);
        BigDecimal tempMax = new BigDecimal(max);
        if(tempMin.compareTo(totalSh) == -1 && tempMax.compareTo(totalSh) == 1) {
            return true;
        }
        return false;
    }

    private Boolean betweenShBox(BigDecimal min, BigDecimal max, BigDecimal totalSh) {
        if(min.compareTo(totalSh) == -1 && max.compareTo(totalSh) == 1) {
            return true;
        }
        return false;
    }

    //道法伤害=攻击力*道法伤害系数*(1+增伤)*(2+爆伤)*(1+道伤加成)*(1+鬼将+灭心)
    private BigDecimal daoFaShangHai(YiShouBody body, BigDecimal gjl, BigDecimal bs, boolean mieXin, boolean bj) {

         BigDecimal result = gjl
                .multiply(body.getDfxs().add(BigDecimal.ONE))
                .multiply(BigDecimal.ONE.add(body.getZengShang()))
                .multiply(body.getBian72());

        if(bj) {
            if(mieXin){
                result = result.multiply(BigDecimal.ONE.add(body.getMieXin()).add(body.getGuiJiang().add(body.getWangquanfugui())));
            }else{
                result = result.multiply(BigDecimal.ONE.add(body.getGuiJiang().add(body.getWangquanfugui())));
            }
            return result.multiply(new BigDecimal(2).add(bs));
        }else{
            return result.multiply(BigDecimal.ONE.add(body.getGuiJiang()));
        }
    }

    private boolean jyCheck(BigDecimal totalSh, BigDecimal jy, Integer round) {
        BigDecimal kangxing = this.computeBox(totalSh);
        if(kangxing.compareTo(jy) == 1){
            return false;
        }
        Integer jyTemp = jy.subtract(kangxing).intValue();
        List<WeightRandom.WeightObj<String>> list = new ArrayList<WeightRandom.WeightObj<String>>();
        list.add(new WeightRandom.WeightObj<>("击晕", jyTemp));
        list.add(new WeightRandom.WeightObj<>("不击晕", 80 - jyTemp));
        WeightRandom wr = RandomUtil.weightRandom(list);
        String result = wr.next().toString();

        if(result.equals("击晕")){
            return true;
        }
        return false;
    }

    private Boolean bjCheck(BigDecimal totalSh, BigDecimal bj) {
        BigDecimal kangxing = this.computeBox(totalSh);
        if(kangxing.compareTo(bj) == 1){
            return false;
        }
        Integer jyTemp = bj.subtract(kangxing).intValue();
        if(new BigDecimal(jyTemp).subtract(attribute_warning).compareTo(BigDecimal.ZERO) == -1){
            System.out.println("暴击属性预警，当前抗性为" + kangxing + ",我的主属性为：" + bj.intValue());
        }
        List<WeightRandom.WeightObj<String>> list = new ArrayList<WeightRandom.WeightObj<String>>();
        list.add(new WeightRandom.WeightObj<>("暴击", jyTemp));
        list.add(new WeightRandom.WeightObj<>("不暴击", 100 - jyTemp));
        WeightRandom wr = RandomUtil.weightRandom(list);
        String result = wr.next().toString();

        if(result.equals("暴击")){
            return true;
        }
        //System.out.println("没有暴击" + totalSh.intValue());
        return false;
    }

    //连击检测
    private boolean ljCheck(Integer ljCount, BigDecimal totalSh, BigDecimal lj) {
        //第一次共计和第一次连击必须触发，第一次必须连击，剩下的判断是否是50%
        if(Arrays.asList(0).contains(ljCount)){
            return true;
        }

        BigDecimal kangxing = this.computeBox(totalSh);
        if(kangxing.compareTo(lj) == 1){
            return false;
        }
        Integer fjTemp = lj.subtract(kangxing).intValue();

        if(fjTemp >= 100){
            fjTemp = 100;
        }

        if(new BigDecimal(fjTemp).subtract(attribute_warning).compareTo(BigDecimal.ZERO) == -1 ){
            System.out.println("连击属性预警，当前抗性为" + kangxing + ",我的主属性为：" + lj.intValue());
        }

        List<WeightRandom.WeightObj<String>> list = new ArrayList<WeightRandom.WeightObj<String>>();
        //第一次100，第2次100，第3次50 2，第四次25 4，第五次12.5 8，第6次6.25 16
        Integer ljgv = (int) (fjTemp/Math.pow(2, ljCount-1));//2:50,3:25,4:
        list.add(new WeightRandom.WeightObj<>("连击", ljgv));
        list.add(new WeightRandom.WeightObj<>("不连击", 100 - ljgv));
        WeightRandom wr = RandomUtil.weightRandom(list);
        String result = wr.next().toString();
        if("不连击".equals(result)){
            return false;
        }
        return true;
    }

    //反击检测
    private boolean fjCheck(boolean jy, BigDecimal totalSh, BigDecimal fj) {
        if(jy){//晕住了不反击
            return false;
        }
        BigDecimal kangxing = this.computeBox(totalSh);
        if(kangxing.compareTo(fj) == 1){
            return false;
        }
        Integer fjTemp = fj.subtract(kangxing).intValue();

//        if(new BigDecimal(fjTemp).subtract(attribute_warning).compareTo(BigDecimal.ZERO) == -1 ){
//            System.out.println("反击属性预警，当前抗性为" + kangxing + ",我的主属性为：" + fj.intValue());
//        }

        List<WeightRandom.WeightObj<String>> list = new ArrayList<WeightRandom.WeightObj<String>>();
        list.add(new WeightRandom.WeightObj<>("反击", fjTemp));
        list.add(new WeightRandom.WeightObj<>("不反击", 80 - fjTemp));
        WeightRandom wr = RandomUtil.weightRandom(list);
        String result = wr.next().toString();
        if("不反击".equals(result)){
            return false;
        }
        return true;
    }

    //人物伤害计算，攻击力*(2+爆伤)*(1+增伤)*(1+鬼将+灭心)
    private BigDecimal getRenWuShangHai(BigDecimal bs, BigDecimal gjl, YiShouBody body, boolean mieXin, Boolean bj){

        BigDecimal jc = BigDecimal.ONE.add(body.getZengShang());
        if(bj) {
            if(mieXin){
                jc = jc.multiply(BigDecimal.ONE.add(body.getMieXin()).add(body.getGuiJiang().add(body.getWangquanfugui())));
            }else{
                jc = jc.multiply(BigDecimal.ONE.add(body.getGuiJiang().add(body.getWangquanfugui())));
            }
            return gjl.multiply(new BigDecimal(2).add(bs)).multiply(jc);
        }else{
            return gjl.multiply(jc);
        }

    }

    //青龙伤害，伤害=1.08*(1+增伤)*(1+强灵)*攻击力
    private BigDecimal getQingLongShangHai(YiShouBody body, BigDecimal gjl){
        return gjl.multiply(body.getQingLong()).multiply(body.getZengShang().add(BigDecimal.ONE)).multiply(body.getJcql().add(BigDecimal.ONE));
    }

    //获取攻击力
    private BigDecimal getGjl(int round, YiShouBody body, BigDecimal damoCount, Boolean isBaoQi, BigDecimal totalSh) {
        //第一回合初始化攻击力，还没有触发宝器阁主，青龙和第一次出手没有宝器，后面的都需要有宝器
        BigDecimal xs = BigDecimal.ONE.add(body.getXiangFuRen()).add(body.getJinJiaoJian()).add(new BigDecimal(0.1));

        if(round == 1){
            if (isBaoQi){
                xs = xs.add(body.getBaoQiGeZhu());
            }
            return body.getGjl().multiply(xs);
        }

        if(damoCount.compareTo(new BigDecimal(5)) == 1){
            damoCount = new BigDecimal(5);
        }

        if(Arrays.asList(3,4,5).contains(round)) {
            xs = xs.add(new BigDecimal(0.044));
        }
        if(Arrays.asList(6,7,8).contains(round)) {
            xs = xs.add(new BigDecimal(0.088));
        }
        if(round > 8) {
            xs = xs.add(new BigDecimal(0.132));
        }

        BigDecimal jc = BigDecimal.ZERO;

        if(this.bjCheck(totalSh, body.getBaoj())) {
            jc = body.getBaoQiGeZhu().add(damoCount.multiply(body.getDaMo())).add(xs);
        }else{
            jc = damoCount.multiply(body.getDaMo()).add(xs);
        }

        return body.getGjl().multiply(jc);
    }


    public static void main(String[] args) {


        YiShouHandle handle = new YiShouHandle();

        YiShouBody yiShouBody = new YiShouBody();


//        for(int count = 1; count <= 50; count++){
//            Map<String, BigDecimal> result = handle.handle(yiShouBody);
//            System.out.println("本次造成伤害：" + result.get("total") + ",攻击次数：" + result.get("gj") + ",反击次数：" + result.get("fj") + ",击晕次数：" + result.get("jy"));
//        }

        for(double cishu = 400; cishu <= 400; cishu=cishu + 1) {
            System.out.println("攻击力每增长一亿所能打出的异兽伤害,当前攻击力：" + cishu + "亿,得到数据如下:");
            yiShouBody.setGjl(new BigDecimal(cishu));
            //每5次为一组，执行1000组看数据
            int count = 1;
            List<BigDecimal> result = new ArrayList<>();
            Map<Integer,List<BigDecimal>> param = new HashMap<>();
            for(int i = 1; i <= count ; i++){
                List<BigDecimal> a = new ArrayList<>();
                for(int j = 1; j<=1; j++) {
                    BigDecimal b = handle.handle(yiShouBody).get("total");
                    result.add(b);
                    a.add(b);
                }
                param.put(i, a);
            }

            BigDecimal big = result.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal danci = big.divide(new BigDecimal(result.size()),2, BigDecimal.ROUND_HALF_UP);
            BigDecimal max = result.stream().max(BigDecimal::compareTo).get();
            BigDecimal min = result.stream().min(BigDecimal::compareTo).get();
            System.out.println("共计执行：" + count * 5 + "次，" + count + "组数据");
            System.out.println("平均单次伤害：" + danci + ", 总伤害为: " + danci.multiply(new BigDecimal(5)));
            System.out.println("单次最大值为" + max + ",单次最小值为：" + min);

            BigDecimal groupMax = param.values().stream().map(e->e.stream().reduce(BigDecimal.ZERO,BigDecimal::add)).max(BigDecimal::compareTo).get();
            BigDecimal groupMin = param.values().stream().map(e->e.stream().reduce(BigDecimal.ZERO,BigDecimal::add)).min(BigDecimal::compareTo).get();
            System.out.println("每组最大分值" + groupMax + ",每组最小值为：" + groupMin);
            System.out.println("-----------------------------------------------------------------------------------------");
        }



    }



}
