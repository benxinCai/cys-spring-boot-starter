package com.cys.kaduo.notice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.kaduo.notice.domain.CysNotice;

/**
 * 通知公告Mapper接口
 * 
 * @author cys
 * @date 2023-10-13
 */
public interface CysNoticeMapper extends BaseMapper<CysNotice> {

}
