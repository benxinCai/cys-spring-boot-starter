package com.cys.kaduo.rolemenu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;

import java.util.List;

/**
 * 小程序菜单关联表Service接口
 * 
 * @author cys
 * @date 2023-09-04
 */
public interface ICysRoleMinimenuService extends IService<CysRoleMinimenu>
{
    /**
     * 查询小程序菜单关联表
     * 
     * @param id 小程序菜单关联表主键
     * @return 小程序菜单关联表
     */
    public CysRoleMinimenu selectCysRoleMinimenuById(Long id);

    /**
     * 查询小程序菜单关联表列表
     * 
     * @param cysRoleMinimenu 小程序菜单关联表
     * @return 小程序菜单关联表集合
     */
    public List<CysRoleMinimenu> selectCysRoleMinimenuList(CysRoleMinimenu cysRoleMinimenu);

    /**
     * 新增小程序菜单关联表
     * 
     * @param cysRoleMinimenu 小程序菜单关联表
     * @return 结果
     */
    public int insertCysRoleMinimenu(CysRoleMinimenu cysRoleMinimenu);

    /**
     * 修改小程序菜单关联表
     * 
     * @param cysRoleMinimenu 小程序菜单关联表
     * @return 结果
     */
    public int updateCysRoleMinimenu(CysRoleMinimenu cysRoleMinimenu);

    /**
     * 批量删除小程序菜单关联表
     * 
     * @param ids 需要删除的小程序菜单关联表主键集合
     * @return 结果
     */
    public int deleteCysRoleMinimenuByIds(Long[] ids);

    /**
     * 删除小程序菜单关联表信息
     * 
     * @param id 小程序菜单关联表主键
     * @return 结果
     */
    public int deleteCysRoleMinimenuById(Long id);
}
