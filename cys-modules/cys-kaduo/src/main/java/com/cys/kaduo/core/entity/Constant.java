package com.cys.mynika.entity;

import java.util.ArrayList;
import java.util.List;

public class Constant {

    //不可以申请的角色列表
    public static List<String> noAuthApply = new ArrayList<String>(){{
        add("admin");
        add("common");
        add("auth_manage");
    }};


}
