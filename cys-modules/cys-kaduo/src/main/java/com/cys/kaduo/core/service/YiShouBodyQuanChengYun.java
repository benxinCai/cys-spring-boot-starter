package com.cys.mynika.service;

import java.math.BigDecimal;

public class YiShouBodyQuanChengYun {

    //以下为攻击加成
    private BigDecimal baoQiGeZhu = new BigDecimal(0.44);//宝器阁主加成√
    private BigDecimal xiangFuRen = new BigDecimal(0.22);//香夫人加成
    private BigDecimal jinJiaoJian = new BigDecimal(0.158);//金焦戬加成
    private BigDecimal daMo = new BigDecimal(0.069);//大魔每次攻击获得攻击加成

    private BigDecimal moliqing = new BigDecimal(100);

    private BigDecimal damingwang = new BigDecimal(0);

    private BigDecimal qinglong = new BigDecimal(50);

    //以下为伤害加成
    private BigDecimal guiJiang = new BigDecimal(0.44);//鬼将加成
    private BigDecimal mieXin = new BigDecimal(0.9);//灭心等级
    private BigDecimal qingLong = new BigDecimal(1.08);//青龙攻击加成
    private BigDecimal bian72 = new BigDecimal(2.89);//72变攻击加成
    private BigDecimal xiaoYue = new BigDecimal(2100);//啸月增加妖气1960
    private BigDecimal pg = new BigDecimal(2000);//普通供给增加妖气
    private BigDecimal gjl = new BigDecimal(141.1);//本体面板攻击
    private BigDecimal qlbs = new BigDecimal(0.27);//每次暴击后提升的爆伤
    private BigDecimal zengShang = new BigDecimal(1.811);//增伤√
    private BigDecimal jcbs = new BigDecimal(0.749);//基础爆伤
    private BigDecimal jcql = new BigDecimal(1.11);//基础强灵
    private BigDecimal dfxs = new BigDecimal(0.593);//道法伤害系数

    private BigDecimal baoj = new BigDecimal(520).add(this.damingwang).add(this.qinglong);

    private BigDecimal fanj = new BigDecimal(367).add(this.moliqing);

    private BigDecimal lianj = new BigDecimal(540);

    private BigDecimal jiyun = new BigDecimal(221);

    public BigDecimal getBaoj() {
        return baoj;
    }

    public void setBaoj(BigDecimal baoj) {
        this.baoj = baoj;
    }

    public BigDecimal getFanj() {
        return fanj;
    }

    public void setFanj(BigDecimal fanj) {
        this.fanj = fanj;
    }

    public BigDecimal getLianj() {
        return lianj;
    }

    public void setLianj(BigDecimal lianj) {
        this.lianj = lianj;
    }

    public BigDecimal getJiyun() {
        return jiyun;
    }

    public void setJiyun(BigDecimal jiyun) {
        this.jiyun = jiyun;
    }

    public BigDecimal getDfxs() {
        return dfxs;
    }

    public void setDfxs(BigDecimal dfxs) {
        this.dfxs = dfxs;
    }

    public BigDecimal getQlbs() {
        return qlbs;
    }

    public void setQlbs(BigDecimal qlbs) {
        this.qlbs = qlbs;
    }

    public BigDecimal getZengShang() {
        return zengShang;
    }

    public void setZengShang(BigDecimal zengShang) {
        this.zengShang = zengShang;
    }

    public BigDecimal getJcbs() {
        return jcbs;
    }

    public void setJcbs(BigDecimal jcbs) {
        this.jcbs = jcbs;
    }

    public BigDecimal getJcql() {
        return jcql;
    }

    public void setJcql(BigDecimal jcql) {
        this.jcql = jcql;
    }

    public BigDecimal getBaoQiGeZhu() {
        return baoQiGeZhu;
    }

    public void setBaoQiGeZhu(BigDecimal baoQiGeZhu) {
        this.baoQiGeZhu = baoQiGeZhu;
    }

    public BigDecimal getXiangFuRen() {
        return xiangFuRen;
    }

    public void setXiangFuRen(BigDecimal xiangFuRen) {
        this.xiangFuRen = xiangFuRen;
    }

    public BigDecimal getJinJiaoJian() {
        return jinJiaoJian;
    }

    public void setJinJiaoJian(BigDecimal jinJiaoJian) {
        this.jinJiaoJian = jinJiaoJian;
    }

    public BigDecimal getDaMo() {
        return daMo;
    }

    public void setDaMo(BigDecimal daMo) {
        this.daMo = daMo;
    }

    public BigDecimal getGuiJiang() {
        return guiJiang;
    }

    public void setGuiJiang(BigDecimal guiJiang) {
        this.guiJiang = guiJiang;
    }

    public BigDecimal getMieXin() {
        return mieXin;
    }

    public void setMieXin(BigDecimal mieXin) {
        this.mieXin = mieXin;
    }

    public BigDecimal getQingLong() {
        return qingLong;
    }

    public void setQingLong(BigDecimal qingLong) {
        this.qingLong = qingLong;
    }

    public BigDecimal getBian72() {
        return bian72;
    }

    public void setBian72(BigDecimal bian72) {
        this.bian72 = bian72;
    }

    public BigDecimal getXiaoYue() {
        return xiaoYue;
    }

    public void setXiaoYue(BigDecimal xiaoYue) {
        this.xiaoYue = xiaoYue;
    }

    public BigDecimal getPg() {
        return pg;
    }

    public void setPg(BigDecimal pg) {
        this.pg = pg;
    }

    public BigDecimal getGjl() {
        return gjl;
    }

    public void setGjl(BigDecimal gjl) {
        this.gjl = gjl;
    }
}
