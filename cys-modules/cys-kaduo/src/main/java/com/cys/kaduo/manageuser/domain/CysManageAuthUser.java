package com.cys.kaduo.manageuser.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cys.common.annotation.Excel;
import com.cys.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 小程序授权管理员对象 cys_manage_auth_user
 * 
 * @author cys
 * @date 2023-09-15
 */
@Data
@ApiModel(value="cys_manage_auth_user对象", description="")
@TableName("cys_manage_auth_user")
public class CysManageAuthUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @ApiModelProperty(value = "${comment}")
    @TableField("id")
    private String id;

    /** 管理员ID */
    @ApiModelProperty(value = "${comment}")
    @TableField("user_id")
    private Long userId;

    /** 管理员名称 */
    @Excel(name = "管理员名称")
    @ApiModelProperty(value = "管理员名称")
    @TableField("user_name")
    private String userName;

    /** 管理员昵称 */
    @Excel(name = "管理员昵称")
    @ApiModelProperty(value = "管理员昵称")
    @TableField("nick_name")
    private String nickName;


    /** 角色ID */
    @ApiModelProperty(value = "管理员名称")
    @TableField("role_id")
    private Long roleId;

    /** 角色名称 */
    @Excel(name = "角色名称")
    @ApiModelProperty(value = "角色名称")
    @TableField("role_name")
    private String roleName;

}
