package com.cys.kaduo.sign.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.common.utils.SecurityUtils;
import com.cys.kaduo.qrcode.handle.RedisHandle;
import com.cys.kaduo.sign.domain.CysSign;
import com.cys.kaduo.sign.mapper.CysSignMapper;
import com.cys.kaduo.sign.service.ICysSignService;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 签到管理Service业务层处理
 * 
 * @author cys
 * @date 2023-10-19
 */
@Service
public class CysSignServiceImpl extends ServiceImpl<CysSignMapper, CysSign> implements ICysSignService
{
    /**
     * 查询签到管理
     * 
     * @param id 签到管理主键
     * @return 签到管理
     */
    @Override
    public CysSign selectCysSignById(String id)
    {
        return this.getById(id);
    }

    /**
     * 查询签到管理列表
     * 
     * @param cysSign 签到管理
     * @return 签到管理
     */
    @Override
    public List<CysSign> selectCysSignList(CysSign cysSign)
    {
        LambdaQueryWrapper<CysSign> wrapper = Wrappers.lambdaQuery(cysSign);
        return this.list(wrapper);
    }

    /**
     * 新增签到管理
     * 有子表的话请自行添加
     * @param cysSign 签到管理
     * @return 结果
     */
    @Override
    public int insertCysSign(CysSign cysSign)
    {
        this.save(cysSign);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改签到管理
     * 有子表的话请自行修改
     * @param cysSign 签到管理
     * @return 结果
     */
    @Override
    public int updateCysSign(CysSign cysSign)
    {
        this.updateById(cysSign);
        return 1;
    }

    /**
     * 批量删除签到管理
     * 有子表的话请自行修改
     * @param ids 需要删除的签到管理主键
     * @return 结果
     */
    @Override
    public int deleteCysSignByIds(String[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除签到管理信息
     * 
     * @param id 签到管理主键
     * @return 结果
     */
    @Override
    public int deleteCysSignById(String id)
    {
        this.removeById(id);
        return 1;
    }


    private void initSignData(Long userId){

        String key = "sign:userId:" + userId;
        RedisHandle.getRedis().deleteObject(key);
        QueryWrapper<CysSign> queryWrapper = new QueryWrapper<CysSign>();
        queryWrapper.select("min(sign_date) as sign_date").lambda().eq(CysSign::getUserId, userId);
        List<CysSign> signList = this.list(queryWrapper);//只有一条或者没有
        Date startDate = null;//首次签到时间
        if(CollUtil.isNotEmpty(signList)){//这个人的签到数据不是空
            startDate = signList.get(0).getSignDate();
        }else{
            startDate = new Date();//首次签到时间为今天
        }
        List<CysSign> cysSignList = this.lambdaQuery().eq(CysSign::getUserId, userId).list();
        for(CysSign sign : cysSignList){
            Date endDate = sign.getSignDate();
            if(startDate.getTime() > endDate.getTime()){
                continue;
            }
            long count = DateUtil.betweenDay(startDate, endDate, true);
            RedisHandle.getRedis().redisTemplate.opsForValue().setBit(key, count, true);
        }
    }

    /**
     * 签到
     * @return
     */
    public AjaxResult sign(){

        Long userId = SecurityUtils.getUserId();

        QueryWrapper<CysSign> queryWrapper = new QueryWrapper<CysSign>();
        queryWrapper.apply("to_days(sign_date) = to_days(now())").lambda().eq(CysSign::getUserId, userId);
        List<CysSign> signList = this.list(queryWrapper);

        if(CollUtil.isNotEmpty(signList)){
            return AjaxResult.error("您今日已经签过到了");
        } else {
            Date date = new Date();
            CysSign sign = new CysSign();
            sign.setSignDate(date);
            sign.setYear(DateUtil.format(date, "yyyy"));
            sign.setMonth(DateUtil.format(date, "MM"));
            sign.setDay(DateUtil.format(date, "dd"));
            sign.setUserId(userId);
            sign.setNickName(SecurityUtils.getLoginUser().getUser().getNickName());
            this.save(sign);
            this.initSignData(userId);
        }
        return AjaxResult.success();
    }

    /**
     * 连续签到次数
     * @return
     */
    public int continuousSignCount(){

        SysUser user = SecurityUtils.getLoginUser().getUser();
        QueryWrapper<CysSign> queryWrapper = new QueryWrapper<CysSign>();
        queryWrapper.select("min(sign_date) as sign_date").lambda().eq(CysSign::getUserId, user.getUserId());
        CysSign cysSign = this.getOne(queryWrapper);//只有一条或者没有
        if(ObjectUtil.isNull(cysSign)){
            return 0;
        }
        int countDays = (int)DateUtil.betweenDay(cysSign.getSignDate(), new Date(), true) + 1;

        String key = "sign:userId:" + user.getUserId();
        //5. 获取本月截至今天为止的所有的签到记录，返回的是一个十进制的数字 BITFIELD sign:5:202301 GET u3 0
        List<Long> result = RedisHandle.getRedis().redisTemplate.opsForValue().bitField(
                key,
                BitFieldSubCommands.create()
                        .get(BitFieldSubCommands.BitFieldType.unsigned(countDays)).valueAt(0));
        System.out.println(result);
        //没有任务签到结果
        if (result == null || result.isEmpty()) {
            return 0;
        }
        Long num = result.get(0);
        if (num == null || num == 0) {
            return 0;
        }
        //6. 循环遍历
        int count = 0;
        while (true) {
            //6.1 让这个数字与1 做与运算，得到数字的最后一个bit位 判断这个数字是否为0
            if ((num & 1) == 0) {
                //如果为0，签到结束
                break;
            } else {
                count ++;
            }
            num >>>= 1;
        }
        return count;
    }

    /**
     * 一共签到次数
     * @return
     */
    public Long signCount(){
        String key = "sign:userId:" + SecurityUtils.getUserId();

        return (Long)RedisHandle.getRedis().redisTemplate.execute((RedisCallback<Long>) connection -> connection.bitCount(key.getBytes()));
    }

}
