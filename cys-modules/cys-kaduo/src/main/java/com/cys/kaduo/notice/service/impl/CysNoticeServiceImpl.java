package com.cys.kaduo.notice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.utils.DateUtils;
import com.cys.kaduo.notice.domain.CysNotice;
import com.cys.kaduo.notice.mapper.CysNoticeMapper;
import com.cys.kaduo.notice.service.ICysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 通知公告Service业务层处理
 * 
 * @author cys
 * @date 2023-10-13
 */
@Service
public class CysNoticeServiceImpl extends ServiceImpl<CysNoticeMapper, CysNotice> implements ICysNoticeService
{
    @Autowired
    private CysNoticeMapper cysNoticeMapper;

    /**
     * 查询通知公告
     * 
     * @param noticeId 通知公告主键
     * @return 通知公告
     */
    @Override
    public CysNotice selectCysNoticeByNoticeId(String noticeId)
    {
        return this.getById(noticeId);
    }

    /**
     * 查询通知公告列表
     * 
     * @param cysNotice 通知公告
     * @return 通知公告
     */
    @Override
    public List<CysNotice> selectCysNoticeList(CysNotice cysNotice)
    {
        LambdaQueryWrapper<CysNotice> wrapper = Wrappers.lambdaQuery(cysNotice);
        return this.list(wrapper);
    }

    /**
     * 新增通知公告
     * 有子表的话请自行添加
     * @param cysNotice 通知公告
     * @return 结果
     */
    @Override
    public int insertCysNotice(CysNotice cysNotice)
    {
        cysNotice.setCreateTime(DateUtils.getNowDate());
        this.save(cysNotice);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改通知公告
     * 有子表的话请自行修改
     * @param cysNotice 通知公告
     * @return 结果
     */
    @Override
    public int updateCysNotice(CysNotice cysNotice)
    {
        this.updateById(cysNotice);
        return 1;
    }

    /**
     * 批量删除通知公告
     * 有子表的话请自行修改
     * @param noticeIds 需要删除的通知公告主键
     * @return 结果
     */
    @Override
    public int deleteCysNoticeByNoticeIds(String[] noticeIds)
    {
        this.removeByIds(Arrays.asList(noticeIds));
        return 1;
    }

    /**
     * 删除通知公告信息
     * 
     * @param noticeId 通知公告主键
     * @return 结果
     */
    @Override
    public int deleteCysNoticeByNoticeId(String noticeId)
    {
        this.removeById(noticeId);
        return 1;
    }
}
