package com.cys.kaduo.minimenu.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.core.domain.AjaxResult;
import com.cys.kaduo.minimenu.domain.CysMiniMenu;
import com.cys.kaduo.minimenu.mapper.CysMiniMenuMapper;
import com.cys.kaduo.minimenu.service.ICysMiniMenuService;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;
import com.cys.kaduo.rolemenu.service.ICysRoleMinimenuService;
import com.cys.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 小程序菜单Service业务层处理
 * 
 * @author cys
 * @date 2023-09-04
 */
@Service
public class CysMiniMenuServiceImpl extends ServiceImpl<CysMiniMenuMapper, CysMiniMenu> implements ICysMiniMenuService
{
    @Autowired
    private CysMiniMenuMapper cysMiniMenuMapper;

    @Autowired
    private ICysRoleMinimenuService cysRoleMinimenuService;

    @Autowired
    private ISysRoleService roleService;

    /**
     * 查询小程序菜单
     * 
     * @param id 小程序菜单主键
     * @return 小程序菜单
     */
    @Override
    public CysMiniMenu selectCysMiniMenuById(Long id)
    {
        return this.getById(id);
    }

    /**
     * 查询小程序菜单列表
     * 
     * @param cysMiniMenu 小程序菜单
     * @return 小程序菜单
     */
    @Override
    public List<CysMiniMenu> selectCysMiniMenuList(CysMiniMenu cysMiniMenu)
    {
        LambdaQueryWrapper<CysMiniMenu> wrapper = Wrappers.lambdaQuery(cysMiniMenu);
        return this.list(wrapper);
    }

    public AjaxResult saveRoleMenu(List<CysRoleMinimenu> minimenus){

        if(CollUtil.isEmpty(minimenus)){
            return AjaxResult.success();
        }

        CysRoleMinimenu cysRoleMinimenu = minimenus.get(0);

        List<String> ids = cysRoleMinimenuService.lambdaQuery()
                .eq(CysRoleMinimenu::getMiniMenuId, cysRoleMinimenu.getMiniMenuId())
                .list()
                .stream()
                .map(CysRoleMinimenu::getId)
                .collect(Collectors.toList());

        cysRoleMinimenuService.removeByIds(ids);

        minimenus = minimenus.stream().map(e->{
            String roleName = roleService.selectRoleById(e.getRoleId()).getRoleName();
            e.setRoleName(roleName);
            return e;
        }).collect(Collectors.toList());

        cysRoleMinimenuService.saveBatch(minimenus);

        return AjaxResult.success();
    }

    /**
     * 新增小程序菜单
     * 有子表的话请自行添加
     * @param cysMiniMenu 小程序菜单
     * @return 结果
     */
    @Override
    public int insertCysMiniMenu(CysMiniMenu cysMiniMenu)
    {
        this.save(cysMiniMenu);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改小程序菜单
     * 有子表的话请自行修改
     * @param cysMiniMenu 小程序菜单
     * @return 结果
     */
    @Override
    public int updateCysMiniMenu(CysMiniMenu cysMiniMenu)
    {
        this.updateById(cysMiniMenu);
        return 1;
    }

    /**
     * 批量删除小程序菜单
     * 有子表的话请自行修改
     * @param ids 需要删除的小程序菜单主键
     * @return 结果
     */
    @Override
    public int deleteCysMiniMenuByIds(Long[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除小程序菜单信息
     * 
     * @param id 小程序菜单主键
     * @return 结果
     */
    @Override
    public int deleteCysMiniMenuById(Long id)
    {
        this.removeById(id);
        return 1;
    }
}
