package com.cys.kaduo.manageuser.service.impl;

import cn.hutool.core.collection.ListUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.common.core.domain.entity.SysRole;
import com.cys.common.core.domain.entity.SysUser;
import com.cys.kaduo.manageuser.domain.CysManageAuthUser;
import com.cys.kaduo.manageuser.mapper.CysManageAuthUserMapper;
import com.cys.kaduo.manageuser.service.ICysManageAuthUserService;
import com.cys.system.service.ISysRoleService;
import com.cys.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 小程序授权管理员Service业务层处理
 * 
 * @author cys
 * @date 2023-09-15
 */
@Service
public class CysManageAuthUserServiceImpl extends ServiceImpl<CysManageAuthUserMapper, CysManageAuthUser> implements ICysManageAuthUserService
{

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysUserService userService;

    private String manageRole = "auth_manage";

    /**
     * 查询小程序授权管理员
     * 
     * @param id 小程序授权管理员主键
     * @return 小程序授权管理员
     */
    @Override
    public CysManageAuthUser selectCysManageAuthUserById(String id)
    {
        return this.getById(id);
    }

    /**
     * 查询小程序授权管理员列表
     * 
     * @param cysManageAuthUser 小程序授权管理员
     * @return 小程序授权管理员
     */
    @Override
    public List<CysManageAuthUser> selectCysManageAuthUserList(CysManageAuthUser cysManageAuthUser)
    {
        LambdaQueryWrapper<CysManageAuthUser> wrapper = Wrappers.lambdaQuery(cysManageAuthUser);
        return this.list(wrapper);
    }

    /**
     * 新增小程序授权管理员
     * 有子表的话请自行添加
     * @param cysManageAuthUser 小程序授权管理员
     * @return 结果
     */
    @Transactional
    @Override
    public int insertCysManageAuthUser(CysManageAuthUser cysManageAuthUser)
    {
        roleService.authRole(manageRole, cysManageAuthUser.getUserId());//授权小程序端管理员角色
        String roleName = roleService.selectRoleById(cysManageAuthUser.getRoleId()).getRoleName();
        cysManageAuthUser.setRoleName(roleName);
        this.save(cysManageAuthUser);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改小程序授权管理员
     * 有子表的话请自行修改
     * @param cysManageAuthUser 小程序授权管理员
     * @return 结果
     */
    @Override
    public int updateCysManageAuthUser(CysManageAuthUser cysManageAuthUser)
    {
        this.updateById(cysManageAuthUser);
        return 1;
    }

    /**
     * 批量删除小程序授权管理员
     * 有子表的话请自行修改
     * @param ids 需要删除的小程序授权管理员主键
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteCysManageAuthUserByIds(String[] ids) {
        //一个一个验证删除，主要是需要验证管理员权限是否还需要保留，删干净了就要把小程序端菜单隐藏了，不允许看了
        for(String id : ids){
            this.deleteCysManageAuthUserById(id);
        }
        return 1;
    }

    /**
     * 删除小程序授权管理员信息
     * 
     * @param id 小程序授权管理员主键
     * @return 结果
     */
    @Override
    public int deleteCysManageAuthUserById(String id)
    {
        CysManageAuthUser authUser = this.getById(id);//如果用户ID的数量只有一个，那么就直接把角色干掉吧
        Integer count = this.lambdaQuery().eq(CysManageAuthUser::getUserId, authUser.getUserId()).count();
        if(count == 1){
            roleService.cancelAuth(authUser.getUserId(), manageRole);
        }
        this.removeById(id);
        return 1;
    }

    public List<List<SysRole>> listRoleByUserName(String userName){
        SysRole role = new SysRole();
        role.setAppletShow("是");
        List<SysRole> sysRoleList = roleService.selectRoleList(role);
        SysUser user = userService.selectUserByUserName(userName);
        CysManageAuthUser authUser = new CysManageAuthUser();
        authUser.setUserId(user.getUserId());
        List<CysManageAuthUser> authUserList = this.selectCysManageAuthUserList(authUser);
        List<Long> owenRoleIds = authUserList.stream().map(CysManageAuthUser::getRoleId).collect(Collectors.toList());
        sysRoleList = sysRoleList.stream().map(item->{
            item.setFlag(false);
            if(owenRoleIds.contains(item.getRoleId())){
                item.setFlag(true);
            }
            return item;
        }).collect(Collectors.toList());
        List<List<SysRole>> resultList = ListUtil.split(sysRoleList, 4);
        return resultList;
    }
}
