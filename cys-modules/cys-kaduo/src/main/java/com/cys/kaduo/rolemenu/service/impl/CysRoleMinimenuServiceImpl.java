package com.cys.kaduo.rolemenu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.kaduo.rolemenu.domain.CysRoleMinimenu;
import com.cys.kaduo.rolemenu.mapper.CysRoleMinimenuMapper;
import com.cys.kaduo.rolemenu.service.ICysRoleMinimenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 小程序菜单关联表Service业务层处理
 * 
 * @author cys
 * @date 2023-09-04
 */
@Service
public class CysRoleMinimenuServiceImpl extends ServiceImpl<CysRoleMinimenuMapper, CysRoleMinimenu> implements ICysRoleMinimenuService
{
    @Autowired
    private CysRoleMinimenuMapper cysRoleMinimenuMapper;

    /**
     * 查询小程序菜单关联表
     * 
     * @param id 小程序菜单关联表主键
     * @return 小程序菜单关联表
     */
    @Override
    public CysRoleMinimenu selectCysRoleMinimenuById(Long id)
    {
        return this.getById(id);
    }

    /**
     * 查询小程序菜单关联表列表
     * 
     * @param cysRoleMinimenu 小程序菜单关联表
     * @return 小程序菜单关联表
     */
    @Override
    public List<CysRoleMinimenu> selectCysRoleMinimenuList(CysRoleMinimenu cysRoleMinimenu)
    {
        LambdaQueryWrapper<CysRoleMinimenu> wrapper = Wrappers.lambdaQuery(cysRoleMinimenu);
        return this.list(wrapper);
    }

    /**
     * 新增小程序菜单关联表
     * 有子表的话请自行添加
     * @param cysRoleMinimenu 小程序菜单关联表
     * @return 结果
     */
    @Override
    public int insertCysRoleMinimenu(CysRoleMinimenu cysRoleMinimenu)
    {
        this.save(cysRoleMinimenu);//mp方法，无法返回插入行数，故先给个返回1，证明成功
        return 1;
    }

    /**
     * 修改小程序菜单关联表
     * 有子表的话请自行修改
     * @param cysRoleMinimenu 小程序菜单关联表
     * @return 结果
     */
    @Override
    public int updateCysRoleMinimenu(CysRoleMinimenu cysRoleMinimenu)
    {
        this.updateById(cysRoleMinimenu);
        return 1;
    }

    /**
     * 批量删除小程序菜单关联表
     * 有子表的话请自行修改
     * @param ids 需要删除的小程序菜单关联表主键
     * @return 结果
     */
    @Override
    public int deleteCysRoleMinimenuByIds(Long[] ids)
    {
        this.removeByIds(Arrays.asList(ids));
        return 1;
    }

    /**
     * 删除小程序菜单关联表信息
     * 
     * @param id 小程序菜单关联表主键
     * @return 结果
     */
    @Override
    public int deleteCysRoleMinimenuById(Long id)
    {
        this.removeById(id);
        return 1;
    }
}
