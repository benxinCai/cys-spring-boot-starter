SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cys_table_config
-- ----------------------------
CREATE TABLE `cys_table_config`  (
     `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
     `module` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模块名称',
     `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
     `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '加载的文件名称',
     PRIMARY KEY (`id`) USING BTREE,
     UNIQUE INDEX `module`(`module`) USING BTREE COMMENT '唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '当前模块是否插入了数据' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;

