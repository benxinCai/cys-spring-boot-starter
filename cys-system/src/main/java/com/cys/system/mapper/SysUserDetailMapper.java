package com.cys.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.system.domain.SysUserDetail;


public interface SysUserDetailMapper extends BaseMapper<SysUserDetail> {
}
