package com.cys.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.system.domain.SysUserDetail;
import com.cys.system.mapper.SysUserDetailMapper;
import com.cys.system.service.ISysUserDetailService;
import org.springframework.stereotype.Service;

@Service
public class SysUserDetailServiceImpl  extends ServiceImpl<SysUserDetailMapper, SysUserDetail> implements ISysUserDetailService {
}
