package com.cys.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.system.domain.SysUserDetail;

public interface ISysUserDetailService extends IService<SysUserDetail> {

}
