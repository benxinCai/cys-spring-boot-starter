package com.cys.base.holiday.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.base.holiday.domain.CysHoliday;
import com.cys.base.holiday.mapper.CysHolidayMapper;
import com.cys.base.holiday.service.ICysHolidayService;
import com.cys.common.core.domain.AjaxResult;
import com.cys.common.core.redis.RedisCache;
import com.cys.common.tools.tianxing.handle.api.TianXingApi;
import com.cys.common.utils.StringUtils;
import com.cys.other.util.BeanUtil;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author cys
 * @since 2023-07-21
 */
@Service
public class CysHolidayServiceImpl extends ServiceImpl<CysHolidayMapper, CysHoliday> implements ICysHolidayService {

    /**
     * 每年加载数据
     */
    @PostConstruct
    private void init() {
        //当前年
        String year = DateUtil.format(new Date(), "yyyy");//先看今年有没有，有的话就不处理
        Integer count = this.lambdaQuery().like(CysHoliday::getDate, year).count();
        List<CysHoliday> saveToDbHolidayList = new ArrayList<>();
        if (count == 0) {
            for (int month = 1; month <= 12; month++) {
                String date = year + "-" + month;
                TianXingApi.listHolidayByMouth(date).forEach(item -> {
                    CysHoliday cysHoliday = new CysHoliday();
                    cysHoliday.setCnweekday(item.getCnweekday());
                    cysHoliday.setDate(item.getDate());
                    cysHoliday.setInfo(item.getInfo());
                    cysHoliday.setName(item.getName());
                    cysHoliday.setWeekday(item.getWeekday());
                    cysHoliday.setDaycode(item.getDaycode());
                    cysHoliday.setCreateDate(new Date());
                    saveToDbHolidayList.add(cysHoliday);
                });
            }
        }
        if (CollUtil.isNotEmpty(saveToDbHolidayList)) {
            this.saveBatch(saveToDbHolidayList);
        }
    }

    public AjaxResult jinQiaoWorkHoliday(String time) {
        return AjaxResult.success(jinQiaoWorkHolidayBase(time));
    }


    public JSONArray jinQiaoWorkHolidayBase(String time) {
        String holidayKey = "holiday:jinqiao:work:" + time;
        RedisCache redis = BeanUtil.getRedis();

        JSONArray holidayArray = redis.getCacheObject(holidayKey);
        if (ObjectUtil.isNull(holidayArray)) {
            Optional<CysHoliday> optional = this.lambdaQuery().eq(CysHoliday::getDate, time).oneOpt();
            if (optional.isPresent()) {
                JSONArray result = new JSONArray();
                CysHoliday holiday = optional.get();
                result.add(this.setJsonData("warning", holiday.getCnweekday()));//星期几
                result.add(this.setJsonData("", holiday.getInfo()));//工作日/调休日/双休日
                if (StringUtils.isNotEmpty(holiday.getName())) {
                    result.add(this.setJsonData("info", holiday.getName()));//节日名称
                }
                if (Arrays.asList("0", "3").contains(holiday.getDaycode())) {//正常上班
                    result.add(this.setJsonData("success", "上班"));//正常上班还是加班
                } else {
                    result.add(this.setJsonData("danger", "加班"));//正常上班还是加班
                }
                redis.setCacheObject(holidayKey, result, 30, TimeUnit.DAYS);//30天
                return result;
            } else {
                return new JSONArray();
            }
        }
        return holidayArray;
    }


    private JSONObject setJsonData(String type, String label) {
        JSONObject json = new JSONObject();
        json.put("type", type);
        json.put("label", label);
        return json;
    }

}
