package com.cys.base.holiday.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.base.holiday.domain.CysHoliday;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cys
 * @since 2023-07-21
 */
public interface CysHolidayMapper extends BaseMapper<CysHoliday> {

}
