package com.cys.base.holiday.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author cys
 * @since 2023-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CysHoliday对象", description="")
@TableName("cys_holiday")
public class CysHoliday implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId
    private String id;

    @ApiModelProperty(value = "当前阳历日期")
    @TableField("date")
    private String date;

    @ApiModelProperty(value = "日期类型，为0表示工作日、为1节假日、为2双休日、3为调休日（上班）")
    @TableField("daycode")
    private String daycode;

    @ApiModelProperty(value = "星期（数字）")
    @TableField("weekday")
    private String weekday;

    @ApiModelProperty(value = "星期（中文）")
    @TableField("cnweekday")
    private String cnweekday;

    @ApiModelProperty(value = "文字提示，工作日、节假日、节日、双休日、调休日")
    @TableField("info")
    private String info;

    @ApiModelProperty(value = "节假日名称（中文）")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_date")
    private Date createDate;


}
