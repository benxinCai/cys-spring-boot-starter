package com.cys.base.holiday.service;

import com.alibaba.fastjson2.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.base.holiday.domain.CysHoliday;
import com.cys.common.core.domain.AjaxResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cys
 * @since 2023-07-21
 */
public interface ICysHolidayService extends IService<CysHoliday> {

    public AjaxResult jinQiaoWorkHoliday(String time);

    public JSONArray jinQiaoWorkHolidayBase(String time);
}
