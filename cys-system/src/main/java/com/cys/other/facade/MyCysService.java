package com.cys.other.facade;

/**
 * @author 镜花水月
 * @date 2022年12月06日 9:13
 */

public class MyCysService {

    public String sayHello(String name){
        return String.format("Hello World, %s", name);
    }

}
