package com.cys.other.facade;

import cn.binarywang.wx.miniapp.bean.WxMaCodeLineColor;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.cys.other.wechat.config.WxMaConfiguration;
import com.cys.other.wechat.entity.PayBody;
import com.github.binarywang.wxpay.bean.order.WxPayMpOrderResult;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderRequest;
import com.github.binarywang.wxpay.constant.WxPayConstants;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import me.chanjar.weixin.common.error.WxErrorException;

/**
 * @author 镜花水月
 * @date 2022年12月06日 15:50
 * 这个类会被定义为bean
 */
public class MyWechatService {

    /**
     * 获取不限制的小程序二维码
     * @return
     */
    public byte[] getUnLimitQrCode(String scene, String page) throws WxErrorException {
        Boolean check_path = false;
        String env_version = "release";//"develop";
        Boolean is_hyaline = false;
        Boolean auto_color = false;
        Integer width = 430;
        WxMaCodeLineColor codeLineColor = new WxMaCodeLineColor("0","0","0");
        WxMaConfiguration.getMaService().initHttp();
        return WxMaConfiguration.getMaService().getQrcodeService().createWxaCodeUnlimitBytes(scene, page, check_path,
                env_version, width, auto_color, codeLineColor, is_hyaline);
    }

    /**
     * @Author: cys
     * @Date: 2022/12/6 15:52
     * 通过jsCode换取手机号码
     **/
    public String getPhoneByCode(String jsCode) throws WxErrorException {
        WxMaPhoneNumberInfo numberInfo = WxMaConfiguration.getMaService().getUserService().getNewPhoneNoInfo(jsCode);
        return numberInfo.getPhoneNumber();
    }

    public String getPhoneByCode(String jsCode, String appId) throws WxErrorException {
        WxMaPhoneNumberInfo numberInfo = WxMaConfiguration.getMaService(appId).getUserService().getNewPhoneNoInfo(jsCode);
        return numberInfo.getPhoneNumber();
    }

    /**
     * @Author: cys
     * @Date: 2022/12/6 15:53
     * 通过jsCode换取openId
     **/
    public String getOpenIdByCode(String jsCode) throws WxErrorException {
        WxMaJscode2SessionResult result = WxMaConfiguration.getMaService().getUserService().getSessionInfo(jsCode);
        return result.getOpenid();
    }

    public String getOpenIdByCode(String jsCode, String appId) throws WxErrorException {
        WxMaJscode2SessionResult result = WxMaConfiguration.getMaService(appId).getUserService().getSessionInfo(jsCode);
        return result.getOpenid();
    }

    /**
     * @Author: cys
     * @Date: 2022/12/6 16:07
     * 微信统一下单,返回结果可直接供前台使用
     **/
    public WxPayMpOrderResult unifiedOrder(PayBody body) throws WxPayException {
        WxPayUnifiedOrderRequest request = new WxPayUnifiedOrderRequest();
        request.setOutTradeNo(body.getOutTradeNo());
        request.setNotifyUrl(body.getNotifyUrl());
        request.setTotalFee(body.getTotalFee());
        request.setOpenid(body.getOpenId());

        request.setTradeType(body.getTradeType());
        if(StrUtil.isEmpty(body.getTradeType())){
            request.setTradeType(WxPayConstants.TradeType.JSAPI);
        }

        request.setSpbillCreateIp(body.getSpbillCreateIp());
        if(StrUtil.isEmpty(body.getSpbillCreateIp())){
            request.setSpbillCreateIp("127.0.0.1");
        }

        request.setSignType(body.getSignType());
        if(StrUtil.isEmpty(body.getSignType())){
            request.setSignType(WxPayConstants.SignType.MD5);
        }

        request.setBody(request.getBody());
        WxPayService payService = SpringUtil.getBean(WxPayService.class);
        return payService.createOrder(request);//可以直接返回包装好的参数,前端直接使用即可。
    }
}
