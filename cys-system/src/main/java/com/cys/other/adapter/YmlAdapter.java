package com.cys.other.adapter;

import com.cys.common.core.util.SpringBeanUtil;
import org.springframework.core.env.Environment;


/**
 * Yml文件相关内容
 *
 */
public class YmlAdapter {

    private static Environment environment;

    private YmlAdapter(){}

    //基础用户搭建
    public static YmlAdapter builder(){
        return new YmlAdapter();
    }

    public Integer getTemp() {
        String appId = this.getEnvironmentService().getProperty("temp.appId");
        return Integer.parseInt(appId);
    }

    public String getProperty(String key){
        return this.getEnvironmentService().getProperty(key);
    }

    private Environment getEnvironmentService(){
        if(environment == null){
            environment = SpringBeanUtil.getBean(Environment.class);
        }
        return environment;
    }



}
