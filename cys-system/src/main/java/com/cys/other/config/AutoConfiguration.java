package com.cys.other.config;

import com.cys.other.facade.MyCysService;
import com.cys.other.facade.MyWechatService;
import com.cys.other.init.data.InitDataConfiguration;
import com.cys.other.interceptor.CorsInterceptorConfiguration;
import com.cys.other.wechat.config.WxMaConfiguration;
import com.cys.other.wechat.config.WxPayConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author 镜花水月，暂时不设置开关控制
 * @date 2022年12月06日 9:16
 */
@Configuration
@Import(
        value={
                WxMaConfiguration.class,//小程序
                WxPayConfiguration.class,//支付
                InitDataConfiguration.class,//初始化数据
                CorsInterceptorConfiguration.class//跨域
        })
//@ConditionalOnProperty(prefix = "nika", name = "switch", havingValue = "true")
public class AutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public MyCysService getServiceBean() {
        return new MyCysService();
    }

    @Bean("wechatService")
    @ConditionalOnMissingBean
    public MyWechatService getWechatBean(){
        return new MyWechatService();
    }

}
