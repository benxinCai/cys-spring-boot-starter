package com.cys.other.handle.entity;

import cn.hutool.poi.excel.ExcelWriter;
import lombok.Data;
import org.apache.poi.ss.usermodel.CellStyle;

@Data
public class ExcelBody {

    private ExcelWriter excelWriter;
    private String start;
    private String end;
    private String content;
    private CellStyle cellStyle;
}
