package com.cys.other.handle;

import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.excel.cell.CellLocation;
import com.cys.common.exception.base.BaseException;
import com.cys.other.handle.entity.ExcelBody;
import org.apache.poi.ss.usermodel.CellStyle;

public class ExcelMergeHandle {

    private ExcelBody excelBody;

    private ExcelMergeHandle(){}

    private void setExcelBody(ExcelBody excelBody){
        this.excelBody = excelBody;
    }

    public static ExcelMergeHandle builder(ExcelWriter writer){
        ExcelMergeHandle excelHandle = new ExcelMergeHandle();
        ExcelBody excel = new ExcelBody();
        excel.setExcelWriter(writer);
        excelHandle.setExcelBody(excel);
        return excelHandle;
    }

    public ExcelMergeHandle setStart(String start){
        excelBody.setStart(start);
        return this;
    }

    public ExcelMergeHandle setEnd(String end){
        excelBody.setEnd(end);
        return this;
    }

    public ExcelMergeHandle setContent(String content){
        excelBody.setContent(content);
        return this;
    }

    public ExcelMergeHandle setCellStyle(CellStyle cellStyle){
        excelBody.setCellStyle(cellStyle);
        return this;
    }

    //设置全局属性，也就是基础通用的样式,不可更改
    public ExcelMergeHandle setGlobalStyle(){
        ExcelWriter writer = this.excelBody.getExcelWriter();
        CellStyle style = ExcelStyleHandle.builder(writer)
                .setBorder()
                .setWrapText()
                .setAlignCenter()
                .setVerticalAlignment()
                .setFontSize(11)
                .setFontName("宋体")
                .get();
        return this.setCellStyle(style);
    }

    public void merge(){
        //基础数据
        String start = excelBody.getStart();
        String end = excelBody.getEnd();
        ExcelWriter writer = excelBody.getExcelWriter();
        String content = excelBody.getContent();
        CellStyle cellStyle = excelBody.getCellStyle();

        //基础数据处理
        if(StrUtil.isEmpty(start) || StrUtil.isEmpty(end)){
            throw new BaseException("不存在开始或者结束节点,无法合并.");
        }
        if(StrUtil.isEmpty(content)){//兼容null
            content = "";
        }

        //两个一致的话就直接写了，不需要合并
        if(start.equals(end)){
            writer.writeCellValue(start, content);
            writer.setStyle(cellStyle, start);
            return;
        }

        //合并 选择范围至少要两个cell要不会报错
        CellLocation startLocation = ExcelUtil.toLocation(start);//开始的节点， 如A1，A2
        CellLocation endLocation = ExcelUtil.toLocation(end);//结束的节点， 如B1，B2

        int startCol = startLocation.getX();
        int lastCol = endLocation.getX();

        int startRow = startLocation.getY();
        int endRow = endLocation.getY();

        writer.merge(startRow, endRow, startCol, lastCol , content, false);

        for(int x = startCol; x <= lastCol; x++){
            for(int y = startRow; y <= endRow; y++){
                writer.setStyle(cellStyle, x, y);
            }
        }
    }
}
