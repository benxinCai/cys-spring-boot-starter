package com.cys.other.handle;

import cn.hutool.poi.excel.ExcelWriter;
import org.apache.poi.ss.usermodel.*;

/**
 * 样式基础类，可以考虑成前端基础CSS的样式表，做成样式拼接
 */
public class ExcelStyleHandle {

    private CellStyle style;

    private Font font;

    private ExcelStyleHandle(){}

    public static ExcelStyleHandle builder(ExcelWriter excelWriter){
        ExcelStyleHandle excelStyleHandle = new ExcelStyleHandle();
        excelStyleHandle.style = excelWriter.createCellStyle();
        excelStyleHandle.font = excelWriter.createFont();
        return excelStyleHandle;
    }

    //设置边框
    public ExcelStyleHandle setBorder(){
        style.setBorderBottom(BorderStyle.NONE);
        style.setBorderRight(BorderStyle.NONE);
        style.setBorderLeft(BorderStyle.NONE);
        style.setBorderTop(BorderStyle.NONE);
        return this;
    }

    //设置文字超行换格
    public ExcelStyleHandle setWrapText(){
        this.style.setWrapText(true);
        return this;
    }

    //水平居左
    public ExcelStyleHandle setAlignLeft(){
        style.setAlignment(HorizontalAlignment.LEFT);
        return this;
    }

    //水平居中
    public ExcelStyleHandle setAlignCenter(){
        style.setAlignment(HorizontalAlignment.CENTER);
        return this;
    }

    //水平居右
    public ExcelStyleHandle setAlignRight(){
        style.setAlignment(HorizontalAlignment.RIGHT);
        return this;
    }

    //垂直居中
    public ExcelStyleHandle setVerticalAlignment(){
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        return this;
    }

    //字体
    public ExcelStyleHandle setFontName(String fontName){
        this.font.setFontName(fontName);
        return this;
    }

    //字体加粗
    public ExcelStyleHandle setBold(boolean bold){
        this.font.setBold(bold);
        return this;
    }

    //大小
    public ExcelStyleHandle setFontSize(Integer fontSize){
        this.font.setFontHeightInPoints(fontSize.shortValue());
        return this;
    }

    //字体颜色
    public ExcelStyleHandle setFontName(IndexedColors color){
        this.font.setColor(color.getIndex());//字体颜色
        return this;
    }

    public CellStyle get(){
        style.setFont(font);
        return style;
    }

}
