package com.cys.other.strategy;

import cn.hutool.core.util.StrUtil;
import com.cys.other.exeption.NikaException;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class BuildStrategyUtil<T, R> {

    private String packages;

    private Map<String, Map<String, Function<T, R>>> result = null;

    private Map<String, Function<T, R>> templateMap = null;

    public BuildStrategyUtil(){
        result = new HashMap<>();
        templateMap = new HashMap<>();
    }

    public BuildStrategyUtil<T, R> setPackage(String pack){
        templateMap.clear();
        result.put(pack, templateMap);
        this.packages = pack;
        return this;
    }

    public BuildStrategyUtil<T, R> build(String type, Function<T, R> function){
        if(StrUtil.isEmpty(this.packages)){
            throw new NikaException("不存在包配置，配置项出错，请先设置包名");
        }
        templateMap.put(type, function);
        result.put(this.packages, templateMap);//重复替换
        return this;
    }

    public Map<String, Map<String, Function<T, R>>> end(){
        return result;
    }

}
