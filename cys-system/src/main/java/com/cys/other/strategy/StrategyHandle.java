package com.cys.other.strategy;

import cn.hutool.core.util.ObjectUtil;
import com.cys.common.core.util.SpringBeanUtil;
import com.cys.other.exeption.NikaException;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;

/**
 * 策略处理器,后续可以增加组的概念，使其更通用
 */
@Service
@DependsOn({"springUtil"})
@SuppressWarnings("unchecked")
public class StrategyHandle<T, R> {

    private Map<String, Map<String, Function<T, R>>> functionMap = new HashMap<>();//需要有一个包的概念，否则会使全局都是用一个Map影响效率
    @PostConstruct
    private void initData(){
        Map<String, StrategyService> beans = SpringBeanUtil.getApplicationContext().getBeansOfType(StrategyService.class);
        beans.values().forEach(bean -> {
            Map<String, Map<String, Function>> temp = bean.register();
            for(String packages: temp.keySet()){//第一层是包
                Map<String, Function<T, R>> tempMap = functionMap.get(packages);
                if(ObjectUtil.isEmpty(tempMap)){
                    tempMap = new HashMap<>();
                }
                Map<String, Function> resultMap = temp.get(packages);
                for(String key : resultMap.keySet()){
                    tempMap.put(key, resultMap.get(key));
                }
                functionMap.put(packages, tempMap);
            }
        });
    }

    public R handle(String packages, String type, T param){

        Map<String, Function<T, R>> packageMap = functionMap.get(packages);
        if(ObjectUtil.isEmpty(packageMap)){
            throw new NikaException("不存在该这个包，请注册");
        }

        Function<T, R> function = packageMap.get(type);
        if(ObjectUtil.isEmpty(function)){
            throw new NikaException("此包下不存在这个类型，请注册");
        }

        return function.apply(param);
    }

    public Set<String> listPackage(){
        return functionMap.keySet();
    }

    public Set<String> listTypeByPackage(String pack){
        Map<String, Function<T, R>> packageMap = functionMap.get(pack);
        if(ObjectUtil.isEmpty(packageMap)){
            return new HashSet<>();
        }
        return packageMap.keySet();
    }
}
