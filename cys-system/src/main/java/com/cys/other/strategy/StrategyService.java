package com.cys.other.strategy;

import java.util.Map;
import java.util.function.Function;

public interface StrategyService<T, R> {

    Map<String, Map<String, Function<T, R>>> register();

}
