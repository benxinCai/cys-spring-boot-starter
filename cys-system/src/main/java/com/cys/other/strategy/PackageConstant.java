package com.cys.other.strategy;

/**
 * 策略模式的包常量
 */
public class PackageConstant {

    public final static String INIT_DATA = "initData";

    public final static String MOBAGE_DO_TASK = "mobage_do_task";
}
