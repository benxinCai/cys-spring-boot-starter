package com.cys.other.interceptor;

import com.cys.other.interceptor.entity.CorsFilterProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 镜花水月
 * @date 2022年12月16日 10:30
 * 跨域拦截器
 */
@Configuration
@EnableConfigurationProperties(CorsFilterProperties.class)
@ConditionalOnProperty(prefix = "nika.cors", name = "switch", havingValue = "true")
public class CorsInterceptorConfiguration implements WebMvcConfigurer {

    private static final Logger logger = LoggerFactory.getLogger(CorsInterceptorConfiguration.class);

    @Autowired
    private CorsFilterProperties properties;

    /**
     * @Author: cys
     * @Date: 2022/12/16 13:56
     * 过滤器，用于配置跨域请求，可以使用动态配置，
     * 可插拔式配置，以及请求路径和请求参数
     **/
    @Bean
    public CorsFilter corsFilter(){
        logger.info("========== start load cors setting ==========");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        String accessControlAllowCredentials = properties.getAccessControlAllowCredentials();
        if(StringUtils.isBlank(accessControlAllowCredentials)){
            accessControlAllowCredentials = "false";
        }
        boolean isCookies = false;//填错了就是false。
        if("true".equals(accessControlAllowCredentials)){
            isCookies = true;
        }
        if("false".equals(accessControlAllowCredentials)){
            isCookies = false;
        }
        config.setAllowCredentials(isCookies);

        // 设置访问源地址
        String accessControlAllowOrigin = properties.getAccessControlAllowOrigin();//允许访问的源
        if(StringUtils.isBlank(accessControlAllowOrigin)){//没有源
            config.addAllowedOrigin("*");
        }else{
            String origins[] = accessControlAllowOrigin.split(",");
            for(String origin : origins){
                if(StringUtils.isNoneBlank(origin)){
                    config.addAllowedOrigin(origin.trim());
                }
            }
        }


        // 设置访问源请求头
        String accessControlAllowHeaders = properties.getAccessControlAllowHeaders();//允许访问的源
        if(StringUtils.isBlank(accessControlAllowHeaders)){//没有消息头
            config.addAllowedHeader("*");
        }else{
            String heads[] = accessControlAllowHeaders.split(",");
            for(String head : heads){
                if(StringUtils.isNoneBlank(head)){
                    config.addAllowedHeader(head.trim());
                }
            }
        }

        // 设置访问源请求方法
        String accessControlAllowMethods = properties.getAccessControlAllowMethods();//允许访问的源
        if(StringUtils.isBlank(accessControlAllowMethods)){//没有配置方法
            config.addAllowedMethod("*");
        }else{
            String methods[] = accessControlAllowMethods.split(",");
            for(String method : methods){
                if(StringUtils.isNoneBlank(method)){
                    config.addAllowedMethod(method.trim());
                }
            }
        }

        // 需要暴露的请求头
        String accessControlExposeHeaders = properties.getAccessControlExposeHeaders();//允许访问的源
        if(StringUtils.isBlank(accessControlExposeHeaders)){//没有配置暴露头
            config.addExposedHeader("*");
        }else{
            String exposeHeaders[] = accessControlExposeHeaders.split(",");
            for(String exposeHeader : exposeHeaders){
                if(StringUtils.isNoneBlank(exposeHeader)){
                    config.addExposedHeader(exposeHeader.trim());
                }
            }
        }

        // 对接口配置跨域设置
        if(StringUtils.isBlank(this.properties.getAccessControlUrl())){//没配置那就是所有都可以
            source.registerCorsConfiguration("/**", config);
        }else{
            String[] urls = this.properties.getAccessControlUrl().split(",");
            for(String url : urls){
                source.registerCorsConfiguration(url, config);
            }
        }
        return new CorsFilter(source);
    }


}
