package com.cys.other.interceptor.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author 镜花水月
 * @date 2022年12月16日 14:04
 */
@ConfigurationProperties(prefix = "nika.cors")
public class CorsFilterProperties {

    private String accessControlAllowOrigin; //支持的源

    private String accessControlAllowHeaders; //支持请求头

    private String accessControlAllowMethods; //支持的请求方法

    private String accessControlUrl; //允许跨域的路径

    private String accessControlExposeHeaders; //需要暴露的头

    private String accessControlAllowCredentials; //是否允许接收跨域的Cookie凭证数据

    public String getAccessControlAllowOrigin() {
        return accessControlAllowOrigin;
    }

    public void setAccessControlAllowOrigin(String accessControlAllowOrigin) {
        this.accessControlAllowOrigin = accessControlAllowOrigin;
    }

    public String getAccessControlAllowHeaders() {
        return accessControlAllowHeaders;
    }

    public void setAccessControlAllowHeaders(String accessControlAllowHeaders) {
        this.accessControlAllowHeaders = accessControlAllowHeaders;
    }

    public String getAccessControlAllowMethods() {
        return accessControlAllowMethods;
    }

    public void setAccessControlAllowMethods(String accessControlAllowMethods) {
        this.accessControlAllowMethods = accessControlAllowMethods;
    }

    public String getAccessControlUrl() {
        return accessControlUrl;
    }

    public void setAccessControlUrl(String accessControlUrl) {
        this.accessControlUrl = accessControlUrl;
    }

    public String getAccessControlExposeHeaders() {
        return accessControlExposeHeaders;
    }

    public void setAccessControlExposeHeaders(String accessControlExposeHeaders) {
        this.accessControlExposeHeaders = accessControlExposeHeaders;
    }

    public String getAccessControlAllowCredentials() {
        return accessControlAllowCredentials;
    }

    public void setAccessControlAllowCredentials(String accessControlAllowCredentials) {
        this.accessControlAllowCredentials = accessControlAllowCredentials;
    }
}
