package com.cys.other.exeption;

/**
 * @author 镜花水月,自定义注解
 * @date 2022年12月13日 15:14
 */
public class NikaException extends RuntimeException{

    private String message;

    public NikaException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
