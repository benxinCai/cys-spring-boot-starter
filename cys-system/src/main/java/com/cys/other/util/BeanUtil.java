package com.cys.other.util;

import com.cys.common.core.redis.RedisCache;
import com.cys.common.core.util.SpringBeanUtil;
import com.cys.system.service.ISysUserService;

public class BeanUtil {

    private static RedisCache redis = null;

    private static ISysUserService userService = null;

    public static RedisCache getRedis(){
        if(redis == null){
            redis = SpringBeanUtil.getBean(RedisCache.class);
        }
        return redis;
    }

    public static ISysUserService getUserService(){
        if(userService == null){
            userService = SpringBeanUtil.getBean(ISysUserService.class);
        }
        return userService;
    }
}
