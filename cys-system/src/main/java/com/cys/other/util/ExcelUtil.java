package com.cys.other.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.log.StaticLog;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelWriter;
import com.cys.common.annotation.Excel;
import com.cys.common.utils.ServletUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ExcelUtil {

    public static <T> void downloadTemplateToExcel(Class<T> clazz, String sheetName, Supplier<List<T>> supplier) {
        // 通过工具类创建writer
        ExcelWriter writer = cn.hutool.poi.excel.ExcelUtil.getWriter(true);
        //第一页，基础信息
        writer.renameSheet(sheetName);

        Map<String, String> headers = getDataHeaderToExport(clazz);

        List<T> tempList = supplier.get();
        List<Map<String, Object>> excelList = new ArrayList<>();

        if (CollUtil.isNotEmpty(tempList)) {
            excelList = tempList.stream().map(data -> {
                Map<String, Object> resultMap = new HashMap<>();
                for (String name : headers.keySet()) {
                    Object value = ReflectUtil.getFieldValue(data, name);
                    resultMap.put(name, value);
                }
                return resultMap;
            }).collect(Collectors.toList());
        } else {
            Map<String, Object> temp = new HashMap<>();
            headers.keySet().forEach(key -> {
                temp.put(key, "");
            });
            excelList.add(temp);
        }

        for (int i = 0; i < headers.size(); i++) {
            writer.setColumnWidth(i, 25);
        }

        writer.setHeaderAlias(headers);
        writer.write(excelList, true);
        handleExcelIO(writer);
    }

    public static <T> void downloadTemplateToExcel(Class<T> clazz, String sheetName) {
        // 通过工具类创建writer
        ExcelWriter writer = cn.hutool.poi.excel.ExcelUtil.getWriter(true);
        //第一页，基础信息
        writer.renameSheet(sheetName);

        Map<String, String> headers = getDataHeaderToExport(clazz);

        Map<String, String> temp = new HashMap<>();//作为一个空的值，否则不会输出标题
        headers.keySet().forEach(key -> {
            temp.put(key, "");
        });

        for (int i = 0; i < headers.size(); i++) {
            writer.setColumnWidth(i, 25);
        }

        writer.setHeaderAlias(headers);
        writer.write(Arrays.asList(temp), true);
        handleExcelIO(writer);
    }

    public static <T> List<T> readDataFromExcel(MultipartFile file, Class<T> clazz, String sheetName) {
        ExcelReader excelReader = null;
        try {
            excelReader = cn.hutool.poi.excel.ExcelUtil.getReader(file.getInputStream());
        } catch (Exception e) {
            StaticLog.error("读取导入的excel数据失败");
            throw new RuntimeException(e.getMessage());
        }
        excelReader.setSheet(sheetName);//原料及产品单价导入模板

        Map<String, String> collect = getDataHeaderToImport(clazz);

        excelReader.setHeaderAlias(collect);
        return excelReader.readAll(clazz);
    }

    public static void handleExcelIO(ExcelWriter writer) {
        ServletOutputStream out = null;
        // 关闭writer，释放内存
        HttpServletResponse response = ServletUtils.getResponse();
        StringBuffer sb = new StringBuffer();
        sb.append("export").append(new Date().getTime()).append(".xlsx");
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
        response.setHeader("Content-Disposition", "attachment;filename=" + sb.toString());
        try {
            out = response.getOutputStream();
        } catch (Exception e) {
            StaticLog.error("获取输出流失败,失败原因未:{}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        } finally {
            writer.flush(out, true);
            writer.close();
            IoUtil.close(out);
        }
    }


    public static Map<String, String> getDataHeaderToImport(Class clazz) {
        return getBaseHeader(clazz)
                .entrySet()
                .stream()
                .collect(Collectors.toMap(entry -> entry.getValue(), entry -> entry.getKey()));
    }

    public static Map<String, String> getDataHeaderToExport(Class clazz) {
        return getBaseHeader(clazz);
    }

    private static Map<String, String> getBaseHeader(Class clazz) {
        Map<String, String> headers = new LinkedHashMap<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Excel excel = field.getAnnotation(Excel.class);
            if (ObjectUtil.isNotEmpty(excel)) {
                headers.put(field.getName(), excel.name());
            }
        }
        return headers;
    }

}
