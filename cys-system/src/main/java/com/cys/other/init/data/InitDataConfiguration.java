package com.cys.other.init.data;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.cys.other.exeption.NikaException;
import com.cys.other.init.entity.TableConfig;
import com.cys.other.strategy.PackageConstant;
import com.cys.other.strategy.StrategyHandle;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.FileReader;
import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author 镜花水月
 * @date 2022年12月12日 16:16
 */
@Configuration
@ConditionalOnProperty(prefix = "nika.initData", name = "switch", havingValue = "true")
public class InitDataConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(InitDataConfiguration.class);

    @Autowired
    private StrategyHandle<String, TableConfig> strategyHandle;

    @Autowired
    private DataSource dataSource;

    @Autowired
    ResourceLoader resourceLoader;


    /**
     * @Author: cys
     * @Date: 2022/12/13 16:56
     * 获取配置里面的所有字段，如果配置为true，那么就加在相应的数据
     **/
    @PostConstruct
    public void runScript() throws SQLException {

        logger.info("------------------ init data by nika start -------------------------");
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("show tables like '%cys_table_config%'");
        ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()){//存在
            logger.info("存在config,不再重新加载");
        }else{
            this.createConfigTable(connection);
        }
        this.loadModuleData(connection);

        connection.close();
        statement.close();
        resultSet.close();
        logger.info("------------------ init data by nika end -------------------------");
    }

    /**
     * 加载模块数据
     */
    private void loadModuleData(Connection connection) throws SQLException {
        Set<String> setKeys = this.strategyHandle.listTypeByPackage(PackageConstant.INIT_DATA);
        List<TableConfig> tableConfigList = this.readTableConfigList(connection);
        for(String key : setKeys){
            TableConfig config = this.strategyHandle.handle(PackageConstant.INIT_DATA, key, "");//加载模块返回的应该是文件名称
            if(StrUtil.isEmpty(config.getAdd())){//空不加载
                continue;
            }
            if(!"true".equals(config.getAdd())){//非true不加载
                continue;
            }
            boolean flag = tableConfigList.stream().anyMatch(e->e.getModule().equals(config.getModule()));
            if(!flag){
                this.runSqlFile(connection, config.getFileName());
                this.addTableConfig(config, connection);
            }
        }
    }

    private void addTableConfig(TableConfig config, Connection connection) throws SQLException {
        StringBuffer sb = new StringBuffer();
        sb.append("insert into cys_table_config")
                .append("(")
                .append("id, module, file_name")
                .append(")")
                .append("values")
                .append("('")
                .append(IdWorker.getIdStr())
                .append("','")
                .append(config.getModule())
                .append("','")
                .append(config.getFileName())
                .append("')");
        connection.prepareStatement(sb.toString()).execute();
        connection.commit();
    }

    /**
     * 读取所有模块配置
     * @return
     */
    private List<TableConfig> readTableConfigList(Connection connection) throws SQLException {
        String sql = "select * from cys_table_config";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<TableConfig> tableConfigList = new ArrayList<>();
        while (resultSet.next()){
            TableConfig config = new TableConfig();
            config.setId(resultSet.getString("id"));
            config.setModule(resultSet.getString("module"));
            config.setFileName(resultSet.getString("file_name"));
            config.setCreateTime(resultSet.getDate("create_time"));
            tableConfigList.add(config);
        }
        return tableConfigList;
    }

    private void createConfigTable(Connection connection){
        String fileName = "cys_config_table.sql";
        this.runSqlFile(connection, fileName);
    }

    private void runSqlFile(Connection connection, String fileName){
        try{

            Resource resource = resourceLoader.getResource("classpath:sql/" + fileName);
            //初始化脚本运行器
            ScriptRunner sr = new ScriptRunner(connection);
            //创建阅读器对象
            Reader reader = new FileReader(resource.getFile());
            //运行脚本
            sr.runScript(reader);
        }catch (Exception e){
            logger.error("run sql script find error {}", e.getMessage());
            throw new NikaException(e.getMessage());
        }
    }
}
